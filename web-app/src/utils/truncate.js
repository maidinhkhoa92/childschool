export default function truncate(address = '', chars = 5) {
  const MAX_ADDRESS_LENGTH = 10;
  if (address.length <= MAX_ADDRESS_LENGTH) {
    return address;
  }
  const separator = '...';
  const firstPortionLength = Math.floor(chars / 2);
  return address.substr(0, firstPortionLength) + separator;
}
