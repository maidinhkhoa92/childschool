export const District = [
  'Andalucía',
  'Aragón',
  'Asturias',
  'Baleares',
  'Canarias',
  'Cantabria',
  'Castilla-La Mancha',
  'Castilla y León',
  'Cataluña',
  'Comunidad Valenciana',
  'Extremadura',
  'Galicia',
  'Madrid',
  'Murcia',
  'Navarra',
  'País Vasco',
  'La Rioja'
];

export const Cities = new Map([
  ['Andalucía', ['Almería', 'Cádiz', 'Córdoba', 'Granada', 'Huelva', 'Jaén', 'Málaga', 'Sevilla']],
  ['Aragón', ['Huesca', 'Teruel', 'Zaragoza']],
  ['Asturias', ['Oviedo']],
  ['Baleares', ['Palma de Mallorca']],
  ['Canarias', ['Santa Cruz de Tenerife', 'Las Palmas de Gran Canaria']],
  ['Cantabria', ['Santander']],
  ['Castilla-La Mancha', ['Albacete', 'Ciudad Real', 'Cuenca', 'Guadalajara', 'Toledo']],
  [
    'Castilla y León',
    ['Ávila', 'Burgos', 'León', 'Salamanca', 'Segovia', 'Soria', 'Valladolid', 'Zamora']
  ],
  ['Cataluña', ['Barcelona', 'Gerona', 'Lérida', 'Tarragona']],
  ['Comunidad Valenciana', ['Alicante', 'Castellón de la Plana', 'Valencia']],
  ['Extremadura', ['Badajoz', 'Cáceres']],
  ['Galicia', ['La Coruña', 'Lugo', 'Orense', 'Pontevedra']],
  ['Madrid', ['Madrid']],
  ['Murcia', ['Murcia']],
  ['Navarra', ['Pamplona']],
  ['País Vasco', ['Bilbao', 'San Sebastián', 'Vitoria']],
  ['La Rioja', ['Logroño']]
]);
