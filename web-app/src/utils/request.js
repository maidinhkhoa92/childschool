import axios from 'axios';
import { store } from 'store';
import { authActions } from 'store/actions';
const { REACT_APP_BASE_URL } = process.env;

const request = axios.create({
  baseURL: REACT_APP_BASE_URL,
  timeout: 0,
  headers: {
    'Content-Type': 'application/json'
  }
});

// before send request
request.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return error;
  }
);

// after send request
request.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response && error.response.data && error.response.data.shouldLogout) {
      store.dispatch({ type: authActions.LOGOUT_REQUEST })
    }
    const response = JSON.parse(JSON.stringify(error));
    return response.response;
  }
);

export default request;
