import firebase from 'firebase';
import 'firebase/storage';
import APP_CONFIG from 'constant/APP_CONFIG';

const app = firebase.initializeApp({
  apiKey: APP_CONFIG.firebase.apiKey,
  authDomain: APP_CONFIG.firebase.REACT_APP_AUTH_DOMAIN || 'https://accounts.google.com/o/oauth2/auth',
  databaseURL: APP_CONFIG.firebase.REACT_APP_DATABASE_URL || 'https://childschool-webapp.firebaseio.com',
  projectId: APP_CONFIG.firebase.REACT_APP_PROJECT_ID || 'childschool-webapp',
  storageBucket: APP_CONFIG.firebase.REACT_APP_STORAGE_BUCKET || 'gs://childschool-webapp.appspot.com'
});

export const storageRef = app.storage();

export default app.firestore();
