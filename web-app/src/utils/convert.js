import moment from 'moment';

const convertIcon = type => {
  switch (type) {
    case 'Photo':
      return '/images/activities/1.svg';
    case 'Video':
      return '/images/activities/2.svg';
    case 'Foods':
      return '/images/activities/3.svg';
    case 'FeedingBottle':
      return '/images/activities/4.svg';
    case 'Bathroom':
      return '/images/activities/5.svg';
    case 'Achievements':
      return '/images/activities/6.svg';
    case 'CheerUp':
      return '/images/activities/7.svg';
    case 'Siesta':
      return '/images/activities/8.svg';
    case 'Medicines':
      return '/images/activities/9.svg';
    default:
      return '/images/activities/10.svg';
  }
};

const convertTime = value => {
  return moment(value).format('HH:mm');
};

const convertImage = ({ type, content, group }) => {
  switch (type) {
    case 'Photo':
      return content;
    case 'Video':
      return '/images/activities/2.svg';
    case 'Foods':
      return '/images/activities/3.svg';
    case 'FeedingBottle':
      return '/images/activities/4.svg';
    case 'Bathroom':
      return '/images/activities/5.svg';
    case 'Achievements':
      return group;
    case 'CheerUp':
      return group;
    case 'Siesta':
      return '/images/activities/8.svg';
    case 'Medicines':
      return '/images/activities/9.svg';
    default:
      return '/images/activities/10.svg';
  }
};

export { convertIcon, convertTime, convertImage };
