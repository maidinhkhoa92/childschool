export default [
  {
    id: 1,
    content: 'L@ llevo a las',
    time: true,
    square: true
  },
  {
    id: 2,
    content: 'L@ recojo a las',
    time: true,
    square: true
  },
  {
    id: 3,
    content: 'L@ recogerá',
    time: false,
    square: true
  },
  {
    id: 4,
    content: 'Le toca la medicina a las',
    time: true,
    square: true
  },
  {
    id: 5,
    content: 'No l@ llevo hoy',
    time: false,
    square: false
  }
];
