import actions from './actions';

const initialState = {
  token: null,
  infor: {}
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return { ...state, token: action.respon.token, infor: action.respon };
    case actions.COMPLETE_DIRECTOR_SUCCESS:
      return { ...state, infor: action.respon };
    case actions.RESET_PASSWORD_SUCCESS:
      return { ...state, token: action.payload.token, infor: action.payload };
    case actions.LOGOUT_RESET:
      return initialState;
    default:
      return state;
  }
};

export default Auth;
