const actions = {
  CONTACT_REQUEST: 'CONTACT_REQUEST',
  contact: (params, success, fail) => ({
    type: actions.CONTACT_REQUEST,
    params,
    success,
    fail
  }),

  UPDATE_PASSWORD_REQUEST: 'UPDATE_PASSWORD_REQUEST',
  updatePassword: (params, success, fail) => ({
    type: actions.UPDATE_PASSWORD_REQUEST,
    params,
    success,
    fail
  }),

  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  login: (params, success, fail) => ({
    type: actions.LOGIN_REQUEST,
    params,
    success,
    fail
  }),

  UPDATE_PIN_REQUEST: 'UPDATE_PIN_REQUEST',
  updatePin: (params, confirm, success, fail) => ({
    type: actions.UPDATE_PIN_REQUEST,
    params,
    confirm,
    success,
    fail
  }),

  COMPARE_PIN_REQUEST: 'COMPARE_PIN_REQUEST',
  comparePin: (params, success, fail) => ({
    type: actions.COMPARE_PIN_REQUEST,
    params,
    success,
    fail
  }),

  CHANGE_PIN_REQUEST: 'CHANGE_PIN_REQUEST',
  changePin: (params, success, fail) => ({
    type: actions.CHANGE_PIN_REQUEST,
    params,
    success,
    fail
  }),

  COMPLETE_DIRECTOR_REQUEST: 'COMPLETE_DIRECTOR_REQUEST',
  COMPLETE_DIRECTOR_SUCCESS: 'COMPLETE_DIRECTOR_SUCCESS',
  completeDirector: (params, success, fail) => ({
    type: actions.COMPLETE_DIRECTOR_REQUEST,
    params,
    success,
    fail
  }),

  FORGOT_PIN_REQUEST: 'FORGOT_PIN_REQUEST',
  forgotPin: (params, success, fail) => ({
    type: actions.FORGOT_PIN_REQUEST,
    params,
    success,
    fail
  }),

  FORGOT_PASSWORD_REQUEST: 'FORGOT_PASSWORD_REQUEST',
  FORGOT_PASSWORD_SUCCESS: 'FORGOT_PASSWORD_SUCCESS',
  forgotPassword: payload => ({
    type: actions.FORGOT_PASSWORD_REQUEST,
    payload,
    meta: {
      thunk: true
    }
  }),

  RESET_PASSWORD_REQUEST: 'RESET_PASSWORD_REQUEST',
  RESET_PASSWORD_SUCCESS: 'RESET_PASSWORD_SUCCESS',
  resetPassword: payload => ({
    type: actions.RESET_PASSWORD_REQUEST,
    payload,
    meta: {
      thunk: true
    }
  }),

  SUBCRIBE_REQUEST: 'SUBCRIBE_REQUEST',
  SUBCRIBE_SUCCESS: 'SUBCRIBE_SUCCESS',
  subcribe: payload => ({
    type: actions.SUBCRIBE_REQUEST,
    payload,
    meta: {
      thunk: true
    }
  }),

  LOGOUT_REQUEST: 'LOGOUT_REQUEST',
  LOGOUT_RESET: 'LOGOUT_RESET',
  logout: success => ({
    type: actions.LOGOUT_REQUEST,
    success
  }),

  AUTH_FAILURE: 'AUTH_FAILURE'
};
export default actions;
