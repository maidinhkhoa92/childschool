import actions from './actions';
import { all, fork, takeLatest, put, select } from 'redux-saga/effects';
import {
  Contact,
  UpdatePassword,
  Login,
  UpdatePin,
  ComparePin,
  CompleteDirector,
  ChangePin,
  ForgotPin,
  ForgotPassword,
  ResetPassword,
  Subcribe
} from 'services/auth';
import {
  classesActions,
  childActions,
  staffActions,
  menuActions,
  messageActions,
  eventActions,
  checkinActions
} from '../actions';
import { getToken, getUserId } from 'store/selectors';

export function* contactSaga() {
  yield takeLatest(actions.CONTACT_REQUEST, function* (data) {
    const { params, success, fail } = data;
    try {
      const res = yield Contact(params);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updatePasswordSaga() {
  yield takeLatest(actions.UPDATE_PASSWORD_REQUEST, function* (data) {
    const { params, success, fail } = data;
    try {
      const res = yield UpdatePassword(params);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* loginSaga() {
  yield takeLatest(actions.LOGIN_REQUEST, function* (data) {
    const { params, success, fail } = data;
    try {
      const res = yield Login(params);
      if (res.status === 200) {
        yield put({ type: actions.LOGIN_SUCCESS, respon: res.data });
        success(res.data.active);
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updatePinSaga() {
  yield takeLatest(actions.UPDATE_PIN_REQUEST, function* (data) {
    const { params, confirm, success, fail } = yield data;
    const token = yield select(getToken);

    if (JSON.stringify(params) === JSON.stringify(confirm)) {
      try {
        const res = yield UpdatePin({ digit: params }, token);
        if (res.status === 200) {
          yield success();
        } else {
          yield fail(res.data.message);
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      yield fail();
    }
  });
}

export function* comparePinSaga() {
  yield takeLatest(actions.COMPARE_PIN_REQUEST, function* (data) {
    const { params, success, fail } = yield data;
    const token = yield select(getToken);

    try {
      const res = yield ComparePin({ digit: params }, token);
      if (res.status === 200) {
        yield success();
      } else {
        yield fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* completeDirectorSaga() {
  yield takeLatest(actions.COMPLETE_DIRECTOR_REQUEST, function* (data) {
    const { params, success, fail } = yield data;
    const token = yield select(getToken);
    const user_id = yield select(getUserId);
    try {
      const res = yield CompleteDirector(user_id, params, token);
      if (res.status === 200) {
        yield put({ type: actions.COMPLETE_DIRECTOR_SUCCESS, respon: res.data });
        yield success();
      } else {
        yield fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* changePinSaga() {
  yield takeLatest(actions.CHANGE_PIN_REQUEST, function* (data) {
    const { params, success, fail } = yield data;
    const token = yield select(getToken);
    try {
      const res = yield ChangePin(params, token);
      if (res.status === 200) {
        yield success();
      } else {
        yield fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* forgotPinSaga() {
  yield takeLatest(actions.FORGOT_PIN_REQUEST, function* (data) {
    const { params, success, fail } = yield data;
    const token = yield select(getToken);

    try {
      const res = yield ForgotPin({ email: params }, token);
      if (res.status === 200) {
        yield success();
      } else {
        yield fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* forgotPasswordSaga() {
  yield takeLatest(actions.FORGOT_PASSWORD_REQUEST, function* (data) {
    const { payload, meta } = yield data;

    try {
      const res = yield ForgotPassword(payload);
      if (res.status === 200) {
        yield put({ type: actions.FORGOT_PASSWORD_SUCCESS, payload: res.data, meta });
      } else {
        yield put({ type: actions.AUTH_FAILURE, payload: res.data, error: true, meta });
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* subcribeSaga() {
  yield takeLatest(actions.SUBCRIBE_REQUEST, function* (data) {
    const { payload, meta } = yield data;

    try {
      const res = yield Subcribe(payload);
      if (res.status === 200) {
        yield put({ type: actions.SUBCRIBE_SUCCESS, payload: res.data, meta });
      } else {
        yield put({ type: actions.AUTH_FAILURE, payload: res.data, error: true, meta });
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* resetPasswordSaga() {
  yield takeLatest(actions.RESET_PASSWORD_REQUEST, function* (data) {
    const { payload, meta } = yield data;

    try {
      const res = yield ResetPassword(payload.params, payload.token);
      if (res.status === 200) {
        yield put({ type: actions.RESET_PASSWORD_SUCCESS, payload: res.data, meta });
      } else {
        yield put({ type: actions.AUTH_FAILURE, payload: res.data, error: true, meta });
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* logoutSaga() {
  yield takeLatest(actions.LOGOUT_REQUEST, function* (data) {
    const { success } = yield data;
    yield put({ type: actions.LOGOUT_RESET });
    yield put({ type: classesActions.RESET_CLASSES });
    yield put({ type: childActions.RESET_CHILD });
    yield put({ type: staffActions.RESET_STAFF });
    yield put({ type: menuActions.RESET_MENU });
    yield put({ type: messageActions.RESET_MESSAGE });
    yield put({ type: eventActions.RESET_EVENT });
    yield put({ type: checkinActions.RESET_CHECKIN });
    if (success) {
      yield success();
    }

  });
}

export default function* rootSaga() {
  yield all([
    fork(contactSaga),
    fork(updatePasswordSaga),
    fork(loginSaga),
    fork(updatePinSaga),
    fork(comparePinSaga),
    fork(completeDirectorSaga),
    fork(logoutSaga),
    fork(changePinSaga),
    fork(forgotPinSaga),
    fork(forgotPasswordSaga),
    fork(resetPasswordSaga),
    fork(subcribeSaga)
  ]);
}
