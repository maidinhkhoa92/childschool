import actions from './actions';
import _ from 'lodash';

const initialState = {
  list: []
};

const Staff = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_STAFF_SUCCESS:
      return { ...state, list: action.respon };
    case actions.CREATE_STAFF_SUCCESS:
      return { ...state, list: [...state.list, action.respon] };
    case actions.EDIT_STAFF_SUCCESS:
      return {
        ...state,
        list: _.map(state.list, item => {
          if (item.id === action.respon.id) {
            return action.respon;
          }
          return item;
        })
      };
    case actions.DEACTIVE_STAFF_SUCCESS:
      return { ...state, list: _.filter(state.list, item => item.id !== action.respon) };
    case actions.RESET_STAFF:
      return initialState;
    default:
      return state;
  }
};

export default Staff;
