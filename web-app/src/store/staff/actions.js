const actions = {
  GET_STAFF_REQUEST: 'GET_STAFF_REQUEST',
  GET_STAFF_SUCCESS: 'GET_STAFF_SUCCESS',
  get: (success, fail) => ({
    type: actions.GET_STAFF_REQUEST,
    success,
    fail
  }),

  CREATE_STAFF_REQUEST: 'CREATE_STAFF_REQUEST',
  CREATE_STAFF_SUCCESS: 'CREATE_STAFF_SUCCESS',
  create: (params, success, fail) => ({
    type: actions.CREATE_STAFF_REQUEST,
    params,
    success,
    fail
  }),

  EDIT_STAFF_REQUEST: 'EDIT_STAFF_REQUEST',
  EDIT_STAFF_SUCCESS: 'EDIT_STAFF_SUCCESS',
  edit: (id, params, success, fail) => ({
    type: actions.EDIT_STAFF_REQUEST,
    id,
    params,
    success,
    fail
  }),

  DEACTIVE_STAFF_REQUEST: 'DEACTIVE_STAFF_REQUEST',
  DEACTIVE_STAFF_SUCCESS: 'DEACTIVE_STAFF_SUCCESS',
  deactive: (id, success, fail) => ({
    type: actions.DEACTIVE_STAFF_REQUEST,
    id,
    success,
    fail
  }),

  RESET_STAFF: 'RESET_STAFF'
};
export default actions;
