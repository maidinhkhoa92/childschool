import actions from './actions';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { List, Create, Update, deactive } from 'services/staff';
import { getToken } from 'store/selectors';

export function* getStaffSaga() {
  yield takeLatest(actions.GET_STAFF_REQUEST, function*(data) {
    const { success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield List(token);
      if (res.status === 200) {
        yield put({ type: actions.GET_STAFF_SUCCESS, respon: res.data.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* createStaffSaga() {
  yield takeLatest(actions.CREATE_STAFF_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Create(params, token);

      if (res.status === 200) {
        yield put({ type: actions.CREATE_STAFF_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* editStaffSaga() {
  yield takeLatest(actions.EDIT_STAFF_REQUEST, function*(data) {
    const { id, params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Update(id, params, token);

      if (res.status === 200) {
        yield put({ type: actions.EDIT_STAFF_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* deactiveStaffSaga() {
  yield takeLatest(actions.DEACTIVE_STAFF_REQUEST, function*(data) {
    const { id, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield deactive(id, token);

      if (res.status === 200) {
        yield put({ type: actions.DEACTIVE_STAFF_SUCCESS, respon: id });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getStaffSaga),
    fork(createStaffSaga),
    fork(editStaffSaga),
    fork(deactiveStaffSaga)
  ]);
}
