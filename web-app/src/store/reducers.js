import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import Auth from 'store/auth/reducer';
import Classes from 'store/classes/reducer';
import Child from 'store/child/reducer';
import Staff from 'store/staff/reducer';
import Menu from 'store/menu/reducer';
import Message from 'store/message/reducer';
import Event from 'store/event/reducer';
import Checkin from 'store/checkin/reducer';
import Alert from 'store/alert/reducer';
import News from 'store/news/reducer';

const reducers = combineReducers({
  Auth,
  Classes,
  Child,
  Staff,
  Menu,
  Message,
  Event,
  Checkin,
  Alert,
  News
});

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['Alert', 'News', 'Checkin', 'Note']
};
const persistedReducer = persistReducer(persistConfig, reducers);
export default persistedReducer;
