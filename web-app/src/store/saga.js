import { all } from 'redux-saga/effects';
import authSagas from 'store/auth/saga';
import classesSagas from 'store/classes/saga';
import childSagas from 'store/child/saga';
import staffSagas from 'store/staff/saga';
import mediaSagas from 'store/media/saga';
import menuSagas from 'store/menu/saga';
import messageSagas from 'store/message/saga';
import eventSagas from 'store/event/saga';
import checkinSagas from 'store/checkin/saga';
import newsSagas from 'store/news/saga';

export default function* rootSaga() {
  yield all([
    authSagas(),
    classesSagas(),
    childSagas(),
    staffSagas(),
    mediaSagas(),
    menuSagas(),
    messageSagas(),
    eventSagas(),
    checkinSagas(),
    newsSagas()
  ]);
}
