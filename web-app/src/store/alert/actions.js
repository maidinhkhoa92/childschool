const actions = {
  ALERT_OPEN: 'ALERT_OPEN',
  open: message => ({
    type: actions.ALERT_OPEN,
    message
  }),

  ALERT_CLOSE: 'ALERT_CLOSE',
  close: () => ({
    type: actions.ALERT_CLOSE
  })
};
export default actions;
