import actions from './actions';

const initialState = {
  message: '',
  status: false
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case actions.ALERT_OPEN:
      return { ...state, message: action.message, status: true };
    case actions.ALERT_CLOSE:
      return { ...state, message: '', status: false };
    default:
      return state;
  }
};

export default Auth;
