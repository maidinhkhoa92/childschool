const actions = {
  GET_NEWS_REQUEST: 'GET_NEWS_REQUEST',
  GET_NEWS_SUCCESS: 'GET_NEWS_SUCCESS',
  get: (params, success, fail) => ({
    type: actions.GET_NEWS_REQUEST,
    params,
    success,
    fail
  }),

  RESET_NEWS: 'RESET_NEWS'
};
export default actions;
