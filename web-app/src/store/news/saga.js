import actions from './actions';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { Get } from 'services/news';
import { getToken } from 'store/selectors';

export function* getNewsSaga() {
  yield takeLatest(actions.GET_NEWS_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Get(params, token);

      if (res.status === 200) {
        yield put({ type: actions.GET_NEWS_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getNewsSaga)]);
}
