import actions from './actions';

const initialState = {
  list: []
};

const News = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_NEWS_SUCCESS:
      return { ...state, list: action.respon };
    case actions.RESET_NEWS:
      return initialState;
    default:
      return state;
  }
};

export default News;
