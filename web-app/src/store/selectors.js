export const getToken = state => state.Auth.token;
export const getUserId = state => state.Auth.infor.id;
export const getInfor = state => state.Auth.infor;
export const classesId = state => state.Classes.detail.id;
export const childId = state => state.Child.detail.id;
