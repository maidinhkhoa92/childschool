import actions from './actions';
import { all, fork, takeLatest } from 'redux-saga/effects';
import { UploadImage, UploadVideo } from 'services/media';

export function* uploadImageSaga() {
  yield takeLatest(actions.UPLOAD_IMAGE_REQUEST, function*(data) {
    const { image, success, fail } = data;
    try {
      const res = yield UploadImage(image);
      success(res.url);
    } catch (error) {
      fail(error.message);
    }
  });
}

export function* uploadVideoSaga() {
  yield takeLatest(actions.UPLOAD_VIDEO_REQUEST, function*(data) {
    const { image, success, fail } = data;
    try {
      const res = yield UploadVideo(image);
      success(res.url);
    } catch (error) {
      fail(error.message);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(uploadImageSaga), fork(uploadVideoSaga)]);
}
