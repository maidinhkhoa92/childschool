const actions = {
  UPLOAD_IMAGE_REQUEST: 'UPLOAD_IMAGE_REQUEST',
  uploadImage: (image, success, fail) => ({
    type: actions.UPLOAD_IMAGE_REQUEST,
    image,
    success,
    fail
  }),

  UPLOAD_VIDEO_REQUEST: 'UPLOAD_VIDEO_REQUEST',
  uploadVideo: (image, success, fail) => ({
    type: actions.UPLOAD_VIDEO_REQUEST,
    image,
    success,
    fail
  })
};
export default actions;
