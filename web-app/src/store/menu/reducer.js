import actions from './actions';

const initialState = {
  url: '',
  name: ''
};

const MENU = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_MENU_SUCCESS:
      return action.respon;
    case actions.CREATE_MENU_SUCCESS:
      return action.respon;
    case actions.RESET_MENU:
      return initialState;
    default:
      return state;
  }
};

export default MENU;
