const actions = {
  GET_MENU_REQUEST: 'GET_MENU_REQUEST',
  GET_MENU_SUCCESS: 'GET_MENU_SUCCESS',
  get: (params, success, fail) => ({
    type: actions.GET_MENU_REQUEST,
    params,
    success,
    fail
  }),

  CREATE_MENU_REQUEST: 'CREATE_MENU_REQUEST',
  CREATE_MENU_SUCCESS: 'CREATE_MENU_SUCCESS',
  create: (file, success, fail) => ({
    type: actions.CREATE_MENU_REQUEST,
    file,
    success,
    fail
  }),

  RESET_MENU: 'RESET_MENU'
};
export default actions;
