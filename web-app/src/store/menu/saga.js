import actions from './actions';
import _ from 'lodash';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { Upload, Create, Get } from 'services/menu';
import { getToken } from 'store/selectors';

export function* getMenuSaga() {
  yield takeLatest(actions.GET_MENU_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Get(params, token);
      if (res.status === 200) {
        yield put({ type: actions.GET_MENU_SUCCESS, respon: _.last(res.data.data) });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* createMenuSaga() {
  yield takeLatest(actions.CREATE_MENU_REQUEST, function*(data) {
    const { file, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Upload(file, token);

      const params = {
        url: res.url,
        name: res.name
      };

      const respon = yield Create(params, token);
      if (respon.status === 200) {
        yield put({ type: actions.CREATE_MENU_SUCCESS, respon: respon.data });
        success();
      } else {
        fail(respon.data.message);
      }
    } catch (error) {
      fail(error);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getMenuSaga), fork(createMenuSaga)]);
}
