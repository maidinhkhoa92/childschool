const actions = {
  GET_CHILD_REQUEST: 'GET_CHILD_REQUEST',
  GET_CHILD_SUCCESS: 'GET_CHILD_SUCCESS',
  get: (params, success, fail) => ({
    type: actions.GET_CHILD_REQUEST,
    params,
    success,
    fail
  }),

  DETAIL_CHILD_REQUEST: 'DETAIL_CHILD_REQUEST',
  DETAIL_CHILD_SUCCESS: 'DETAIL_CHILD_SUCCESS',
  detail: (id, success, fail) => ({
    type: actions.DETAIL_CHILD_REQUEST,
    id,
    success,
    fail
  }),

  UPDATE_CHILD_REQUEST: 'UPDATE_CHILD_REQUEST',
  UPDATE_CHILD_SUCCESS: 'UPDATE_CHILD_SUCCESS',
  update: (params, success, fail) => ({
    type: actions.UPDATE_CHILD_REQUEST,
    params,
    success,
    fail
  }),

  POST_NEWS_CHILD_REQUEST: 'POST_NEWS_CHILD_REQUEST',
  postNews: (params, success, fail) => ({
    type: actions.POST_NEWS_CHILD_REQUEST,
    params,
    success,
    fail
  }),

  UPDATE_CHILD_PERSON_REQUEST: 'UPDATE_CHILD_PERSON_REQUEST',
  UPDATE_CHILD_PERSON_SUCCESS: 'UPDATE_CHILD_PERSON_SUCCESS',
  updatePerson: (id, params, success, fail) => ({
    type: actions.UPDATE_CHILD_PERSON_REQUEST,
    id,
    params,
    success,
    fail
  }),

  SEARCH_CHILD_REQUEST: 'SEARCH_CHILD_REQUEST',
  SEARCH_CHILD_SUCCESS: 'SEARCH_CHILD_SUCCESS',
  SEARCH_CHILD_STATUS_SUCCESS: 'SEARCH_CHILD_STATUS_SUCCESS',
  search: (params, success, fail) => ({
    type: actions.SEARCH_CHILD_REQUEST,
    params,
    success,
    fail
  }),

  RESET_CHILD: 'RESET_CHILD'
};
export default actions;
