import actions from './actions';

const initialState = {
  detail: {
    profile: {
      firstName: 'Sofía',
      lastName: 'Otero'
    },
    firstTeacher: {
      _id: ''
    },
    secondTeacher: {
      _id: ''
    },
    family: {
      profile: {},
      email: ''
    }
  },
  search: {
    status: false,
    list: []
  },
  list: []
};

const Child = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_CHILD_SUCCESS:
      return { ...state, list: action.respon };
    case actions.DETAIL_CHILD_SUCCESS:
      return { ...state, detail: action.respon };
    case actions.UPDATE_CHILD_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          profile: action.respon.profile
        }
      };
    case actions.UPDATE_CHILD_PERSON_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          profile: action.respon.profile
        }
      };
    case actions.SEARCH_CHILD_SUCCESS:
      return {
        ...state,
        search: {
          list: action.data.list,
          status: action.data.status
        }
      };
    case actions.RESET_CHILD:
      return initialState;
    default:
      return state;
  }
};

export default Child;
