import actions from './actions';
import { all, fork, takeLatest, select, put, call } from 'redux-saga/effects';
import { Get, Detail, Update, postNews, updatePerson, search } from 'services/child';
import { CompleteDirector } from 'services/auth';
import { getToken, childId, getInfor } from 'store/selectors';

export function* listChildSaga() {
  yield takeLatest(actions.GET_CHILD_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Get(params, token);

      if (res.status === 200) {
        yield put({ type: actions.GET_CHILD_SUCCESS, respon: res.data.data });
        success(res.data.data);
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* detailChildSaga() {
  yield takeLatest(actions.DETAIL_CHILD_REQUEST, function*(data) {
    const { id, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Detail(id, token);

      if (res.status === 200) {
        yield put({ type: actions.DETAIL_CHILD_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updateChildSaga() {
  yield takeLatest(actions.UPDATE_CHILD_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    const child_id = yield select(childId);
    try {
      const res = yield Update(child_id, params, token);

      if (res.status === 200) {
        yield put({ type: actions.UPDATE_CHILD_SUCCESS, respon: res.data });

        // update family profile
        const infor = yield select(getInfor);
        console.log(params.profile);
        const body = {
          email: infor.email,
          profile: {
            ...infor.profile,
            photo:
              params.profile && params.profile.secondFather && params.profile.secondFather.photo
                ? params.profile.secondFather.photo
                : null
          }
        };
        const response = yield call(CompleteDirector, infor.id, body, token);
        if (response.status === 200) {
          success();
        } else {
          fail(res.data.message);
        }
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* postNewsSaga() {
  yield takeLatest(actions.POST_NEWS_CHILD_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield postNews(params, token);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updateChildPersonSaga() {
  yield takeLatest(actions.UPDATE_CHILD_PERSON_REQUEST, function*(data) {
    const { id, params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield updatePerson(id, params, token);

      if (res.status === 200) {
        // yield put({ type: actions.UPDATE_CHILD_PERSON_SUCCESS, respon: res.data });
        const respon = yield Detail(id, token);

        if (respon.status === 200) {
          yield put({ type: actions.DETAIL_CHILD_SUCCESS, respon: respon.data });
          success();
        } else {
          fail(respon.data.message);
        }
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* searchChildSaga() {
  yield takeLatest(actions.SEARCH_CHILD_REQUEST, function*(data) {
    const { params, success, fail } = yield data;
    const token = yield select(getToken);
    try {
      if (params.word === '') {
        yield put({ type: actions.SEARCH_CHILD_SUCCESS, data: { list: [], status: false } });
        fail();
      } else {
        const res = yield search(params, token);
        if (res.status === 200) {
          const result = {
            list: res.data,
            status: true
          };
          yield put({ type: actions.SEARCH_CHILD_SUCCESS, data: result });
          success();
        } else {
          yield put({ type: actions.SEARCH_CHILD_SUCCESS, data: { list: [], status: false } });
          fail();
        }
      }
    } catch (error) {
      console.log(error);
    }
  });
}
export default function* rootSaga() {
  yield all([
    fork(listChildSaga),
    fork(detailChildSaga),
    fork(updateChildSaga),
    fork(postNewsSaga),
    fork(updateChildPersonSaga),
    fork(searchChildSaga)
  ]);
}
