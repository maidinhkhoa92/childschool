const actions = {
  CREATE_MESSAGE_REQUEST: 'CREATE_MESSAGE_REQUEST',
  CREATE_MESSAGE_SUCCESS: 'CREATE_MESSAGE_SUCCESS',
  create: (params, success, fail) => ({
    type: actions.CREATE_MESSAGE_REQUEST,
    params,
    success,
    fail
  }),

  GET_MESSAGE_REQUEST: 'GET_MESSAGE_REQUEST',
  GET_MESSAGE_SUCCESS: 'GET_MESSAGE_SUCCESS',
  get: (params, success, fail) => ({
    type: actions.GET_MESSAGE_REQUEST,
    params,
    success,
    fail
  }),

  UPDATE_MESSAGE_REQUEST: 'UPDATE_MESSAGE_REQUEST',
  update: (id, params, success, fail) => ({
    type: actions.UPDATE_MESSAGE_REQUEST,
    id,
    params,
    success,
    fail
  }),

  RESET_MESSAGE: 'RESET_MESSAGE'
};
export default actions;
