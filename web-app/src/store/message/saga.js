import actions from './actions';
import _ from 'lodash';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { create, list, update } from 'services/message';
import { getToken, getUserId } from 'store/selectors';

export function* createMessageSaga() {
  yield takeLatest(actions.CREATE_MESSAGE_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    const user_id = yield select(getUserId);
    const body = yield {
      message: params.message,
      classes: _.map(params.classes, item => item.id),
      from_user: user_id,
      to_user: _.map(params.to_user, item => item.id),
      type: params.type,
      note: params.note ? params.note : ''
    };
    try {
      const res = yield create(body, token);

      if (res.status === 200) {
        yield put({ type: actions.CREATE_MESSAGE_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* getMessageSaga() {
  yield takeLatest(actions.GET_MESSAGE_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield list(params, token);

      if (res.status === 200) {
        yield put({ type: actions.GET_MESSAGE_SUCCESS, respon: res.data.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updateMessageSaga() {
  yield takeLatest(actions.UPDATE_MESSAGE_REQUEST, function*(data) {
    const { id, params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield update(id, params, token);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getMessageSaga), fork(createMessageSaga), fork(updateMessageSaga)]);
}
