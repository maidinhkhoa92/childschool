import actions from './actions';

const initialState = {
  list: []
};

const Message = (state = initialState, action) => {
  switch (action.type) {
    case actions.CREATE_MESSAGE_SUCCESS:
      return { ...state, list: [action.respon, ...state.list] };
    case actions.GET_MESSAGE_SUCCESS:
      return { ...state, list: action.respon };
    case actions.RESET_MESSAGE:
      return initialState;
    default:
      return state;
  }
};

export default Message;
