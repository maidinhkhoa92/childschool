import actions from './actions';
import _ from 'lodash';

const initialState = {
  list: [],
  detail: {},
  child: []
};

const Event = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_EVENT_SUCCESS:
      return { ...state, list: action.respon };
    case actions.CREATE_EVENT_SUCCESS:
      return { ...state, list: [...state.list, action.respon] };
    case actions.DELETE_EVENT_SUCCESS:
      return { ...state, list: _.filter(state.list, item => item.id !== action.respon) };
    case actions.UPDATE_EVENT_SUCCESS:
      return { ...state, detail: { ...state.detail, name: action.respon } };
    case actions.DETAIL_EVENT_SUCCESS:
      return { ...state, detail: actions.respon };
    case actions.RESET_EVENT:
      return initialState;
    default:
      return state;
  }
};

export default Event;
