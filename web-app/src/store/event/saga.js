import actions from './actions';
import moment from 'moment';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { List, Create, Delete, Update, Detail } from 'services/event';
import { getToken } from 'store/selectors';

export function* getEventSaga() {
  yield takeLatest(actions.GET_EVENT_REQUEST, function*(data) {
    const { query, success, fail } = data;
    const token = yield select(getToken);
    query.date = moment(query.date).format('YYYY-MM-DD');
    try {
      const res = yield List(query, token);

      if (res.status === 200) {
        yield put({ type: actions.GET_EVENT_SUCCESS, respon: res.data.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* createEventSaga() {
  yield takeLatest(actions.CREATE_EVENT_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Create(params, token);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* deleteEventSaga() {
  yield takeLatest(actions.DELETE_EVENT_REQUEST, function*(data) {
    const { id, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Delete(id, token);

      if (res.status === 200) {
        yield put({ type: actions.DELETE_EVENT_SUCCESS, respon: id });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updateEventSaga() {
  yield takeLatest(actions.UPDATE_EVENT_REQUEST, function*(data) {
    const { id, params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Update(id, params, token);

      if (res.status === 200) {
        yield put({ type: actions.UPDATE_EVENT_SUCCESS, respon: res.data.name });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* getDetailEventSaga() {
  yield takeLatest(actions.DETAIL_EVENT_REQUEST, function*(data) {
    const { id, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Detail(id, token);

      if (res.status === 200) {
        success(res.data);
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getEventSaga),
    fork(createEventSaga),
    fork(deleteEventSaga),
    fork(updateEventSaga),
    fork(getDetailEventSaga)
  ]);
}
