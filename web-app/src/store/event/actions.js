const actions = {
  GET_EVENT_REQUEST: 'GET_EVENT_REQUEST',
  GET_EVENT_SUCCESS: 'GET_EVENT_SUCCESS',
  get: (query, success, fail) => ({
    type: actions.GET_EVENT_REQUEST,
    query,
    success,
    fail
  }),

  CREATE_EVENT_REQUEST: 'CREATE_EVENT_REQUEST',
  CREATE_EVENT_SUCCESS: 'CREATE_EVENT_SUCCESS',
  create: (params, success, fail) => ({
    type: actions.CREATE_EVENT_REQUEST,
    params,
    success,
    fail
  }),

  DELETE_EVENT_REQUEST: 'DELETE_EVENT_REQUEST',
  DELETE_EVENT_SUCCESS: 'DELETE_EVENT_SUCCESS',
  delete: (id, success, fail) => ({
    type: actions.DELETE_EVENT_REQUEST,
    id,
    success,
    fail
  }),

  UPDATE_EVENT_REQUEST: 'UPDATE_EVENT_REQUEST',
  UPDATE_EVENT_SUCCESS: 'UPDATE_EVENT_SUCCESS',
  update: (id, params, success, fail) => ({
    type: actions.UPDATE_EVENT_REQUEST,
    id,
    params,
    success,
    fail
  }),

  DETAIL_EVENT_REQUEST: 'DETAIL_EVENT_REQUEST',
  DETAIL_EVENT_SUCCESS: 'DETAIL_EVENT_SUCCESS',
  detail: (id, success, fail) => ({
    type: actions.DETAIL_EVENT_REQUEST,
    id,
    success,
    fail
  }),

  RESET_EVENT: 'RESET_EVENT'
};
export default actions;
