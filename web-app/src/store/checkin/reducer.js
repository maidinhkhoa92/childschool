import actions from './actions';

const initialState = {
  detail: {},
  list: []
};

const Checkin = (state = initialState, action) => {
  switch (action.type) {
    case actions.CREATE_CHECKIN_SUCCESS:
      return { ...state, detail: action.respon };
    case actions.EDIT_CHECKIN_SUCCESS:
      return { ...state, detail: action.respon };
    case actions.LIST_CHECKIN_SUCCESS:
      return { ...state, list: action.respon };
    case actions.RESET_CHECKIN:
      return initialState;
    default:
      return state;
  }
};

export default Checkin;
