import actions from './actions';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { List, Create, Update } from 'services/checkin';
import { getToken } from 'store/selectors';

export function* listCheckinSaga() {
  yield takeLatest(actions.LIST_CHECKIN_REQUEST, function*(data) {
    const { success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield List(token);
      if (res.status === 200) {
        yield put({ type: actions.LIST_CHECKIN_SUCCESS, respon: res.data });
        yield success();
      } else {
        yield fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* createCheckinSaga() {
  yield takeLatest(actions.CREATE_CHECKIN_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Create(params, token);

      if (res.status === 200) {
        yield put({ type: actions.CREATE_CHECKIN_SUCCESS, respon: res.data });
        success(res.data);
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updateCheckinSaga() {
  yield takeLatest(actions.EDIT_CHECKIN_REQUEST, function*(data) {
    const { id, params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Update(id, params, token);

      if (res.status === 200) {
        yield put({ type: actions.EDIT_CHECKIN_SUCCESS, respon: res.data });
        success(res.data);
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export default function* rootSaga() {
  yield all([fork(listCheckinSaga), fork(createCheckinSaga), fork(updateCheckinSaga)]);
}
