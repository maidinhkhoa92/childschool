const actions = {
  LIST_CHECKIN_REQUEST: 'LIST_CHECKIN_REQUEST',
  LIST_CHECKIN_SUCCESS: 'LIST_CHECKIN_SUCCESS',
  list: (success, fail) => ({
    type: actions.LIST_CHECKIN_REQUEST,
    success,
    fail
  }),

  CREATE_CHECKIN_REQUEST: 'CREATE_CHECKIN_REQUEST',
  CREATE_CHECKIN_SUCCESS: 'CREATE_CHECKIN_SUCCESS',
  create: (params, success, fail) => ({
    type: actions.CREATE_CHECKIN_REQUEST,
    params,
    success,
    fail
  }),

  EDIT_CHECKIN_REQUEST: 'EDIT_CHECKIN_REQUEST',
  EDIT_CHECKIN_SUCCESS: 'EDIT_CHECKIN_SUCCESS',
  edit: (id, params, success, fail) => ({
    type: actions.EDIT_CHECKIN_REQUEST,
    id,
    params,
    success,
    fail
  }),

  RESET_CHECKIN: 'RESET_CHECKIN'
};
export default actions;
