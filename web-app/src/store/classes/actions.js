const actions = {
  GET_CLASSES_REQUEST: 'GET_CLASSES_REQUEST',
  GET_CLASSES_SUCCESS: 'GET_CLASSES_SUCCESS',
  get: (success, fail) => ({
    type: actions.GET_CLASSES_REQUEST,
    success,
    fail
  }),

  CREATE_CLASSES_REQUEST: 'CREATE_CLASSES_REQUEST',
  CREATE_CLASSES_SUCCESS: 'CREATE_CLASSES_SUCCESS',
  create: (params, success, fail) => ({
    type: actions.CREATE_CLASSES_REQUEST,
    params,
    success,
    fail
  }),

  DETAIL_CLASSES_REQUEST: 'DETAIL_CLASSES_REQUEST',
  DETAIL_CLASSES_SUCCESS: 'DETAIL_CLASSES_SUCCESS',
  detail: (id, success, fail) => ({
    type: actions.DETAIL_CLASSES_REQUEST,
    id,
    success,
    fail
  }),

  DELETE_CLASSES_REQUEST: 'DELETE_CLASSES_REQUEST',
  DELETE_CLASSES_SUCCESS: 'DELETE_CLASSES_SUCCESS',
  delete: (id, success, fail) => ({
    type: actions.DELETE_CLASSES_REQUEST,
    id,
    success,
    fail
  }),

  UPDATE_CLASSES_REQUEST: 'UPDATE_CLASSES_REQUEST',
  UPDATE_CLASSES_SUCCESS: 'UPDATE_CLASSES_SUCCESS',
  update: (id, params, success, fail) => ({
    type: actions.UPDATE_CLASSES_REQUEST,
    id,
    params,
    success,
    fail
  }),

  ADD_CHILD_CLASSES_REQUEST: 'ADD_CHILD_CLASSES_REQUEST',
  ADD_CHILD_CLASSES_SUCCESS: 'ADD_CHILD_CLASSES_SUCCESS',
  addChild: (params, success, fail) => ({
    type: actions.ADD_CHILD_CLASSES_REQUEST,
    params,
    success,
    fail
  }),

  REMOVE_CHILD_CLASSES_REQUEST: 'REMOVE_CHILD_CLASSES_REQUEST',
  REMOVE_CHILD_CLASSES_SUCCESS: 'REMOVE_CHILD_CLASSES_SUCCESS',
  removeChild: (childId, success, fail) => ({
    type: actions.REMOVE_CHILD_CLASSES_REQUEST,
    childId,
    success,
    fail
  }),

  POST_NEWS_STATUS_CHILD_REQUEST: 'POST_NEWS_STATUS_CHILD_REQUEST',
  POST_NEWS_STATUS_CHILD_SUCCESS: 'POST_NEWS_STATUS_CHILD_SUCCESS',
  postNewsStatus: (params, success, fail) => ({
    type: actions.POST_NEWS_STATUS_CHILD_REQUEST,
    params,
    success,
    fail
  }),

  RESET_CLASSES: 'RESET_CLASSES'
};
export default actions;
