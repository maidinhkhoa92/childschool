import actions from './actions';
import _ from 'lodash';

const initialState = {
  list: [],
  detail: {
    name: 'Music',
    color: '#AEB6FF'
  },
  child: []
};

const Classes = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_CLASSES_SUCCESS:
      return { ...state, list: action.respon };
    case actions.CREATE_CLASSES_SUCCESS:
      return { ...state, list: [...state.list, action.respon] };
    case actions.DETAIL_CLASSES_SUCCESS:
      return { ...state, detail: action.respon, child: action.respon.child };
    case actions.ADD_CHILD_CLASSES_SUCCESS:
      return { ...state, detail: action.respon, child: action.respon.child };
    case actions.UPDATE_CLASSES_SUCCESS:
      return { ...state, detail: { ...state.detail, name: action.respon } };
    case actions.POST_NEWS_STATUS_CHILD_SUCCESS:
      return {
        ...state,
        child: _.map(state.child, item => {
          if (item.id === action.respon.id) {
            return {
              ...item,
              sleeping: action.respon.sleeping
            };
          }
          return item;
        })
      };
    case actions.RESET_CLASSES:
      return initialState;
    default:
      return state;
  }
};

export default Classes;
