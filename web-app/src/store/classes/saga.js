import actions from './actions';
import { all, fork, takeLatest, select, put } from 'redux-saga/effects';
import { List, Create, Detail, AddChild, RemoveChild, Delete, Update } from 'services/classes';
import { postNews } from 'services/child';
import { getToken, classesId } from 'store/selectors';

export function* getClassesSaga() {
  yield takeLatest(actions.GET_CLASSES_REQUEST, function*(data) {
    const { success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield List(token);

      if (res.status === 200) {
        yield put({ type: actions.GET_CLASSES_SUCCESS, respon: res.data.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* createClassesSaga() {
  yield takeLatest(actions.CREATE_CLASSES_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Create(params, token);

      if (res.status === 200) {
        yield put({ type: actions.CREATE_CLASSES_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* detailClassesSaga() {
  yield takeLatest(actions.DETAIL_CLASSES_REQUEST, function*(data) {
    const { id, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Detail(id, token);

      if (res.status === 200) {
        yield put({ type: actions.DETAIL_CLASSES_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* deleteClassesSaga() {
  yield takeLatest(actions.DELETE_CLASSES_REQUEST, function*(data) {
    const { id, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Delete(id, token);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* updateClassesSaga() {
  yield takeLatest(actions.UPDATE_CLASSES_REQUEST, function*(data) {
    const { id, params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield Update(id, params, token);

      if (res.status === 200) {
        yield put({ type: actions.UPDATE_CLASSES_SUCCESS, respon: res.data.name });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* addChildClassesSaga() {
  yield takeLatest(actions.ADD_CHILD_CLASSES_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    const id = yield select(classesId);
    try {
      const res = yield AddChild(id, params, token);

      if (res.status === 200) {
        yield put({ type: actions.ADD_CHILD_CLASSES_SUCCESS, respon: res.data });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* removeChildClassesSaga() {
  yield takeLatest(actions.REMOVE_CHILD_CLASSES_REQUEST, function*(data) {
    const { childId, success, fail } = data;
    const token = yield select(getToken);
    const class_Id = yield select(classesId);
    try {
      const res = yield RemoveChild(class_Id, childId, token);

      if (res.status === 200) {
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export function* postNewsStatusSaga() {
  yield takeLatest(actions.POST_NEWS_STATUS_CHILD_REQUEST, function*(data) {
    const { params, success, fail } = data;
    const token = yield select(getToken);
    try {
      const res = yield postNews(params, token);

      if (res.status === 200) {
        yield put({ type: actions.POST_NEWS_STATUS_CHILD_SUCCESS, respon: res.data[0] });
        success();
      } else {
        fail(res.data.message);
      }
    } catch (error) {
      console.log(error);
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getClassesSaga),
    fork(createClassesSaga),
    fork(detailClassesSaga),
    fork(addChildClassesSaga),
    fork(removeChildClassesSaga),
    fork(deleteClassesSaga),
    fork(updateClassesSaga),
    fork(postNewsStatusSaga)
  ]);
}
