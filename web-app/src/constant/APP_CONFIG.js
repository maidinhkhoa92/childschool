export default {
    firebase: {
        apiKey: process.env.REACT_APP_FIREBASE_KEY || 'AIzaSyDNzZis7J30BJSm479iLYAvDKh_co4EFOs',
        authDomain: process.env.REACT_APP_AUTH_DOMAIN || 'https://accounts.google.com/o/oauth2/auth',
        databaseURL: process.env.REACT_APP_DATABASE_URL || 'https://childschool-webapp.firebaseio.com',
        projectId: process.env.REACT_APP_PROJECT_ID || 'childschool-webapp',
        storageBucket: process.env.REACT_APP_STORAGE_BUCKET || 'gs://childschool-webapp.appspot.com'
    }
    
}