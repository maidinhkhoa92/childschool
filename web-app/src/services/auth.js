import request from 'utils/request';

const Contact = params => {
  return request({
    url: '/contact',
    method: 'post',
    data: params
  });
};

const UpdatePassword = params => {
  return request({
    url: '/user/update-password',
    method: 'patch',
    data: params
  });
};

const Login = params => {
  return request({
    url: '/login',
    method: 'post',
    data: params
  });
};

const UpdatePin = (params, token) => {
  return request({
    url: '/user/update-digit',
    method: 'patch',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const ChangePin = (params, token) => {
  return request({
    url: '/user/change-digit',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const ComparePin = (params, token) => {
  return request({
    url: '/user/compare-digit',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const CompleteDirector = (id, params, token) => {
  return request({
    url: '/user/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};
const ForgotPin = (params, token) => {
  return request({
    url: '/user/forgot-digit',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const ForgotPassword = params => {
  return request({
    url: '/user/forgot-password',
    method: 'post',
    data: params
  });
};

const ResetPassword = (params, token) => {
  return request({
    url: '/user/reset-password',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Subcribe = params => {
  return request({
    url: '/user/subcribe',
    method: 'post',
    data: params
  });
};

export {
  Contact,
  UpdatePassword,
  ChangePin,
  Login,
  UpdatePin,
  ComparePin,
  CompleteDirector,
  ForgotPin,
  ForgotPassword,
  ResetPassword,
  Subcribe
};
