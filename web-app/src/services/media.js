import { storageRef } from 'utils/firebase';
import firebase from 'firebase';
import moment from 'moment';
import Resizer from 'react-image-file-resizer';
import { dataURLtoFile } from 'utils/convertBase64';
import { isFileImage } from 'utils/fileType';

const UploadImage = imageUrl => {
  return new Promise((resolve, reject) => {
    const fileName = imageUrl.name;
    Resizer.imageFileResizer(
      imageUrl,
      300,
      300,
      'JPEG',
      100,
      0,
      uri => {
        const image = dataURLtoFile(uri, fileName);
        // image is not real
        if (!isFileImage(image)) {
          reject({ message: 'Solo se pueden subir imágenes' });
          return;
        }
        // name for file
        const name = moment(new Date()).format('DD-MM-YYYY-h-mm-ss-') + image.name;
        // config file on firebase
        const uploadingRef = storageRef
          .ref()
          .child(`images/${name}`)
          .put(image);
        // run proccessing
        uploadingRef.on(
          'state_changed',
          function (snapshot) {
            // Observe state change events such as progress, pause, and resume
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
          },
          function (error) {
            // Handle unsuccessful uploads
            reject(error);
          },
          function () {
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            uploadingRef.snapshot.ref.getDownloadURL().then(function (downloadURL) {
              resolve({ url: downloadURL });
            });
          }
        );
      },
      'base64'
    );
  });
};

const UploadVideo = async video => {
  return new Promise((resolve, reject) => {
    if (video.size > 500000) {
      reject({ message: 'Image size is not larger 500kb' })
      return;
    }
    const name = moment(new Date()).format("DD-MM-YYYY-h-mm-ss-") + video.name;
    const uploadingRef = storageRef.ref().child(`videos/${name}`).put(video);
    uploadingRef.on('state_changed', function (snapshot) {
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
      }
    }, function (error) {
      // Handle unsuccessful uploads
      reject(error)
    }, function () {
      // Handle successful uploads on complete
      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
      uploadingRef.snapshot.ref.getDownloadURL().then(function (downloadURL) {
        resolve({ url: downloadURL });
      });
    })
  });
};

export { UploadImage, UploadVideo };
