import request from 'utils/request';

const Get = (params, token) => {
  return request({
    url: '/news',
    method: 'get',
    params: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { Get };
