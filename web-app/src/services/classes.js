import request from 'utils/request';

const List = token => {
  return request({
    url: '/classes',
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Create = (params, token) => {
  return request({
    url: '/classes',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Detail = (id, token) => {
  return request({
    url: '/classes/' + id,
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const AddChild = (id, params, token) => {
  return request({
    url: '/classes/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const RemoveChild = (classId, childId, token) => {
  return request({
    url: `/classes/${classId}/child/${childId}`,
    method: 'delete',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Delete = (id, token) => {
  return request({
    url: '/classes/' + id,
    method: 'delete',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Update = (id, params, token) => {
  return request({
    url: '/classes/' + id,
    method: 'patch',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { List, Create, Detail, AddChild, RemoveChild, Delete, Update };
