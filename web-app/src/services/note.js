import request from 'utils/request';

const Create = (params, token) => {
  return request({
    url: '/note',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Get = (params, token) => {
  return request({
    url: '/note',
    method: 'get',
    params: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Update = (id, params, token) => {
  return request({
    url: '/note/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { Create, Get, Update };
