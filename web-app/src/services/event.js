import request from 'utils/request';

const List = (params, token) => {
  return request({
    url: '/event',
    method: 'get',
    params: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Create = (params, token) => {
  return request({
    url: '/event',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Update = (id, params, token) => {
  return request({
    url: '/event/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Delete = (id, token) => {
  return request({
    url: '/event/' + id,
    method: 'delete',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Detail = (id, token) => {
  return request({
    url: '/event/' + id,
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { List, Create, Update, Delete, Detail };
