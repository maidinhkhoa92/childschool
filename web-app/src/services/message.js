import request from 'utils/request';
import firestore from 'utils/firebase';

const list = (params, token) => {
  return request({
    url: '/message',
    method: 'get',
    params: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const create = (params, token) => {
  return request({
    url: '/message',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const detail = id => {
  let query = firestore.collection('messages');
  query = query.doc(id);
  return query;
};

const update = (id, params, token) => {
  return request({
    url: '/message/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { create, list, detail, update };
