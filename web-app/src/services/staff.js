import request from 'utils/request';

const List = token => {
  return request({
    url: '/staff',
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Create = (params, token) => {
  return request({
    url: '/user',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Update = (id, params, token) => {
  return request({
    url: '/user/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const deactive = (id, token) => {
  return request({
    url: '/user/' + id,
    method: 'delete',
    data: {
      status: false
    },
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { List, Create, Update, deactive };
