import request from 'utils/request';

const List = token => {
  return request({
    url: '/class-action',
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Create = (params, token) => {
  return request({
    url: '/class-action',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Update = (id, params, token) => {
  return request({
    url: '/class-action/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { List, Create, Update };
