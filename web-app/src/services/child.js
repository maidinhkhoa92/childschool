import request from 'utils/request';

const Get = (params, token) => {
  return request({
    url: '/child',
    method: 'get',
    params: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Detail = (id, token) => {
  return request({
    url: '/child/' + id,
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Update = (id, params, token) => {
  return request({
    url: '/child/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const postNews = (params, token) => {
  return request({
    url: '/child',
    method: 'patch',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const updatePerson = (id, params, token) => {
  return request({
    url: '/child-person/' + id,
    method: 'put',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const search = (params, token) => {
  return request({
    url: '/search-child',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { Get, Detail, Update, postNews, updatePerson, search };
