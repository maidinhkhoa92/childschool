import request from 'utils/request';
import { storageRef } from 'utils/firebase';
import moment from 'moment';
import firebase from 'firebase';

const Upload = async pdf => {
  return new Promise((resolve, reject) => {
    const name = moment(new Date()).format("DD-MM-YYYY-h-mm-ss-") + pdf.name;
    const uploadingRef = storageRef.ref().child(`pdfs/${name}`).put(pdf);
    uploadingRef.on('state_changed', function(snapshot){
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
      }
    }, function(error) {
      // Handle unsuccessful uploads
      reject(error)
    }, function() {
      // Handle successful uploads on complete
      // For instance, get the download URL: https://firebasestorage.googleapis.com/...
      uploadingRef.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        resolve({url: downloadURL, name: pdf.name});
      });
    })
  });
};

const Create = (params, token) => {
  return request({
    url: '/menu',
    method: 'post',
    data: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

const Get = (params, token) => {
  return request({
    url: '/menu',
    method: 'get',
    params: params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export { Upload, Create, Get };
