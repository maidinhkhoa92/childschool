import React from 'react';
import history from 'utils/history';

const HeaderBack = ({ title, classTitle, url }) => {
  return (
    <ion-header no-border translucent>
      <ion-toolbar>
        <ion-buttons slot="start" onClick={() => history.goBack()}>
          <ion-img src="/images/back.svg" alt="logo" />
        </ion-buttons>
        <ion-title class={classTitle}>{title}</ion-title>
        <ion-buttons slot="end">
          <a href={url} className="top-right-menu" target="blank">
            <ion-img src="/images/icon/caja-de-almuerzo.svg" alt="logo" />
            Menu
          </a>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
  );
};

export default HeaderBack;
