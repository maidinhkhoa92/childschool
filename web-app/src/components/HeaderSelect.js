import React from 'react';
import _ from 'lodash';
import history from 'utils/history';

const HeaderSelect = ({ onPress, onLeftPress, onRightPress, right, list, selectedValue }) => (
  <ion-header no-border>
    <ion-toolbar>
      <ion-buttons slot="start">
        <ion-img
          src="/images/back.svg"
          onClick={() => {
            onLeftPress ? onLeftPress() : history.goBack();
          }}
        />
      </ion-buttons>
      <ion-row>
        <ion-col>
          <select
            className="header-staff-Lannister"
            value={selectedValue}
            onChange={e => onPress(e.target.value)}
          >
            {_.map(list, (item, key) => (
              <option key={key} value={item.id}>
                {item.name}
              </option>
            ))}
          </select>
        </ion-col>
      </ion-row>
      {right && (
        <ion-buttons slot="end">
          <ion-img src="/images/calendar-add.svg" onClick={() => onRightPress()} />
        </ion-buttons>
      )}
    </ion-toolbar>
  </ion-header>
);

export default HeaderSelect;
