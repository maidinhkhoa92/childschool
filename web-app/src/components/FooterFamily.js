import React from 'react';
import history from 'utils/history';
import _ from 'lodash';

const FooterFamily = ({ phone, toggle, first, second }) => {
  return (
    <ion-footer>
      <ion-toolbar>
        <ion-row class="footer-directorr">
          <ion-col onClick={() => toggle(true)} class="phone-wrapper">
            <ion-img src="/images/footer/telephone.svg" alt="logo" />
          </ion-col>
          <ion-col onClick={() => history.push('/family/avisos')}>
            <ion-img src="/images/footer/avisos.png" alt="logo" />
            Avisos
          </ion-col>
          <ion-col onClick={() => history.push('/family/agenda')}>
            <ion-img src="/images/footer/schedule.svg" alt="logo" />
            Agenda
          </ion-col>
          <ion-col onClick={() => history.push('/family/mensajes')}>
            <ion-img src="/images/footer/message.svg" alt="logo" />
            Mensajes
          </ion-col>
          <ion-col onClick={() => history.push('/family/diario')}>
            <ion-img src="/images/footer/libro.svg" alt="logo" />
            Diario
          </ion-col>
        </ion-row>
      </ion-toolbar>
      {!_.isNull(first) && !_.isNull(second) && phone && (
        <div className="phone-popup">
          {first && <a href={`tel:${first.profile.telephone}`}>{first.profile.firstName}</a>}
          {second && <a href={`tel:${second.profile.telephone}`}>{second.profile.firstName}</a>}
        </div>
      )}
    </ion-footer>
  );
};

export default FooterFamily;
