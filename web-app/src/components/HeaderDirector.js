import React from 'react';
import history from 'utils/history';

const HeaderDirector = ({ name, onLogout }) => {
  return (
    <ion-header no-border translucent class="control-panel">
      <ion-toolbar>
        <ion-buttons slot="start">
          <ion-button onClick={() => onLogout()}>
            <ion-img src="/images/logout.svg" alt="logo" class="icon-back" />
          </ion-button>
        </ion-buttons>

        <ion-title>{name}</ion-title>

        <ion-buttons slot="end">
          <ion-button onClick={() => history.push('/director/center/edit')}>
            <ion-img src="/images/icon/pen-edit.svg" alt="logo" class="icon-back" />
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
  );
};

export default HeaderDirector;
