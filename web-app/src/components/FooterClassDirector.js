import React from 'react';
import history from 'utils/history';

const FooterClassDirector = ({ onPress }) => {
  return (
    <ion-footer>
      <ion-toolbar>
        <ion-row class="footer-directorr">
          <ion-col onClick={() => history.push('/director/checkin')}>
            <ion-img src="/images/footer/check.svg" alt="logo" />
            Check-in/out
          </ion-col>
          <ion-col
            onClick={() =>
              history.push({ pathname: '/director/actividades', state: { type: 'class' } })
            }
          >
            <ion-img src="/images/footer/activity.svg" alt="logo" />
            Actividades
          </ion-col>
          <ion-col onClick={() => history.push('/director/lannister')}>
            <ion-img src="/images/footer/schedule.svg" alt="logo" />
            Agenda
          </ion-col>
          <ion-col onClick={() => history.push('/director/message')}>
            <ion-img src="/images/footer/message.svg" alt="logo" />
            Mensajes
          </ion-col>
          <ion-col onClick={() => onPress(true)}>
            <ion-img src="/images/footer/child.svg" alt="logo" />+ Alumn@
          </ion-col>
        </ion-row>
      </ion-toolbar>
    </ion-footer>
  );
};

export default FooterClassDirector;
