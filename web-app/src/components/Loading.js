import React from 'react';
import { IonSpinner } from '@ionic/react';

const Loading = () => <IonSpinner />;

export default Loading;
