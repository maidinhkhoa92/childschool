import React from 'react';
import history from 'utils/history';

const Header = ({ login, subcribe, children, onClick }) => {
  return (
    <ion-header no-border translucent>
      <ion-toolbar>
        <ion-buttons slot="start" onClick={() => history.push('/')}>
          <ion-img src="/images/icon/alimentador.svg" alt="logo" class="logo-header" />
          <p className="text-logo">OntáBb</p>
        </ion-buttons>
        {children}
        {subcribe && (
          <ion-buttons slot="end" onClick={onClick} class="subcribe-button">
            <a color="success" className="btn-success login-home">
              Contacta
            </a>
          </ion-buttons>
        )}
        {login && (
          <ion-buttons slot="end" onClick={() => history.push('acceso')}>
            <a color="success" className="btn-success login-home">
              Accede
            </a>
          </ion-buttons>
        )}
      </ion-toolbar>
    </ion-header>
  );
};

export default Header;
