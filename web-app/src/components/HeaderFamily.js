import React from 'react';
import history from 'utils/history';

const HeaderFamily = ({ right, onPress, logout, onLogout }) => {
  return (
    <ion-header no-border translucent class="classes-header transparent">
      <ion-toolbar position="top">
        <ion-buttons slot="start">
          <ion-button onClick={() => { logout ? onLogout() : history.goBack()}}>
            <ion-img src={logout ? "/images/logout.svg" : "/images/back.svg"} alt="logo" class="icon-back" />
          </ion-button>
        </ion-buttons>
        <ion-buttons slot="end">
          <ion-button onClick={() => onPress(true)}>
            <ion-img src="/images/edit.svg" alt="logo" class="icon-back" />
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
  );
};

export default HeaderFamily;
