import React from 'react';
import history from 'utils/history';

const FooterClassStaff = () => {
  return (
    <ion-footer>
      <ion-toolbar>
        <ion-row class="footer-directorr">
          <ion-col onClick={() => history.push('/staff/checkin')}>
            <ion-img src="/images/footer/check.svg" alt="logo" />
            Check-in/out
          </ion-col>
          <ion-col
            onClick={() =>
              history.push({ pathname: '/staff/actividades', state: { type: 'class' } })
            }
          >
            <ion-img src="/images/footer/activity.svg" alt="logo" />
            Actividades
          </ion-col>
          <ion-col onClick={() => history.push('/staff/lannister')}>
            <ion-img src="/images/footer/schedule.svg" alt="logo" />
            Agenda
          </ion-col>
          <ion-col onClick={() => history.push('/staff/mensajes')}>
            <ion-img src="/images/footer/message.svg" alt="logo" />
            Mensajes
          </ion-col>
        </ion-row>
      </ion-toolbar>
    </ion-footer>
  );
};

export default FooterClassStaff;
