import React from 'react';
import history from 'utils/history';

const HeaderClasses = ({ right, popup, onPopup, onAction, color }) => {
  return (
    <ion-header no-border translucent class="classes-header">
      <ion-toolbar position="top" style={{ '--background': color }}>
        <ion-buttons slot="start">
          <ion-button onClick={() => history.goBack()}>
            <ion-img src="/images/back.svg" alt="logo" class="icon-back" />
          </ion-button>
        </ion-buttons>
        <ion-buttons slot="end">
          {right && (
            <ion-button onClick={e => onPopup(e)}>
              <ion-img src="/images/dot.svg" alt="logo" class="icon-back" />
            </ion-button>
          )}
        </ion-buttons>
      </ion-toolbar>
      {popup && (
        <div className="button-wrapper">
          <div className="button" onClick={() => onAction('edit')}>
            Editar nombre
          </div>
          <div className="button" onClick={() => onAction('delete')}>
            Eliminar clase
          </div>
        </div>
      )}
    </ion-header>
  );
};

export default HeaderClasses;
