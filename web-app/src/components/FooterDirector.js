import React from 'react';
import history from 'utils/history';
import _ from 'lodash';

const FooterDirectory = ({ phone, toggle, first, second }) => {
  return (
    <ion-footer>
      <ion-toolbar>
        <ion-row class="footer-directorr">
          <ion-col onClick={() => toggle(true)}>
            <ion-img src="/images/footer/telephone.svg" alt="logo" />
          </ion-col>
          <ion-col
            onClick={() =>
              history.push({ pathname: '/director/actividades', state: { type: 'child' } })
            }
          >
            <ion-img src="/images/footer/activity.svg" alt="logo" />
            Actividades
          </ion-col>
          <ion-col onClick={() => history.push('/director/lannister')}>
            <ion-img src="/images/footer/schedule.svg" alt="logo" />
            Agenda
          </ion-col>
          <ion-col onClick={() => history.push('/director/message')}>
            <ion-img src="/images/footer/message.svg" alt="logo" />
            Mensajes
          </ion-col>
          <ion-col onClick={() => history.push('/director/diario')}>
            <ion-img src="/images/footer/libro.svg" alt="logo" />
            Diario
          </ion-col>
        </ion-row>
      </ion-toolbar>
      {phone && (
        <div className="phone-popup">
          {first && <a href={`tel:${first.profile.telephone}`}>{first.profile.firstName}</a>}
          {second && <a href={`tel:${second.profile.telephone}`}>{second.profile.firstName}</a>}
        </div>
      )}
    </ion-footer>
  );
};

export default FooterDirectory;
