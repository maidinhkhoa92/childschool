import React from 'react';
import history from 'utils/history';

const HeaderTransparent = ({ right, popup, onPopup, onAction }) => {
  return (
    <ion-header no-border translucent class="classes-header transparent">
      <ion-toolbar position="top">
        <ion-buttons slot="start">
          <ion-button onClick={() => history.goBack()}>
            <ion-img src="/images/back.svg" alt="logo" class="icon-back" />
          </ion-button>
        </ion-buttons>
        <ion-buttons slot="end">
          {right && (
            <ion-button onClick={() => onPopup()}>
              <ion-img src="/images/dot.svg" alt="logo" class="icon-back" />
            </ion-button>
          )}
        </ion-buttons>
      </ion-toolbar>
      {popup && (
        <div className="button-wrapper">
          <div className="button" onClick={() => onAction('update')}>
            Editar
          </div>
          <div className="button" onClick={() => onAction('delete')}>
            Eliminar perfil
          </div>
        </div>
      )}
    </ion-header>
  );
};

export default HeaderTransparent;
