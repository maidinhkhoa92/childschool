import React from 'react';
import history from 'utils/history';

const FooterStaff = ({ phone, toggle, first, second }) => {
  return (
    <ion-footer>
      <ion-toolbar>
        <ion-row class="footer-directorr">
          <ion-col onClick={() => toggle(true)}>
            <ion-img src="/images/footer/telephone.svg" alt="logo" />
          </ion-col>
          <ion-col
            onClick={() =>
              history.push({ pathname: '/staff/actividades', state: { type: 'child' } })
            }
          >
            <ion-img src="/images/footer/activity.svg" alt="logo" />
            Actividades
          </ion-col>
          <ion-col onClick={() => history.push('/staff/lannister')}>
            <ion-img src="/images/footer/schedule.svg" alt="logo" />
            Agenda
          </ion-col>
          <ion-col onClick={() => history.push('/staff/mensajes')}>
            <ion-img src="/images/footer/message.svg" alt="logo" />
            Mensajes
          </ion-col>
          <ion-col onClick={() => history.push('/staff/diario')}>
            <ion-img src="/images/footer/libro.svg" alt="logo" />
            Diario
          </ion-col>
        </ion-row>
      </ion-toolbar>
      {phone && (
        <div className="phone-popup">
          {first && <a href={`tel:${first.profile.telephone}`}>{first.profile.firstName}</a>}
          {second && <a href={`tel:${second.profile.telephone}`}>{second.profile.firstName}</a>}
        </div>
      )}
    </ion-footer>
  );
};

export default FooterStaff;
