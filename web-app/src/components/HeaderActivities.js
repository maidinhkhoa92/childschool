import React from 'react';

const Header = ({ url, onClose }) => (
  <ion-header no-border translucent class="header-activities">
    <ion-toolbar>
      <ion-img src={url} alt="logo" class="icon-logo" />
      <ion-img
        src="/images/icon/close.svg"
        alt="logo"
        class="icon-close"
        onClick={() => onClose()}
      />
    </ion-toolbar>
  </ion-header>
);

export default Header;
