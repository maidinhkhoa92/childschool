import React from 'react';
import { connect } from 'react-redux';
import { mediaActions } from 'store/actions';

class UploadFile extends React.Component {
  onUpload = e => {
    const image = e.target.files[0];
    this.props.upload(image, this.props.success, this.props.fail);
  };

  imageStyle = () => {
    if (this.props.photo !== null) {
      return { backgroundImage: 'url(' + this.props.photo + ')', ...this.props.style };
    }
    return { ...this.props.style };
  };

  render() {
    return (
      <div className="file-input" style={this.imageStyle()}>
        <input type="file" onChange={e => this.onUpload(e)} disabled={this.props.disabled} />
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  upload: mediaActions.uploadImage
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadFile);
