import React from 'react';
import { Header } from 'components';

export default () => (
  <>
    <Header login={true} />
    <ion-content class="homepage">
      <ion-grid class="main-contnet">
        <div className="container">
          <h3>AVISO LEGAL Y CONDICIONES DE USO</h3>
          <p>
            En cumplimiento del artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la
            Sociedad de la Información y del Comercio Electrónico (LSSICE), René Alejandro Otero
            Bosch (en adelante, “Ontabb”), Responsable de esta web, pone a disposición de los
            Usuarios la presente información, para definir sus Condiciones de Uso.
          </p>
          <p>
            Adicionalmente a los contenidos aquí expuestos, los aspectos específicos relacionados
            con la protección de los datos personales y la privacidad de los usuarios de esta web se
            desarrollan en las páginas de Política de Privacidad y Política de Cookies.
          </p>
          <h3>Identidad del Responsable del Tratamiento</h3>
          <p>Denominación Social: René Alejandro Otero Bosch</p>
          <p>Nombre Comercial: Ontább</p>
          <p>CIF / NIF / NIE: Y4445309K</p>
          <p>Domicilio Social: C/ Campo Florido 64 3-2, 08027, Barcelona.</p>
          <p>Actividad: App guarderías</p>
          <p>
            Teléfono: <a href="tel:688377414">688377414</a>
          </p>
          <p>
            email: <a href="mailto:info@myontabb.com">info@myontabb.com</a>
          </p>
          <p>
            Formulario de contacto online:{' '}
            <a href="https://www.myontabb.com/contact" target="blank">
              https://www.myontabb.com/contact
            </a>
          </p>
          <p>
            Nombre de Dominio:{' '}
            <a href="https://www.myontabb.com/" target="blank">
              https://www.myontabb.com/
            </a>
          </p>
          <p>Registro de Tratamientos conforme al RGPD: Clientes/Proveedores, Usuarios Web.</p>
          <h3>Finalidad de la Web</h3>
          <p>
            La web de Ontább tiene la finalidad de prestar un servicio digital de comunicación entre
            guarderías y familias.
          </p>
          <h3>Marco Normativo</h3>
          <p>
            La actividad de esta web se encuentra sujeta al marco legal español y europeo,
            concretamente a las siguientes normas:
          </p>
          <p>
            Reglamento General de Protección de Datos (RGPD) (UE) 2016/679, que regula el
            tratamiento de datos personales por parte de los Responsables situados en los países de
            la Unión Europea.
          </p>
          <p>
            Ley Orgánica 3/2018, de 5 de diciembre, sobre protección de datos y derechos digitales
            (LOPD y GDD), normas de ámbito regional (aplicables a España), y que definen y amplían
            muchos de los conceptos y derechos presentes en el RGPD.
          </p>
          <p>
            Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y del
            Comercio Electrónico (LSSICE), norma que afecta a aquellas webs que, de alguna forma,
            realicen actividades económicas mediante medios electrónicos, como es el caso de esta
            web.
          </p>
          <h3>Condiciones de Uso y Responsabilidades</h3>
          <p>
            Toda persona que acceda a este sitio web asume el papel de Usuario, comprometiéndose a
            la observancia y cumplimiento riguroso de las condiciones aquí dispuestas, así como a
            cualesquiera otra disposición legal que fuera de aplicación. “Ontább” no se hará
            responsable de los daños y perjuicios, propios o a terceros, producidos por el uso de
            este sitio web por parte del Usuario.
          </p>
          <p>
            “Ontább” proporciona el acceso a artículos, informaciones, servicios y datos de su
            propiedad o de terceros, elaborados con fines meramente informativos o divulgativos, que
            pueden no reflejar el estado actual de la legislación o la jurisprudencia, y que se
            refieren a situaciones generales, por lo que su contenido no debe ser aplicado
            necesariamente por el Usuario a casos concretos. El contenido de esta web, por tanto, no
            puede ser considerado, en ningún caso, sustitutivo de asesoramiento legal.
          </p>
          <p>
            “Ontább” se reserva el derecho a modificar cualquier tipo de información que pudiera
            aparecer en la web, en cualquier momento y sin previo aviso, sin que exista obligación
            de preavisar o poner en conocimiento de los Usuarios dichas obligaciones, entendiéndose
            como suficiente la publicación en el presente sitio web.
          </p>
          <p>
            Este sitio web ha sido revisado y probado para que funcione correctamente de manera
            ininterrumpida. No obstante, “Ontább” no descarta la posibilidad de que existan ciertos
            errores de programación, falta de disponibilidad puntual (por ejemplo, caídas del
            servidor, o mantenimiento del mismo) o que acontezcan causas de fuerza mayor,
            catástrofes naturales, huelgas, o circunstancias semejantes que hagan imposible el
            acceso temporal a la página web. De igual forma, “Ontább” no puede garantizar el
            funcionamiento ininterrumpido o totalmente libre de errores de esta web, ni se
            responsabiliza de los virus que tengan su origen en una transmisión telemática
            infiltrados por terceros generados con la finalidad de obtener resultados negativos para
            un sistema informático.
          </p>
          <p>
            El Usuario se compromete a no utilizar esta web ni, si es el caso, los servicios o
            productos ofrecidos en el mismo, para la realización de actividades contrarias a la ley,
            al orden público o a estas condiciones de uso. Por tanto, “Ontább” no se hace
            responsable de la información y contenidos almacenados, a título enunciativo pero no
            limitativo, en foros, chat´s, generadores de blogs, comentarios, redes sociales o
            cualesquiera otro medio que permita a terceros publicar contenidos. No obstante y en
            cumplimiento de lo dispuesto en el art. 11 y 16 de la LSSI-CE, “Ontább” se pone a
            disposición de todos los Usuarios, autoridades y fuerzas de seguridad, y colaborando de
            forma activa en la retirada o en su caso bloqueo de todos aquellos contenidos que
            pudieran afectar o contravenir la legislación nacional, o internacional, derechos de
            terceros o la moral y el orden público. En caso de que un Usuario considere que existe
            en la web algún contenido que pudiera ser susceptible de esta clasificación, se ruega lo
            notifique de forma inmediata a nuestro personal.
          </p>
          <p>
            “Ontább” ser reserva el derecho a denegar o retirar el acceso al blog sin necesidad de
            advertencia previa, a instancia propia o de un tercero, a aquellos Usuarios que
            incumplan nuestras Condiciones de Uso.
          </p>
          <h3>Propiedad Intelectual</h3>
          <p>
            El sitio web, incluyendo a título enunciativo pero no limitativo su programación,
            edición, compilación y demás elementos necesarios para su funcionamiento, los diseños,
            logotipos, texto y/o gráficos son propiedad de “Ontább” o, en su caso, dispone de
            licencia o autorización expresa por parte de los autores.
          </p>
          <p>
            Todos los contenidos del sitio web se encuentran debidamente protegidos por la normativa
            de propiedad intelectual e industrial (artículos 8 y 32.1, párrafo segundo, de la Ley de
            Propiedad Intelectual), así como inscritos en los registros públicos correspondientes, y
            no se permite la reproducción y/o publicación, total o parcial, del sitio web, ni su
            tratamiento informático, su distribución, difusión, modificación o transformación, sin
            el permiso previo y por escrito del mismo. “Ontább” velará por el cumplimiento de las
            anteriores condiciones, así como por la debida utilización de los contenidos presentados
            en sus páginas web, ejercitando todas las acciones civiles y penales que le correspondan
            en el caso de infracción o incumplimiento de estos derechos por parte del Usuario.
          </p>
          <p>
            Los diseños, logotipos, texto y/o gráficos ajenos a “Ontább” y que pudieran aparecer en
            el sitio web, pertenecen a sus respectivos propietarios, siendo ellos mismos
            responsables de cualquier posible controversia que pudiera suscitarse respecto a los
            mismos. En todo caso, “Ontább” cuenta con la autorización expresa y previa por parte de
            los mismos. “Ontább” reconoce a favor de sus titulares los correspondientes derechos de
            propiedad industrial e intelectual, no implicando su sola mención o aparición en el
            sitio web la existencia de derechos o responsabilidad alguna del Responsable sobre los
            mismos, como tampoco respaldo, patrocinio o recomendación por parte del mismo.
          </p>
          <h3>Enlaces de terceros</h3>
          <p>
            “Ontább” puede poner a disposición del Usuario enlaces u otros elementos que permiten el
            acceso hacia otros sitios web pertenecientes a terceros. No comercializamos los
            productos y/o servicios de dichas páginas enlazadas, ni asumimos ningún tipo de
            responsabilidad sobre las mismas, ni sobre la información contenida en ellas, ni su
            veracidad o licitud, ni de cualesquiera efectos que pudieran derivarse. En todo caso,
            “Ontább” manifiesta que procederá a la retirada inmediata de cualquier contenido que
            pudiera contravenir la legislación nacional o internacional, la moral o el orden
            público, procediendo a la retirada inmediata de la redirección a dicho sitio web,
            poniendo en conocimiento de las autoridades competentes el contenido en cuestión.
          </p>
          <h3>Ley Aplicable y Jurisdicción</h3>
          <p>
            La relación entre Responsable y Usuario se rige en todos y cada uno de sus extremos por
            la ley española, a la que se someten expresamente ambas partes. El idioma de redacción e
            interpretación de este aviso legal es el español. Para la resolución de todas las
            controversias o cuestiones relacionadas con el presente sitio web o de las actividades
            en él desarrolladas, “Ontább” y Usuario acuerdan someterse a los Juzgados y Tribunales
            del domicilio del Usuario.
          </p>
        </div>
        <ion-row class="footer-homepage">
          <ion-col>
            <div className="container">
              <a href="https://www.instagram.com/myontabb/" target="blank">
                <img src="/images/instagram.svg" className="social-icon" />
              </a>
              <a href="#">
                <img src="/images/linkedin.svg" className="social-icon" />
              </a>
            </div>
          </ion-col>
        </ion-row>
      </ion-grid>
    </ion-content>
  </>
);
