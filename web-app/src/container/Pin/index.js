import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { authActions, alertActions } from 'store/actions';
import './index.scss';

class Pin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: {
        status: false,
        msg: ''
      },
      params: [null, null, null, null],
      toggle: false,
      key: 0
    };
    this.element = [];
  }

  componentDidMount() {
    this.element[0].focus();
  }

  setToggel = value => {
    this.setState({
      toggle: value
    });
  };

  checkActive = index => {
    const value = this.state.params[index];
    if (_.isNumber(value)) {
      return 'active';
    }
    return '';
  };

  onKeyPress = (e, index) => {
    e.preventDefault();
    const keyCode = e.keyCode || e.which;
    const value = String.fromCharCode(keyCode);
    if (e.keyCode === 8) {
      if (index === 3 && this.state.params[3] !== null) {
        index += 1;
      }
      let move = true;
      if (this.state.params[3] !== null) {
        move = false;
      }
      this.setState(
        {
          params: _.map(this.state.params, (item, i) => {
            if (index - 1 === i) {
              return null;
            }
            return item;
          })
        },
        () => {
          if (index - 1 >= 0 && move) {
            this.element[index - 1].focus();
          }
        }
      );
    } else if (keyCode >= 48 && keyCode <= 57) {
      this.setState(
        {
          params: _.map(this.state.params, (item, i) => {
            if (index === i) {
              return parseInt(value);
            }
            return item;
          })
        },
        () => {
          if (!_.includes(this.state.params, null)) {
            this.onSubmit();
          }
          if (index + 1 <= 3) {
            this.element[index + 1].focus();
          }
        }
      );
    }
  };

  onSubmit = () => {
    this.props.onSubmit(this.state.params, this.success, this.fail);
  };

  success = () => {
    const { user } = this.props;
    switch (user.typeOfUser) {
      case 'director':
        if (user.completed) {
          history.push('/director');
        } else {
          history.push('/director/center');
        }
        break;
      case 'family':
        history.push('/family');
        break;
      default:
        history.push('/staff');
    }
  };

  fail = (message = '¡PIN incorrecto!') => {
    this.setState({
      error: {
        status: true,
        msg: message
      }
    });
  };

  forgotPin = () => {
    this.props.onForgot(this.props.user.email, this.forgotSuccess, this.forgotFail);
  };

  forgotSuccess = () => {
    this.setState(
      {
        toggle: false
      },
      () => {
        this.props.alert('Éxito');
      }
    );
  };

  forgotFail = () => {
    this.props.alert('Ha fallado');
  };

  render() {
    const { error, toggle, params } = this.state;
    return (
      <React.Fragment>
        <ion-content class="otp-page-pin-container">
          <ion-grid class="otp-page-pin">
            <ion-row class="logo">
              <ion-col align-self-center class="ion-text-center img-logo">
                <ion-img src="/images/icon/alimentador.svg" alt="logo" class="logo-header" />
                <ion-text>
                  <p className="text-logo">OntáBb</p>
                </ion-text>
              </ion-col>
            </ion-row>
            <ion-row class="from-otp-confirm">
              {_.map(params, (item, index) => (
                <input
                  key={index}
                  type="tel"
                  maxLength="1"
                  ref={n => {
                    this.element[index] = n;
                  }}
                  name={index}
                  className={this.checkActive(index)}
                  autoFocus
                  onKeyDown={e => this.onKeyPress(e, index)}
                />
              ))}
            </ion-row>
            {error.status && (
              <ion-row class="wapper-mess">
                <ion-col class="ion-text-center">
                  <p className="text-err">{error.msg}</p>
                </ion-col>
              </ion-row>
            )}
            <ion-row>
              <ion-col>
                <ion-text>
                  <p onClick={() => this.setToggel(true)} className="text-footer">
                    ¿Se te ha olvidado?
                  </p>
                </ion-text>
              </ion-col>
              <ion-col>
                <ion-text class="ion-text-right">
                  <p onClick={() => history.push('change-pin')} className="text-footer">
                    ¿Quieres cambiar el PIN?
                  </p>
                </ion-text>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
        {toggle && (
          <div className="popup-wapper" id="popup">
            <ion-grid class="popup">
              <ion-row>
                <ion-text class="text-popup">
                  Te enviaremos un PIN provisional a tu mail para que puedas establecer otro.
                </ion-text>
              </ion-row>
              <ion-row class="footer">
                <ion-col>
                  <ion-text onClick={() => this.setToggel(false)}>Cancelar</ion-text>
                </ion-col>
                <ion-col>
                  <ion-text onClick={() => this.forgotPin()}>Confirmar</ion-text>
                </ion-col>
              </ion-row>
            </ion-grid>
          </div>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.Auth.infor
  };
};

const mapDispatchToProps = {
  onSubmit: authActions.comparePin,
  onForgot: authActions.forgotPin,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pin);
