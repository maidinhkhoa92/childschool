import React from 'react';
import { Header } from 'components';

export default () => (
  <>
    <Header login={true} />
    <ion-content class="homepage">
      <ion-grid class="main-contnet">
        <div className="container">
          <h2 className="text-center"> POLÍTICA DE PRIVACIDAD Y DE COOKIES</h2>
          <h3 className="text-center">POLÍTICA DE PRIVACIDAD</h3>
          <p>
            “Ontább” se encuentra profundamente comprometido con el cumplimiento de la normativa
            española y europea de protección de datos de carácter personal, y garantiza el
            cumplimiento íntegro de las obligaciones dispuestas, así como la implementación de las
            medidas de seguridad dispuestas en el Reglamento General de Protección de Datos (RGPD)
            (UE) 2016/679 y en Ley Orgánica 3/2018, de 5 de diciembre, sobre protección de datos y
            derechos digitales (LOPD y GDD, en adelante LOPD).
          </p>
          <p>
            De conformidad con estas normativas, informamos que la utilización de nuestra web puede
            requerir que se faciliten ciertos datos personales a través de formularios de registro o
            contacto, o mediante el envío de emails, y que éstos serán objeto de tratamiento por
            “Ontább”, Responsable del tratamiento, cuyos datos son:
          </p>
          <p>Denominación Social: René Alejandro Otero Bosch</p>
          <p>Nombre Comercial: Ontább</p>
          <p>CIF / NIF / NIE: Y4445309K</p>
          <p>Domicilio Social: C/ Campo Florido 64 3-2, 08027, Barcelona.</p>
          <p>Actividad: App guarderías</p>
          <p>
            Teléfono: <a href="tel:688377414">688377414</a>
          </p>
          <p>
            email: <a href="mailto:info@myontabb.com">info@myontabb.com</a>
          </p>
          <p>
            Un dato personal es cualquier información relativa a una persona: nombre, email,
            domicilio, teléfono, NIF/NIE… Adicionalmente, cuando un Usuario visita nuestro sitio
            web, determinada información se almacena automáticamente por motivos técnicos, como la
            dirección IP asignada por su proveedor de acceso a Internet.
          </p>
          <p>
            “Ontább”, como Responsable del Tratamiento, tiene el deber de informar a los Usuarios de
            su sitio web acerca de la recogida de datos de carácter personal que pueden llevarse a
            cabo, bien sea mediante el envío de correo electrónico o al cumplimentar los formularios
            incluidos en el sitio web.
          </p>
          <p>
            Se obtendrán únicamente los datos precisos para poder realizar el servicio contratado, o
            para poder responder adecuadamente a la petición de información realizada por el
            Usuario. Los datos recabados son identificativos y corresponden a un mínimo razonable
            para poder llevar a término la actividad realizada. En particular, no se recogen datos
            especialmente protegidos en ningún momento. En ningún caso se realizará un uso diferente
            de los datos que la finalidad para los que han sido recabados.
          </p>
          <h3>Formularios de contacto/email</h3>
          <p>
            Finalidad: Dar contestación a su solicitud de información realizada a través de
            nuestro/s formulario/s de contacto.
          </p>
          <p>
            Legitimación: La base jurídica que legitima este tratamiento es el consentimiento del
            Usuario, que podrá revocar en cualquier momento.
          </p>
          <p>
            Cesión de datos: Los datos personales serán tratados a través de servidores gestionados
            por Axarnet, que tendrá la consideración de Encargado del Tratamiento.
          </p>
          <h3>Formularios de registro de Usuario</h3>
          <h3>Finalidades:</h3>
          <p>Gestionar su alta de Usuario en nuestra página web.</p>
          <p>
            Envío de comunicaciones vía email y/o teléfono, con el fin de informar al Usuario de
            posibles incidencias, errores, etc.
          </p>
          <p>
            Legitimación: La base jurídica que legitima este tratamiento es el vínculo contractual.
          </p>
          <p>
            Cesión de datos: Los datos personales serán tratados a través de servidores gestionados
            por Axarnet, que tendrá la consideración de Encargado del Tratamiento.
          </p>
          <h3>Menores de edad</h3>
          <p>
            Podrán usar esta web personas de cualquier edad. Sin embargo, según obliga la LOPD y
            GDD, en caso de menores de 14 años, será condición obligatoria el consentimiento de sus
            padres o tutores para que podamos tratar sus datos personales.
          </p>
          <h3>Registro de Usuarios</h3>
          <p>
            El Usuario podrá solicitar una demo gratuita, concertando la cita a través de nuestra
            web. En el caso de que el Usuario (colegios) esté interesado en nuestra plataforma, el
            registro de Usuarios realizará por parte del Responsable. Serán posteriormente los
            responsables de los colegios los que den de alta a los Usuarios de ese centro (niños,
            padres, educadoras…) a través de la plataforma.
          </p>
          <h3>Se crean, por tanto, diversos perfiles de usuario:</h3>
          <p>Alumnos/as (niños/as): carecen de permisos de edición, solo de visualización.</p>
          <p>
            Padres: tienen permisos de visualización, y para cambiar contraseña. Verán lo relativo a
            sus hijos.
          </p>
          <p>
            Educadoras/es: tienen permisos de visualización, y para cambiar contraseña. Verán lo
            relativo a sus aulas y a los niños asociadas a las mismas.
          </p>
          <p>Director/a: permisos de edición (altas, modificaciones, bajas…) y de visualización</p>
          <h3>El Usuario podrá recibir las siguientes notificaciones:</h3>
          <p>Al darse de alta en la plataforma (email de validación de cuenta).</p>
          <p>Por comentarios realizados, y respuestas a los mismos.</p>
          <p>Por recuperación de contraseña (especificada en el campo anterior).</p>
          <p>
            En “Ontább” bloquearemos una cuenta de Usuario si comete acciones sospechosas o
            fraudulentas. Las cuentas de Usuario no se eliminan por falta de uso. Para eliminar una
            cuenta, el Usuario debe solicitarlo a través de su panel de Usuario, o contactando con
            nosotros.
          </p>
          <h3>Garantías, devoluciones y resolución de conflictos</h3>
          <p>
            Debido a la naturaleza del producto, éste se considerará ejecutado a la contratación del
            servicio, por lo que no será de aplicación el derecho de desistimiento establecido en la
            Ley 3/2014, de 27 de marzo, de Defensa de los Consumidores y Usuarios.
          </p>
          <p>
            Como así la exige la nueva normativa europea, informamos a los usuarios de la existencia
            de una plataforma europea de resolución de litigios para contrataciones online. Así,
            para la resolución de litigios en materia de consumo (conforme al Art. 14.1 del
            Reglamento (UE) 524/2013), la Comisión Europea facilita una plataforma de resolución de
            litigios en línea que se encuentra disponible en el siguiente enlace:
            https://webgate.ec.europa.eu/odr/main/?event=main.home.show&lng=ES
          </p>
          <h3>Cancelación y baja de cuentas</h3>
          <p>
            Al producirse un impago, o que un Usuario solicite la baja del servicio (o no renueve el
            mismo tras el período que corresponda), se suspenderá el servicio. “Ontább” se reserva
            el derecho de dar de baja a cualquier Usuario, en caso de incumplimiento de las
            presentes condiciones de uso, o de cualquiera de las obligaciones descritas
            anteriormente, hecho que será comunicado mediante correo electrónico.
          </p>
          <h3>Medidas de Seguridad</h3>
          <p>
            Se informa a los Usuarios de la web de “Ontább” de que se han adoptado las medidas de
            seguridad técnicas, organizativas y de seguridad a nuestro alcance para evitar la
            pérdida, mal uso, alteración, acceso no autorizado y robo de los datos, y que garantizan
            así la confidencialidad, integridad y calidad de la información contenida en las mismas,
            de acuerdo con lo establecido en la normativa vigente en protección de datos. Los datos
            personales que se recogen en los formularios son objeto de tratamiento, únicamente, por
            parte del personal de “Ontább” o de los Encargados del Tratamiento designados.
          </p>
          <p>
            El Sitio Web de “Ontább” cuenta además con un cifrado SSL, que permite al Usuario el
            envío seguro de sus datos personales a través de los formularios de contacto del sitio
            web.
          </p>
          <h3>Veracidad de los datos</h3>
          <p>
            El Usuario manifiesta que todos los datos facilitados por él son ciertos y correctos y
            se compromete a mantenerlos actualizados. El Usuario responderá de la veracidad de sus
            datos y será el único responsable de cuantos conflictos o litigios pudieran resultar por
            la falsedad de los mismos. Es importante que, para que podamos mantener los datos
            personales actualizados, el Usuario informe a “Ontább” siempre que haya habido alguna
            modificación en los mismos.
          </p>
          <h3>Cesión de datos</h3>
          <p>
            “Ontább” no cederá ni comunicará a ningún tercero tus datos, excepto en los casos
            legalmente previstos o cuando la prestación de un servicio implique la necesidad de una
            relación contractual con un Encargado de Tratamiento. Así, el Usuario acepta que algunos
            de los datos personales recabados sean facilitados a estos Encargados del Tratamiento
            (plataformas de pago, gestoría, intermediarios, etc.), cuando sea necesario para la
            efectiva realización de un servicio contratado o producto adquirido. El Usuario acepta
            también que alguno de los servicios puedan ser, total o parcialmente, subcontratados a
            otras personas o empresas, que tendrán la consideración de Encargados del Tratamiento,
            con los que se ha convenido el correspondiente contrato de confidencialidad, o adherido
            a sus políticas de privacidad, establecidas en sus respectivas páginas web. El Usuario
            podrá negarse a la cesión de tus datos a los Encargados del Tratamiento, mediante
            petición escrita, por cualquiera de los medios anteriormente referenciados.
          </p>
          <p>
            Además, en aquellos casos en que sea necesario, los datos de Clientes podrán ser cedidos
            a determinados organismos, en cumplimiento de una obligación legal: Agencia Tributaria
            Española, entidades bancarias, Inspección de Trabajo, etc.
          </p>
          <h3>Ejercicio de Derechos del Usuario</h3>
          <p>
            La LOPD y GDD y el RGPD conceden a los interesados la posibilidad de ejercer una serie
            de derechos relacionados con el tratamiento de sus datos personales. Para ello, el
            Usuario deberá dirigirse, aportando documentación que acredite su identidad (DNI o
            pasaporte), mediante correo electrónico a hola@miagendainfantil.org, o bien mediante
            comunicación escrita a la dirección que aparece en nuestro Aviso Legal. Dicha
            comunicación deberá reflejar la siguiente información: nombre y apellidos del Usuario,
            la petición de solicitud, el domicilio y los datos acreditativos.
          </p>
          <p>
            El ejercicio de derechos deberá ser realizado por el propio Usuario. No obstante, podrán
            ser ejecutados por una persona autorizada como representante legal del Usuario,
            aportándose la documentación que acredite dicha representación.
          </p>

          <h3>El Usuario podrá solicitar el ejercicio de los derechos siguientes:</h3>
          <p>
            Derecho a solicitar el acceso a los datos personales, que es el derecho a obtener
            información sobre si sus propios datos de carácter personal están siendo objeto de
            tratamiento, la finalidad del tratamiento que, en su caso, se esté realizando, así como
            la información disponible sobre el origen de dichos datos y las comunicaciones
            realizadas o previstas de los mismos.
          </p>
          <p>
            Derecho a solicitar su rectificación, en caso de que los datos personales sean
            incorrectos o inexactos, o supresión de los datos que resulten ser inadecuados o
            excesivos.
          </p>
          <p>
            Derecho a solicitar la limitación de su tratamiento, en cuyo caso únicamente serán
            conservados por “Ontább” los datos estrictamente necesarios para el ejercicio o la
            defensa de reclamaciones.
          </p>
          <p>
            Derecho a oponerse al tratamiento: se refiere al derecho del interesado a que no se
            lleve a cabo el tratamiento de sus datos personales o se cese en el mismo en los
            supuestos en que no sea necesario su consentimiento para el tratamiento, que se trate de
            ficheros de prospección comerciales o que tengan la finalidad de adoptar decisiones
            referidas al interesado y basadas únicamente en el tratamiento automatizado de sus
            datos, salvo que por motivos legítimos o el ejercicio o la defensa de posibles
            reclamaciones se tengan que seguir tratando.
          </p>
          <p>
            Derecho a la portabilidad de los datos: en caso de que quiera que sus datos sean
            tratados por otra empresa, “Ontább” le facilitará la portabilidad de sus datos en
            formato exportable.
          </p>
          <p>
            En el caso de que se haya otorgado el consentimiento para alguna finalidad específica,
            el Usuario tiene derecho a retirar el consentimiento en cualquier momento, sin que ello
            afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.
          </p>
          <p>
            No comprometemos a ejecutar todos estos derechos dentro del plazo legal máximo
            establecido de 10 días hábiles.
          </p>
          <p>
            Si un Usuario considera que hay un problema con la forma en que “Ontább” está manejando
            sus datos, puede dirigir sus reclamaciones al Responsable de Seguridad o a la autoridad
            de protección de datos que corresponda, siendo la Agencia Española de Protección de
            Datos la indicada en el caso de España.
          </p>
          <h3>Conservación de los datos</h3>
          <p>
            Los datos de carácter personal de los Usuarios que usen el formulario de contacto o que
            nos envíen un email solicitando información serán tratados durante el tiempo
            estrictamente necesario para atender a la solicitud de información, o hasta que se
            revoque el consentimiento otorgado.
          </p>
          <h3>Redes Sociales</h3>
          <p>
            “Ontább” cuenta con perfil en algunas de las principales redes sociales de Internet
            (Facebook, Linkedin, Instagram reconociéndose en todos los casos Responsable del
            tratamiento de los datos de sus seguidores, fans, suscriptores, comentaristas y otros
            perfiles de Usuarios (en adelante, seguidores) publicados por “Ontább”. La finalidad del
            tratamiento de datos por parte de “Ontább”, cuando la ley no lo prohíba, será la de
            informar a sus seguidores sobre sus actividades y ofertas, por cualquier vía que la red
            social permita, así como prestar servicio personalizado de atención al Usuario. La base
            jurídica que legitima este tratamiento será el consentimiento del interesado, que podrá
            revocar en cualquier momento.
          </p>
          <p>
            En ningún caso “Ontább” extraerá datos de las redes sociales, a menos que se obtuviera
            puntual y expresamente el consentimiento del Usuario para ello (por ejemplo, para la
            realización de un concurso).
          </p>
          <h3>Confidencialidad</h3>
          <p>
            La información suministrada por el Usuario tendrá, en todo caso, la consideración de
            confidencial, sin que pueda ser utilizada para otros fines distintos a los aquí
            descritos. “Ontább” se obliga a no divulgar ni revelar información sobre las
            pretensiones del Usuario, los motivos del asesoramiento solicitado o la duración de su
            relación con éste.
          </p>
          <h3>Validez</h3>
          <p>
            Esta política de privacidad y de protección de datos ha sido redactada por MASTERS
            LOPD®, empresa de RGPD, a día 02 de marzo de 2020, y podrá variar en función de los
            cambios de normativa y jurisprudencia que se vayan produciendo, siendo responsabilidad
            del titular de los datos la lectura del documento actualizado, en orden a conocer sus
            derechos y obligaciones al respecto en cada momento.
          </p>
          <h3>POLÍTICA DE COOKIES</h3>
          <p>
            En cumplimiento de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la
            Información y Comercio Electrónico informamos al Usuario de la utilización de cookies en
            la web de “Ontább”.
          </p>
          <p>
            Una cookie es un fichero que se descarga en el ordenador o el dispositivo del Usuario
            (smartphone, tableta, televisión conectada….) al acceder a determinadas páginas web. Las
            cookies permiten, entre otras cosas, recopilar información estadística, facilitar
            ciertas funcionalidades técnicas, almacenar y recuperar información sobre los hábitos de
            navegación o preferencias de un Usuario o de su equipo y, dependiendo de la información
            que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer
            al Usuario, recordar aspectos del Usuario como su idioma, país, navegador, etc. En caso
            de no querer recibir cookies, el Usuario podrá configurar su navegador para que las
            borre del disco duro de su ordenador, las bloquee o le avise en su caso de instalación
            de las mismas. Para continuar sin cambios en la configuración de las cookies,
            simplemente, continúa navegando en la página web.
          </p>
          <h3>En esta web se utilizan las siguientes cookies:</h3>
          <p>
            Nombre de la cookie Descripción/Finalidad Tipo Caducidad
            baqend-speedkit-session-timestamp, baqend-speedkit-session-id, baqend-speedkit-user-id
          </p>
          <p>
            Esta cookie es usada por el lenguaje de encriptado PHP para permitir que las variables
            de SESIÓN sean guardadas en el servidor web. Esta cookies es esencial para el
            funcionamiento de la web Técnica De Sesión.
          </p>
          <p>
            _ga, _gat, _gid, _gat_UA-77932269-1, _gat_UA-77932269-2 Cookies de Google Analytics.
            Informes estadísticos sobre como los Usuarios encuentran la página web y cómo la
            utilizan: páginas visitadas, tiempo de estancia, tipo de navegador…
          </p>
          <p>_ga y _gid: Se usan para distinguir a los Usuarios.</p>
          <p>_gat: Se usa para limitar el porcentaje de solicitudes</p>
          <h3>Más información</h3>
          <p>Analíticas, de terceros _ga: 2 años</p>
          <p>_gid: 24 horas</p>
          <p>_gat: 10 minutos</p>
          <h3>Configuración de cookies</h3>
          <p>
            El Usuario puede permitir, bloquear o eliminar las cookies instaladas en su equipo
            mediante la configuración de las opciones de su navegador. Puedes encontrar información
            sobre cómo hacerlo, en relación con los navegadores más comunes, en los links que se
            incluyen a continuación:
          </p>
          <p>
            -Para Internet Explorer™ -Para Safari™ -Para Chrome™ -Para Firefox™ -Para Opera™ -Para
            Edge™
          </p>
          <h3>Aceptación de cookies</h3>
          <p>
            Al acceder a este sitio web por primera vez, verás una ventana dónde se informa de la
            utilización de las cookies y donde puedes consultar esta “Política de cookies”. Puedes
            cambiar la configuración de cookies en cualquier momento, configurando el navegador para
            aceptar, o no, las cookies que recibes o para que el navegador te avise cuando un
            servidor quiera guardar una cookie. Te informamos de que, en el caso de bloquear o no
            aceptar la instalación de cookies, es posible que ciertos servicios no estén disponibles
            sin la utilización de éstas o que no pueda acceder a determinados servicios ni tampoco
            aprovechar por completo todo lo que nuestra web ofrece.
          </p>
          <p>
            “Ontább” te agradece que consientas la aceptación de cookies, esto nos ayuda a obtener
            datos más precisos que nos permiten mejorar el contenido y el diseño de nuestras páginas
            webs para adaptarlas a tus preferencias.{' '}
          </p>
        </div>
        <ion-row class="footer-homepage">
          <ion-col>
            <div className="container">
              <a href="https://www.instagram.com/myontabb/" target="blank">
                <img src="/images/instagram.svg" className="social-icon" />
              </a>
              <a href="#">
                <img src="/images/linkedin.svg" className="social-icon" />
              </a>
            </div>
          </ion-col>
        </ion-row>
      </ion-grid>
    </ion-content>
  </>
);
