import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { Header } from 'components';
import { connect } from 'react-redux';
import { authActions } from 'store/actions';
import './index.scss';

class ComfirmPin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: [null, null, null, null],
      confirm: [null, null, null, null],
      error: {
        status: false,
        msg: ''
      }
    };
    this.element = [];
  }

  checkActive = (array, index) => {
    const value = this.state[array][index];
    if (_.isNumber(value)) {
      return 'active';
    }
    return '';
  };

  onKeyPress = (type, index, e) => {
    e.preventDefault();
    const keyCode = e.keyCode || e.which;
    const value = String.fromCharCode(keyCode);
    if (e.keyCode === 8) {
      if (index === 3 && this.state[type][3] !== null) {
        index += 1;
      }
      let move = true;
      if (this.state.confirm[3] !== null) {
        move = false;
      }
      this.setState(
        {
          [type]: _.map(this.state[type], (item, i) => {
            if (index - 1 === i) {
              return null;
            }
            return item;
          })
        },
        () => {
          if (type === 'confirm') {
            index += 4;
          }
          if (index - 1 >= 0 && move) {
            this.element[index - 1].focus();
          }
        }
      );
    } else if (keyCode >= 48 && keyCode <= 57) {
      this.setState(
        {
          [type]: _.map(this.state[type], (item, i) => {
            if (index === i) {
              return parseInt(value);
            }
            return item;
          })
        },
        () => {
          if (!_.includes(this.state.confirm, null) && !_.includes(this.state.params, null)) {
            this.onSubmit();
          }
          if (type === 'confirm') {
            index = index + 4;
          }
          if (index + 1 <= 7) {
            this.element[index + 1].focus();
          }
        }
      );
    }
  };

  onSubmit = () => {
    this.props.onSubmit(this.state.params, this.state.confirm, this.success, this.fail);
  };

  success = () => {
    history.push('pin');
  };

  fail = (message = '! Revisa que coincida tu PIN !') => {
    this.setState({
      error: {
        status: true,
        msg: message
      }
    });
  };

  render() {
    const { error, params, confirm } = this.state;
    return (
      <React.Fragment>
        <Header />
        <ion-content class="confirm-pin-container">
          <ion-grid class="otp-page">
            <ion-row class="ion-text-left">
              <ion-col>
                <p className="title">Hola!</p>
              </ion-col>
            </ion-row>
            <ion-row class="text-sub-confirm ion-text-center">
              <p>Crea un PIN que te sea fácil de recordar para poder acceder a tu sesión</p>
            </ion-row>
            <ion-row class="from-otp-confirm">
              {_.map(params, (item, key) => (
                <input
                  key={key}
                  type="tel"
                  maxLength="1"
                  ref={n => {
                    this.element[key] = n;
                  }}
                  className={this.checkActive('params', key)}
                  onKeyDown={e => this.onKeyPress('params', key, e)}
                />
              ))}
            </ion-row>
            <ion-row class="ion-text-left text-sub">
              <p>Repite el PIN</p>
            </ion-row>
            <ion-row class="from-otp-confirm">
              {_.map(confirm, (item, key) => (
                <input
                  key={key}
                  type="tel"
                  maxLength="1"
                  ref={n => {
                    this.element[key + 4] = n;
                  }}
                  className={this.checkActive('confirm', key)}
                  onKeyDown={e => this.onKeyPress('confirm', key, e)}
                />
              ))}
            </ion-row>
            {error.status && (
              <ion-row class="wapper-mess">
                <ion-col class="ion-text-center">
                  <p className="text-err">{error.msg}</p>
                </ion-col>
              </ion-row>
            )}
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.updatePin
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ComfirmPin);
