import React from 'react';
import { authActions, alertActions } from 'store/actions';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { initialValues, validationSchema } from './validate';

class Subcribe extends React.Component {
  onSubmit = async (values, actions) => {
    const { onSubmit, openAlert } = this.props;

    try {
      actions.setSubmitting(true);

      await onSubmit(values);
      openAlert('Gracias por dejarnos tus datos, nos pondremos en contacto contigo lo antes posible');
      actions.resetForm();
    } catch (e) {
      actions.setErrors({
        email: e.message || e
      });
    } finally {
      actions.setSubmitting(false);
    }
  };

  render() {
    return (
      <div className={this.props.className}>
        <Formik
          initialValues={initialValues}
          validateOnBlur={false}
          validateOnChange={false}
          validationSchema={validationSchema}
          onSubmit={this.onSubmit}
        >
          {props => (
            <form onSubmit={props.handleSubmit}>
              <input
                type="text"
                name="message"
                onChange={props.handleChange}
                placeholder="por ej: 612345678 y/o info@tumail.com"
                required
              />
              <button type="submit">Enviar</button>
            </form>
          )}
        </Formik>

        <h4>¿Eres responsable de una escuela? Te llamamos.</h4>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.subcribe,
  openAlert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Subcribe);
