import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Header } from 'components';
import history from 'utils/history';
import Subcribe from './Subcribe';
import './index.scss';
import { IonPopover, IonCol, IonToggle } from '@ionic/react';

class HomePage extends Component {
  constructor(props) {
    super();
    this.anchorTarget = null;
    this.anchorTarget1 = null;
    this.state = {
      popup: false,
      price: false
    };
  }
  componentDidMount() {
    this.anchorTarget = document.getElementById('guarderia');
    this.anchorTarget1 = document.getElementById('padres');
  }
  handleClick(e) {
    e.preventDefault();
    this.anchorTarget.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }
  handleClick1(e) {
    e.preventDefault();
    this.anchorTarget1.scrollIntoView({ behavior: 'smooth', block: 'start' });
  }
  render() {
    const { price } = this.state;
    return (
      <React.Fragment>
        <Header login subcribe onClick={() => this.setState({ popup: true })}>
          <Subcribe className="subcribe-form" />
        </Header>
        <IonPopover isOpen={this.state.popup} onDidDismiss={() => this.setState({ popup: false })}>
          <Subcribe className="subcribe-form mobile" />
        </IonPopover>
        <ion-content class="homepage">
          <ion-grid class="main-contnet">
            <div className="container">
              <ion-row class="banner">
                <ion-col class="text-banner">
                  <ion-text>Mucho más que una agenda electrónica</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="9" class="text-introduce">
                  <ion-text>
                    OntáBb apoya la educación en centros infantiles mediante la comunicación en
                    tiempo real de las actividades realizadas por el/la niñ@.
                  </ion-text>
                </ion-col>
                <ion-col size="3" class="text-icon desktop-fixed-first">
                  <a
                    href="#guarderia"
                    onClick={e => this.handleClick(e)}
                    aria-label="Scroll to guarderia"
                  >
                    <img src="/images/icon/tablero.svg" alt="logo" />
                    <ion-text>Directores y Educadores</ion-text>
                  </a>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="9" class="text-introduce">
                  <h2>
                    Tanto familiares como las escuelas no perderán el más mínimo detalle del día a
                    día de sus peques.
                  </h2>
                </ion-col>
                <ion-col size="3" class="text-icon desktop-fixed-second">
                  <a
                    href="#padres"
                    onClick={e => this.handleClick1(e)}
                    aria-label="Scroll to padres"
                  >
                    <img src="/images/icon/familia.svg" alt="logo" />
                    <ion-text>Padres</ion-text>
                  </a>
                </ion-col>
              </ion-row>
              <ion-row class="text-title">
                <ion-col>
                  <h1 id="guarderia">Si estás a cargo de una escuela infantil podrás...</h1>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/pen.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>
                    Reducir el papeleo. Digitalizar los reportes diarios y compartirlos con los
                    familiares en tiempo real.
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/poblacion.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Incrementar las matrículas. Diferencia tu centro del resto.</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/save-time.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>
                    Ahorrar tiempo. No más reportes interminables por escrito que acaban en la
                    basura.
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/multitask.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Compartir los eventos y tareas programadas.</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/medalla.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Satisfacer a los padres más exigentes.</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="text-title">
                <ion-col>
                  <ion-text>¿Cómo empezar ?</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <div className="icon styles1">
                    <div className="icon-wrapper">
                      <img src="/images/1.svg" className="icon-1" />
                      <p className="text-icon">
                        La persona responsable del centro debe contactar previamente con nuestro
                        equipo para facilitarle el acceso.
                      </p>
                    </div>
                  </div>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <div className="icon styles2">
                    <div className="icon-wrapper">
                      <img src="/images/2.svg" className="icon-1" />
                      <p className="text-icon">
                        El Responsable añadirá a los grupos de niñ@s y personal del Staff, quienes
                        recibirán un mail de invitación.
                      </p>
                    </div>
                  </div>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <div className="icon styles3">
                    <div className="icon-wrapper">
                      <img src="/images/3.svg" className="icon-1" />
                      <p className="text-icon">
                        A través del enlace recibido, podrán acceder a la plataforma y gestionar los
                        grupos.
                      </p>
                    </div>
                  </div>
                </ion-col>
              </ion-row>
              <ion-row class="text-title">
                <ion-col>
                  <ion-text>Precio y ventajas</ion-text>
                </ion-col>
              </ion-row>

              <ion-row class="box-option " justify-content-center>
                <IonCol class="basica box-option-column" sizeMd="4">
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Acceso seguro</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Envío de información y multimedia en tiempo real</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Registro de asistencia</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Agenda</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Historial curso actual</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Personal de guardería ilimitados</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Padres y receptores ilimitados</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>Soporte telefónico/mail</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="option-item">
                    <ion-col class="icon text-utilities">
                      <img src="/images/icon/Vector.svg" alt="logo" />
                      <ion-text>1 terminal móvil por educador/a<br />(sujeto a condiciones)</ion-text>
                    </ion-col>
                  </ion-row>
                  <div className="btn-bottom">
                    <ion-text class="text-momney ion-text-center">
                      <p>
                        <div className="toggle-price">
                          mensual
                          <IonToggle
                            checked={price}
                            color="danger"
                            onIonChange={e => this.setState({ price: e.detail.checked })}
                          />
                          curso
                        </div>
                        {!price ? 1 : 8}€ <span>{!price ? '/niñ@/mes' : '/niñ@/curso'}</span>
                      </p>
                    </ion-text>
                    <ion-row class="btn-submit">
                      <ion-button
                        class="button-buy"
                        color="danger"
                        onClick={() => history.push('contact')}
                      >
                        Prueba 1 mes gratis
                      </ion-button>
                    </ion-row>
                  </div>
                </IonCol>
              </ion-row>

              <ion-row class="text-title">
                <ion-col>
                  <ion-text id="padres">Si tu niñ@ está en una guardería podrás...</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/time.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Saber lo que hace en tiempo real</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/camara-de-fotos.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Recibir fotos/videos</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/charlar (1).svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Mantener una comunicación fluida con la guardería</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/fecha.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>
                    Apoyar actividades extraescolares y no olvidarte de ninguna gracias a la agenda
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/prueba.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>
                    Dar autorizo para que otro adulto recoja al niñ@ y ser notificado
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/caja-de-almuerzo.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Saber qué y cuánto ha comido tu niñ@</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="box-introduce">
                <ion-col size="2" class="icon icon-left">
                  <img src="/images/icon/orinal.svg" alt="logo" />
                </ion-col>
                <ion-col size="10" class="text-utilities">
                  <ion-text>Estar al tanto de las deposiciones y los cambios de pañales.</ion-text>
                </ion-col>
              </ion-row>

              <ion-row class="text-title">
                <ion-col>
                  <ion-text>¿Cómo empezar?</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <div className="icon styles3">
                    <div className="icon-wrapper">
                      <img src="/images/1.svg" className="icon-1" />
                      <p className="text-icon">
                        Una vez el personal de la guardería se ponga en contacto con nuestro equipo
                        les facilitaremos acceso.
                      </p>
                    </div>
                  </div>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <div className="icon styles1">
                    <div className="icon-wrapper">
                      <img src="/images/2.svg" className="icon-1" />
                      <p className="text-icon">
                        El Director/a creará el perfil de tu hij@ y te enviará un mail de
                        bienvenida.
                      </p>
                    </div>
                  </div>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <div className="icon styles2">
                    <div className="icon-wrapper">
                      <img src="/images/3.svg" className="icon-1" />
                      <p className="text-icon">
                        A través del enlace recibido, podrás acceder a la plataforma y gestionar el
                        perfil de tu hij@ y luego enterarte de su día a día sin perderte ni un
                        segundo.
                      </p>
                    </div>
                  </div>
                </ion-col>
              </ion-row>

              <ion-row class="text-bottom">
                <ion-col class="ion-text-center">
                  <ion-text>Si OntáBb te parece útil y emocionante...</ion-text>
                </ion-col>
              </ion-row>

              <ion-row class="btn-sned">
                <a
                  href="mailto:?subject=Mírate esta novedosa web&amp;body=Hola, te recomiendo usar OntáBb pues es una novedosa web para mejorar la interacción entre padres y guarderías. A los padres les permite conocer mejor el día de sus peques y a los trabajadores de centros les ahorra un montón de trabajo y organiza su día a día. Conóceles más en el siguiente link www.myontabb.com. Un saludo."
                  title="Share by Email"
                  className="btn btn-danger"
                  color="danger"
                >
                  Recomiéndala
                </a>
              </ion-row>
            </div>
            <ion-row class="footer-homepage">
              <ion-col>
                <div className="container">
                  <a href="mailto:info@myontabb.com" target="blank">
                    Contáctanos en info@myontabb.com
                  </a>
                  <div style={{ display: 'flex', flexDirection: 'row' }}>
                    <a href="https://www.instagram.com/myontabb/" target="blank">
                      <img src="/images/instagram.svg" className="social-icon" />
                    </a>
                    <a href="https://www.linkedin.com/company/ontabb" target="blank">
                      <img src="/images/linkedin.svg" className="social-icon" />
                    </a>
                  </div>
                </div>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

HomePage.propTypes = {};

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
