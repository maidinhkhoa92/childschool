import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { IonApp } from '@ionic/react';
import { Loading } from 'components';
// import style

import 'style/global.scss';
import 'style/form.scss';
import '@ionic/core/css/core.css';
import '@ionic/core/css/ionic.bundle.css';
import 'style/index.scss';
import 'style/activities/actividades.scss';
import 'style/activities/bano.scss';
import 'style/activities/biberon.scss';
import 'style/activities/comidas.scss';
import 'style/activities/incidentes.scss';
import 'style/activities/logros.scss';
import 'style/activities/media.scss';
import 'style/activities/medicinas.scss';
import 'style/activities/SelectStudent.scss';
import 'style/activities/siesta.scss';
import 'style/responsive.scss';

const RootContainer = props => {
  return (
    <IonApp>
      <Suspense fallback={<Loading />}>{props.children}</Suspense>
    </IonApp>
  );
};
RootContainer.propTypes = {
  children: PropTypes.node
};
export default RootContainer;
