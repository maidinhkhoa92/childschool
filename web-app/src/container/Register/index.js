import React from 'react';
import { Header, Loading } from 'components';
import { connect } from 'react-redux';
import { IonButton } from '@ionic/react';
import { authActions, alertActions } from 'store/actions';
import history from 'utils/history';
import './index.scss';

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {
        email: '',
        password: '',
        confirm: ''
      },
      loading: false,
      agree: true
    };
  }

  onChange = e => {
    const value = e.target.value;
    const key = e.target.name;

    this.setState({
      params: {
        ...this.state.params,
        [key]: value
      }
    });
  };

  agreed = () => {
    this.setState({
      agree: !this.state.agree
    });
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        history.push('/comfirm-pin');
      }
    );
  };

  fail = e => {
    this.setState({
      loading: false
    });
    this.props.openAlert(e);
  };

  onSubmit = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.onSubmit(this.state.params, this.success, this.fail);
      }
    );
  };

  checkDisable = () => {
    const { agree, params } = this.state;
    if (!agree && params.email !== '' && params.password !== '' && params.confirm !== '') {
      return false;
    }
    return true;
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <ion-content class="register-page">
          <ion-grid class="otp-page">
            <ion-row>
              <div className="form-acceso">
                <p className="title-form ion-text-center">Registro</p>
                <input
                  type="text"
                  placeholder="Email"
                  name="email"
                  required
                  onChange={e => this.onChange(e)}
                />
                <input
                  type="password"
                  placeholder="Define una contraseña"
                  name="password"
                  required
                  onChange={e => this.onChange(e)}
                />
                <input
                  type="password"
                  placeholder="Repite contraseña"
                  name="confirm"
                  required
                  onChange={e => this.onChange(e)}
                />
                <label className="check-box">
                  <input type="checkbox" name="checkBox" required onChange={() => this.agreed()} />
                  <span className="checkmark" />
                  <ion-text>
                    Estoy de acuerdo con los{' '}
                    <a className="text" href="/terms-and-conditions" target="blank">
                      términos y condiciones
                    </a>{' '}
                    y la{' '}
                    <a className="text" href="/privacy-policy" target="blank">
                      política de privacidad
                    </a>{' '}
                    de Ontább
                  </ion-text>
                </label>
                <ion-row class="btn-wapper">
                  {this.state.loading ? (
                    <Loading />
                  ) : (
                    <IonButton
                      onClick={() => this.onSubmit()}
                      color="danger"
                      type="submit"
                      disabled={this.checkDisable()}
                    >
                      Regístrate
                    </IonButton>
                  )}
                </ion-row>
              </div>
            </ion-row>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.updatePassword,
  openAlert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);
