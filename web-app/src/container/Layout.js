import React from 'react';
import { IonToast } from '@ionic/react';
import { connect } from 'react-redux';
import { alertActions } from 'store/actions';

const Layout = ({ children, status, message, close }) => {
  return (
    <React.Fragment>
      <IonToast
        isOpen={status}
        duration={2000}
        onDidDismiss={() => close()}
        message={message}
        position="top"
      />
      {children}
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    status: state.Alert.status,
    message: state.Alert.message
  };
};

const mapDispatchToProps = {
  close: alertActions.close
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);
