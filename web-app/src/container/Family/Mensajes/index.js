import React, { Component } from 'react';
import './index.scss';
import { HeaderBack, Loading } from 'components';
import { connect } from 'react-redux';
import { messageActions } from 'store/actions';
import { FormatPastTime } from 'utils/moment';
import truncate from 'utils/truncate';
import history from 'utils/history';
import _ from 'lodash';

class Mensajes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    const { class_id } = this.props;
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch({ class_id: class_id }, this.success, this.fail);
      }
    );
  }
  success = () => {
    this.setState({
      loading: false
    });
  };
  fails = () => {
    this.setState({
      loading: false
    });
  };
  renderMessage = () => {
    return _.map(this.props.list, (item, key) => (
      <React.Fragment key={key}>
        <ion-row
          class="row-content"
          onClick={() =>
            history.push({
              pathname: `/family/chat/${item.firestore}/${item.id}`,
              state: { type: item.type, name: item.from ? item.from.profile.firstName : '' }
            })
          }
        >
          <ion-img
            src={
              item.to[0].profile && item.to[0].profile.photo
                ? item.to[0].profile.photo
                : '/images/children.png'
            }
            class="icon"
          />
          {this.renderUser(item.from, item.to, item.updated_at, item.message)}
        </ion-row>
      </React.Fragment>
    ));
  };
  renderUser(from, to, updatedDate, message) {
    const { info } = this.props;
    let name = from ? from.profile.firstName : '';
    if (from && from._id === info.id) {
      name = to.length > 0 ? to[0].profile.firstName : '';
    }
    return (
      <ion-col>
        <ion-row class="text1">
          <div>{name}</div>
          <div>{FormatPastTime(updatedDate)}</div>
        </ion-row>
        <ion-row class="text2-phu">
          <div>{truncate(message, 50)}</div>
        </ion-row>
      </ion-col>
    );
  }
  render() {
    if (this.state.loading) {
      return <Loading />;
    }
    return (
      <React.Fragment>
        <HeaderBack title="Mensajes" />
        <ion-content class="content-family-Mensajes">
          <ion-grid class="centent">{this.renderMessage()}</ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    list: state.Message.list,
    class_id: state.Child.detail.classes,
    info: state.Auth.infor
  };
};

const mapDispatchToProps = {
  fetch: messageActions.get
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Mensajes);
