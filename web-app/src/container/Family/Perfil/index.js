import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { Loading, HeaderFamily, FooterFamily } from 'components';
import { connect } from 'react-redux';
import { childActions, classesActions, authActions } from 'store/actions';
import { convertIcon, convertTime } from 'utils/convert';
import EditChild from './Edit';
import { FormatTime } from 'utils/moment';
import './index.scss';

class Perfil extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      popup: false,
      phoneStatus: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.props.match.params.id, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  renderNews = news => {
    let showTime = false;
    let currentTime = '';
    return _.map(news, (item, i) => {
      const formatTime = FormatTime(item.time);
      if (currentTime !== formatTime) {
        showTime = true;
        currentTime = formatTime;
      } else {
        showTime = false;
      }
      return (
        <React.Fragment key={i}>
          {showTime && (
            <ion-row class="date-now">
              <div className="date-wapper">
                <p>{formatTime}</p>
              </div>
            </ion-row>
          )}
          <ion-row class="box-news">
            <ion-col size="2" class="box-left">
              <ion-img src={convertIcon(item.type)} />
            </ion-col>
            <ion-col size="10" class="box-right">
              <ion-row class="text-right">
                <ion-text>{item.title}</ion-text>
                <ion-text class="ion-text-right">{convertTime(item.created_at)}</ion-text>
              </ion-row>
              <ion-row>
                <ion-text class="text-mid">{_.join(item.note, ',')}</ion-text>
              </ion-row>
              <ion-row>
                <ion-text class="text-mid">{_.join(item.selected, ',')}</ion-text>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{this.checkGroup(item.group, item.type)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-text class="text-bottom">
                  {this.checkContent(item.content, item.type)}
                </ion-text>
              </ion-row>
            </ion-col>
          </ion-row>
        </React.Fragment>
      );
    });
  };

  checkContent = (content, type) => {
    if (type === 'Photo' || type === '') {
      return <img src={content} />;
    }
    if (type === 'Video') {
      return (
        <video width="100%" height="240" controls>
          <source src={content} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      );
    }
    return content;
  };

  checkGroup = (content, type) => {
    if (type === 'Achievements' || type === 'CheerUp') {
      return <img src={content} />;
    }
    if (type === 'FeedingBottle') {
      return content + ' ml';
    }
    return content;
  };

  // child action
  onShowPopup = value => {
    this.setState({
      popup: value
    });
  };

  togglePhone = value => {
    this.setState(
      {
        phoneStatus: value
      },
      () => {
        document.addEventListener('click', this.onCloseToggle);
      }
    );
  };
  onCloseToggle = () => {
    this.setState(
      {
        phoneStatus: false
      },
      () => {
        document.removeEventListener('click', this.onCloseToggle);
      }
    );
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img className="wapper-avatar" src={url} alt="logo" />;
    }
    return (
      <div className="wapper-avatar">
        <ion-img src="/images/icon/bebe.svg" allt="avatar" />
      </div>
    );
  };

  logout = () => {
    this.props.logout(() => {
      history.push('/');
    });
  };

  render() {
    const { detail, childs } = this.props;
    const { loading, popup, phoneStatus } = this.state;
    if (loading) {
      return <Loading />;
    }
    return (
      <React.Fragment>
        <ion-content>
          <HeaderFamily logout={childs.length === 1} right onPress={this.onShowPopup} onLogout={this.logout} />
          <ion-grid class="container">
            <ion-col class="banner-img-perfil">
              {this.showAvatar(detail.profile.photo)}
              <ion-text>
                <p>
                  {detail.profile.firstName} {detail.profile.lastName}
                </p>
              </ion-text>
            </ion-col>
            <div className="all-perfil">{this.renderNews(detail.news)}</div>
          </ion-grid>
          <EditChild status={popup} onClose={() => this.onShowPopup(false)} />
        </ion-content>
        <FooterFamily
          toggle={this.togglePhone}
          phone={phoneStatus}
          first={detail.firstTeacher}
          second={detail.secondTeacher}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    detail: state.Child.detail,
    childs: state.Child.list
  };
};

const mapDispatchToProps = {
  fetch: childActions.detail,
  delete: classesActions.removeChild,
  logout: authActions.logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Perfil);
