import React from 'react';
import { connect } from 'react-redux';
import { Upload } from 'components';
import { childActions, alertActions } from 'store/actions';
import { IonModal, IonButton } from '@ionic/react';
import Calendar from 'react-calendar';
import { FormatTime } from 'utils/moment';

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCalendar: false,
      params: {
        profile: {
          photo: null,
          firstFather: {
            photo: null
          },
          secondFather: {
            photo: null
          },
          firstReceiver: {
            photo: null
          },
          secondReceiver: {
            photo: null
          }
        }
      }
    };
  }

  componentDidMount() {
    this.setState({
      params: {
        profile: {
          ...this.state.params.profile,
          ...this.props.detail.profile
        }
      }
    });
  }

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        profile: {
          ...prev.params.profile,
          [key]: value
        }
      }
    }));
  };

  onChangeSub = (key, e) => {
    const subKey = e.target.name;
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        profile: {
          ...prev.params.profile,
          [key]: {
            ...prev.params.profile[key],
            [subKey]: value
          }
        }
      }
    }));
  };

  onSubmit = () => {
    this.props.onSubmit(this.state.params, this.onSuccess, this.onFail);
  };
  onSuccess = () => {
    this.props.onClose(false);
    this.props.alert('Guardado');
  };
  onFail = message => {
    this.props.alert(message);
  };
  uploadSuccess = value => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        profile: {
          ...prev.params.profile,
          photo: value
        }
      }
    }));
  };
  uploadSuccess1 = value => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        profile: {
          ...prev.params.profile,
          firstFather: {
            ...prev.params.profile.firstFather,
            photo: value
          }
        }
      }
    }));
  };
  uploadSuccess2 = value => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        profile: {
          ...prev.params.profile,
          secondFather: {
            ...prev.params.profile.secondFather,
            photo: value
          }
        }
      }
    }));
  };
  uploadSuccess3 = value => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        profile: {
          ...prev.params.profile,
          firstReceiver: {
            ...prev.params.profile.firstReceiver,
            photo: value
          }
        }
      }
    }));
  };
  uploadSuccess4 = value => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        profile: {
          ...prev.params.profile,
          secondReceiver: {
            ...prev.params.profile.secondReceiver,
            photo: value
          }
        }
      }
    }));
  };

  uploadFail = () => {};

  onPopupCalendar = value => {
    this.setState(
      {
        showCalendar: value
      }
    );
  };

  onChangeDate = date => {
    this.setState(prev => ({
      showCalendar: false,
      params: {
        profile: {
          ...prev.params.profile,
          dob: FormatTime(date)
        }
      }
    }));
  };

  render() {
    const { profile } = this.state.params;
    const { firstFather, secondFather, firstReceiver, secondReceiver } = profile;
    const { status, onClose } = this.props;
    const { showCalendar } = this.state;
    return (
      <IonModal isOpen={status} class="main-content-add-child" onDidDismiss={() => onClose(false)}>
        <button className="close" onClick={() => onClose(false)}>
          <img src="/images/close.svg" />
        </button>
        <ion-content>
          <ion-grid class="main-content-complete">
            <ion-row>
              <ion-col class="ion-text-center ">
                <ion-text class="title">Editar perfil</ion-text>
              </ion-col>
            </ion-row>
            <div className="form-person">
              <Upload photo={profile.photo} success={this.uploadSuccess} fail={this.uploadFail} />
            </div>
            <ion-row>
              <div className="form-person">
                <ion-text class="title-person no-margin">Información personal</ion-text>
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  value={profile.firstName}
                  required
                  onChange={e => this.onChange(e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  value={profile.lastName}
                  required
                  onChange={e => this.onChange(e)}
                />
                <div className="calendar-field" onClick={() => this.onPopupCalendar(true)}>
                  <input
                    type="text"
                    placeholder="Fecha de nacimiento"
                    value={profile.dob}
                    name="dob"
                    required
                    disabled
                  />
                  {showCalendar && <Calendar onChange={this.onChangeDate} />}
                </div>
                <select placeholder="Sexo" name="gender" onChange={e => this.onChange(e)}>
                  <option value="varón">Varón</option>
                  <option value="hembra">Hembra</option>
                </select>
                <label>Alergias</label>
                <input
                  type="text"
                  name="allergies"
                  value={profile.allergies}
                  required
                  onChange={e => this.onChange(e)}
                />
                <label>Medicación (sólo si es permanente)</label>
                <input
                  type="text"
                  value={profile.medication}
                  name="medication"
                  required
                  onChange={e => this.onChange(e)}
                />
                <label>Dirección particular</label>
                <textarea rows="2" name="addess" className="no-margin-bottom" />
                <input
                  type="text"
                  placeholder="Código postal"
                  value={profile.postcode}
                  name="postcode"
                  required
                  onChange={e => this.onChange(e)}
                />
              </div>
            </ion-row>
            <ion-row>
              <div className="form-person parent-top">
                <ion-text class="title-person no-margin">Información de los padres</ion-text>
              </div>
              <div className="form-person parent-top">
                <div className="upload-area">
                  <Upload
                    success={this.uploadSuccess2}
                    fail={this.uploadFail}
                    photo={secondFather.photo}
                    style={{ margin: 0 }}
                  />
                </div>
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  onChange={e => this.onChangeSub('secondFather', e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  onChange={e => this.onChangeSub('secondFather', e)}
                />
                <input
                  type="text"
                  placeholder="Teléfono"
                  name="telephone"
                  required
                  onChange={e => this.onChangeSub('secondFather', e)}
                />
                <input
                  type="text"
                  placeholder="Mail"
                  name="email"
                  required
                  onChange={e => this.onChangeSub('secondFather', e)}
                />
              </div>
            </ion-row>
            <ion-row>
              <div className="form-person parent-top">
                <ion-text class="title-person no-margin">Receptores autorizados</ion-text>
                <div className="upload-area">
                  <div className="upload-file">Receptor 1</div>
                  <Upload
                    success={this.uploadSuccess3}
                    fail={this.uploadFail}
                    photo={firstReceiver.photo}
                    style={{ margin: 0 }}
                  />
                </div>
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  onChange={e => this.onChangeSub('firstReceiver', e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  onChange={e => this.onChangeSub('firstReceiver', e)}
                />
                <input
                  type="text"
                  placeholder="Teléfono"
                  name="telephone"
                  required
                  onChange={e => this.onChangeSub('firstReceiver', e)}
                />
                <input
                  type="text"
                  placeholder="Mail"
                  name="email"
                  required
                  onChange={e => this.onChangeSub('firstReceiver', e)}
                />
              </div>
              <div className="form-person parent-top">
                <div className="upload-area">
                  <div className="upload-file">Receptor 2</div>
                  <Upload
                    success={this.uploadSuccess4}
                    fail={this.uploadFail}
                    photo={secondReceiver.photo}
                    style={{ margin: 0 }}
                  />
                </div>
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  onChange={e => this.onChangeSub('secondReceiver', e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  onChange={e => this.onChangeSub('secondReceiver', e)}
                />
                <input
                  type="text"
                  placeholder="Teléfono"
                  name="telephone"
                  required
                  onChange={e => this.onChangeSub('secondReceiver', e)}
                />
                <input
                  type="text"
                  placeholder="Mail"
                  name="email"
                  required
                  onChange={e => this.onChangeSub('secondReceiver', e)}
                />
              </div>
            </ion-row>
            <ion-row class="btn-wapper">
              <IonButton onClick={() => this.onSubmit()} color="danger" type="submit">
                Guardar
              </IonButton>
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {
    detail: state.Child.detail
  };
};

const mapDispatchToProps = {
  onSubmit: childActions.update,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Edit);
