import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { Loading } from 'components';
import { childActions, authActions } from 'store/actions';
import './index.scss';

class Vistagroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch({}, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };
  logout = () => {
    this.props.logout(() => {
      history.push('/');
    });
  };

  renderChild = () => {
    return _.map(this.props.childs, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => history.push('/family/perfil/' + item.id)}>
          <div className="wapper-img">{this.showAvatar(item.profile.photo)}</div>
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img className="img-icon avatar" src={url} alt="logo" />;
    }
    return <ion-img class="img-icon" src="/images/child.svg" alt="logo" />;
  };

  render() {
    const { loading } = this.state;
    const { childs } = this.props;
    if(childs.length === 1) {
      history.push('/family/perfil/' + childs[0].id)
    }
    return ( 
      <React.Fragment>
        <ion-content class="vistagroup-wrapper">
          <ion-button class="logoutButton" onClick={() => this.logout()}>
            <ion-img src="/images/logout.svg" alt="logo" class="icon-back" />
          </ion-button>
          <ion-grid class="vistagroup">
            <ion-row class="wapper-item">{loading ? <Loading /> : this.renderChild()}</ion-row>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    childs: state.Child.list
  };
};

const mapDispatchToProps = {
  fetch: childActions.get,
  logout: authActions.logout
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Vistagroup);
