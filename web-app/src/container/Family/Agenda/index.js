import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import { FormatTime, FormatMonth } from 'utils/moment';
import { HeaderMenu } from 'components';
import { eventActions, menuActions } from 'store/actions';
import './index.scss';

class Lannister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: {
        date: new Date()
      },
      loading: false
    };
  }

  componentDidMount() {
    const { class_id, director_id } = this.props;
    this.setState(
      prev => ({
        query: {
          ...prev.query,
          class_id: class_id
        }
      }),
      () => {
        this.props.fetchMenu({ director: director_id }, this.success, this.fail);
        this.props.fetch(this.state.query, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onChangeDate = (status = true) => {
    let time = moment(this.state.query.date);
    if (status === true) {
      time = time.add(1, 'months');
    } else {
      time = time.subtract(1, 'months');
    }
    this.setState(
      prev => ({
        query: {
          ...prev.query,
          date: time
        }
      }),
      () => {
        this.props.fetch(this.state.query, this.success, this.fail);
      }
    );
  };

  renderEvent = () => {
    return _.map(this.props.events, (item, key) => (
      <React.Fragment key={key}>
        <ion-row class="row2">
          <span className="text-row2">{FormatTime(item.date)}</span>
        </ion-row>

        <ion-row class="row3">
          <ion-row class="row3-1">
            <span className="text-row3">{item.name}</span>
            <span className="textin-row3">desde</span>
            <span className="text-row3">{item.startTime} hrs</span>
            <span className="textin-row3">hasta</span>
            <span className="text-row3">{item.endTime} hrs</span>
          </ion-row>

          <ion-row class="row3-2">
            <span className="textin-row3">Grupo: </span>
            <span className="text-row3">{item.group.name}</span>
          </ion-row>

          <ion-row class="row3-3">
            <span className="textin-row3">Nota: </span>
            <span className="text-row3">{item.note}</span>
          </ion-row>
        </ion-row>
      </React.Fragment>
    ));
  };

  render() {
    const { query } = this.state;
    const { menu } = this.props;
    return (
      <React.Fragment>
        <HeaderMenu title="Agenda" url={menu} />
        <ion-content class="staff-lannister">
          <ion-grid>
            <ion-row class="row1">
              <ion-img
                src="/images/left.svg"
                class="icon-row1"
                onClick={() => this.onChangeDate(false)}
              />
              <ion-text class="text-row1">{FormatMonth(query.date)}</ion-text>
              <ion-img
                src="/images/right.svg"
                class="icon-row1"
                onClick={() => this.onChangeDate(true)}
              />
            </ion-row>
            {this.renderEvent()}
            {/* <ion-row>
              <ion-col class="ion-text-center">
                <a href={menu} target="blank">
                  <ion-img src="/images/group.svg" />
                </a>
              </ion-col>
            </ion-row> */}
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    events: state.Event.list,
    class_id: state.Child.detail.classes,
    director_id: state.Child.detail.directorId,
    menu: state.Menu.url ? state.Menu.url : ''
  };
};

const mapDispatchToProps = {
  fetch: eventActions.get,
  fetchMenu: menuActions.get
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lannister);
