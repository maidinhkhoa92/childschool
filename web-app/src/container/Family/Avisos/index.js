import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { IonButton, IonPopover } from '@ionic/react';
import { HeaderBack, Loading } from 'components';
import { alertActions, messageActions } from 'store/actions';
import Notes from 'utils/notes';
import TimeKeeper from 'react-timekeeper';
import './index.scss';

class Avisos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      params: {
        note: {
          id: 0
        },
        message: '',
        type: 'user'
      },
      modal: false,
      noteId: '',
      time: '12:30',
      notes: []
    };
  }

  componentDidMount() {
    this.setState({
      notes: Notes
    });
  }

  checkNoteCLass = item => {
    const { params } = this.state;
    if (item.id === params.note) {
      return 'note-text active';
    }
    return 'note-text';
  };

  renderNote = () => {
    const { notes } = this.state;
    return _.map(notes, (item, key) => (
      <React.Fragment key={key}>
        <ion-row>
          <ion-col>
            <div className="note-wrapper">
              <div className={this.checkNoteCLass(item)} onClick={() => this.onSelectNote(item.id)}>
                {item.content}
              </div>
              {item.time ? (
                <div onClick={() => this.toggleModal(true, item.id)} className="note-time">
                  {item.timeValue}
                </div>
              ) : (
                item.square && (
                  <input
                    type="text"
                    className="note-time"
                    onChange={e => this.onSetNoteValue(e, item.id)}
                  />
                )
              )}
            </div>
          </ion-col>
        </ion-row>
      </React.Fragment>
    ));
  };

  onSelectNote = item => {
    this.setState(prev => ({
      notes: Notes,
      params: {
        ...prev.params,
        note: item
      }
    }));
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        ...prev.params,
        [key]: value
      }
    }));
  };

  actionSuccess = () => {
    this.setState(
      {
        loading: false,
        modal: false,
        params: {
          note: {
            id: 0
          },
          message: ''
        },
        type: 'user'
      },
      () => {
        this.props.alert('Nota enviada');
        history.goBack();
      }
    );
  };

  actionFail = message => {
    this.props.alert(message);
  };

  setTime = value => {
    this.setState({
      time: value
    });
  };

  toggleModal = (value, noteId = '') => {
    this.setState({
      modal: value,
      noteId: noteId
    });
  };

  onSetTime = () => {
    this.setState(prev => ({
      notes: _.map(prev.notes, note =>
        note.id === prev.noteId ? { ...note, timeValue: prev.time } : note
      ),
      modal: false
    }));
  };

  onSetNoteValue = (e, id) => {
    const value = e.target.value;
    this.setState(prev => ({
      notes: _.map(prev.notes, note => (note.id === id ? { ...note, timeValue: value } : note))
    }));
  };

  onSubmit = () => {
    const { params, notes } = this.state;
    const { user, child } = this.props;
    const Note = _.find(notes, item => item.id === params.note);
    if (Note) {
      const data = {
        ...params,
        note: `${Note.content} ${Note.timeValue}`,
        from_user: user.id,
        to_user: [
          { id: _.isNull(child.firstTeacher) ? '' : child.firstTeacher._id },
          { id: _.isNull(child.firstTeacher) ? '' : child.secondTeacher._id }
        ],
        classes: [{ id: child.classes }]
      };
      this.props.onSubmit(data, this.actionSuccess, this.actionFail);
    } else {
      this.actionFail('Selecciona una nota');
    }
  };

  render() {
    const { modal, loading } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Avisos" />
        <Ion-content>
          <ion-grid class="main-content-avisos">
            {loading ? <Loading /> : this.renderNote()}
          </ion-grid>
        </Ion-content>
        <IonPopover class="large" isOpen={modal} onDidDismiss={() => this.toggleModal(false, '')}>
          <TimeKeeper hour24Mode onChange={newTime => this.setTime(newTime.formatted24)} />
          <div className="actions">
            <ion-text class="text-btn text-right" onClick={() => this.toggleModal(false)}>
              Cancelar
            </ion-text>
            <ion-text class="text-btn" onClick={() => this.onSetTime()}>
              Confirmar
            </ion-text>
          </div>
        </IonPopover>
        <ion-footer>
          <div className="bottom">
            <ion-row>
              <ion-col>
                <textarea
                  name="message"
                  onChange={e => this.onChange(e)}
                  className="message-area-input"
                  placeholder="Nota opcional"
                />
              </ion-col>
            </ion-row>
            <ion-row class="btn-wapper">
              <IonButton onClick={() => this.onSubmit()} color="danger" type="submit">
                Enviar aviso
              </IonButton>
            </ion-row>
          </div>
        </ion-footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    child: state.Child.detail,
    user: state.Auth.infor
  };
};

const mapDispatchToProps = {
  alert: alertActions.open,
  onSubmit: messageActions.create
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Avisos);
