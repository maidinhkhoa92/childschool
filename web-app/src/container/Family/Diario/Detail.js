import React from 'react';
import _ from 'lodash';
import { convertIcon, convertTime } from 'utils/convert';
import { FormatTime } from 'utils/moment';
import { IonModal, IonButton } from '@ionic/react';
import './index.scss';

class Perfil extends React.Component {
  renderNews = news => {
    let showTime = false;
    let currentTime = '';
    return _.map(news, (item, i) => {
      const formatTime = FormatTime(item.time);
      if (currentTime !== formatTime) {
        showTime = true;
        currentTime = formatTime;
      } else {
        showTime = false;
      }
      return (
        <React.Fragment key={i}>
          {showTime && (
            <ion-row class="date-now-perfil">
              <div className="date-wapper">
                <p>{formatTime}</p>
              </div>
            </ion-row>
          )}
          <ion-row class="box-news">
            <ion-col size="2" class="box-left">
              <ion-img src={convertIcon(item.type)} />
            </ion-col>
            <ion-col size="10" class="box-right">
              <ion-row class="text-right">
                <ion-col>
                  <ion-text>{item.title}</ion-text>
                  <ion-text class="ion-text-right">{convertTime(item.time)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{_.join(item.note, ',')}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{this.checkGroup(item.group, item.type)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-bottom">
                    {this.checkContent(item.content, item.type)}
                  </ion-text>
                </ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
        </React.Fragment>
      );
    });
  };

  checkContent = (content, type) => {
    if (type === 'Photo' || type === '') {
      return <img src={content} />;
    }
    if (type === 'Video') {
      return (
        <video width="100%" height="240" controls>
          <source src={content} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      );
    }
    return content;
  };

  checkGroup = (content, type) => {
    if (type === 'Achievements' || type === 'CheerUp') {
      return <img src={content} />;
    }
    if (type === 'FeedingBottle') {
      return content + ' ml';
    }
    return content;
  };

  render() {
    const { list, status, onClose } = this.props;
    return (
      <IonModal isOpen={status} onDidDismiss={() => onClose(false)}>
        <ion-content class="news-modal">
          <IonButton class="close-button" onClick={() => onClose(false)}>
            <ion-img src="/images/close.svg" />
          </IonButton>
          <ion-grid class="container">{this.renderNews(list)}</ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

export default Perfil;
