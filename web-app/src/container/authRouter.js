import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import Layout from './Layout';

export const Normal = props => {
  return (
    <Layout>
      <Route {...props} />
    </Layout>
  );
};

export const Public = props => {
  if (props.isAuthentication) {
    if (props.status === 'Pass confirmed') {
      return <Redirect to="/comfirm-pin" />;
    }
    return <Redirect to="/pin" />;
  }

  return (
    <Layout>
      <Route {...props} />
    </Layout>
  );
};

export const Private = props => {
  if (!props.isAuthentication) {
    return <Redirect to="/acceso" />;
  }
  return (
    <Layout>
      <Route {...props} />
    </Layout>
  );
};
