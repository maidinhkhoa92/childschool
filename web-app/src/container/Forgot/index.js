import React from 'react';
import { Header, Loading } from 'components';
import { connect } from 'react-redux';
import { authActions, alertActions } from 'store/actions';
import history from 'utils/history';
import { Formik } from 'formik';
import './index.scss';
import { initialValues, validationSchema } from './validate';

class Acceso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSubmit = async (values, actions) => {
    const { onSubmit, openAlert } = this.props;
    try {
      actions.setSubmitting(true);

      await onSubmit(values);
      openAlert(
        'Te hemos enviado un correo a la cuenta con la que te has registrado en Ontább, sigue el link y restablece tu contraseña'
      );
    } catch (e) {
      actions.setErrors({
        email: e.message || e
      });
    } finally {
      actions.setSubmitting(false);
    }
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <ion-content class="main-page-acceso">
          <ion-grid class="otp-page">
            <Formik
              initialValues={initialValues}
              validationSchema={validationSchema}
              onSubmit={this.onSubmit}
            >
              {props => (
                <form onSubmit={props.handleSubmit}>
                  {props.errors.email && (
                    <ion-row class="wapper-mess">
                      <ion-col class="ion-text-center">
                        <p className="text-err">{props.errors.email}</p>
                      </ion-col>
                    </ion-row>
                  )}
                  <ion-row>
                    <div className="form-acceso">
                      <p className="title-form ion-text-center">¿Has olvidado tu contraseña?</p>
                      <input
                        type="email"
                        placeholder="Introduce tu email"
                        name="email"
                        required
                        onChange={props.handleChange}
                        className="text-email-acceso"
                      />
                      <ion-row class="btn-wapper">
                        {props.isSubmitting ? (
                          <Loading />
                        ) : (
                          <button type="submit" color="danger" className="form-accede-button">
                            Enviar
                          </button>
                        )}
                      </ion-row>

                      <ion-text class="text-footer ion-text-center">
                        <p onClick={() => history.push('acceso')}>Ve a Acceso</p>
                      </ion-text>
                    </div>
                  </ion-row>
                </form>
              )}
            </Formik>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.forgotPassword,
  openAlert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Acceso);
