import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { Header } from 'components';
import { authActions, alertActions } from 'store/actions';
import './index.scss';

class ChangePin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldDigit: [null, null, null, null],
      newDigit: [null, null, null, null],
      confirmDigit: [null, null, null, null],
      error: {
        status: false,
        msg: ''
      }
    };
    this.element = [];
  }

  checkActive = (array, index) => {
    const value = this.state[array][index];
    if (_.isNumber(value)) {
      return 'active';
    }
    return '';
  };

  onKeyPress = (type, index, e) => {
    e.preventDefault();
    const keyCode = e.keyCode || e.which;
    const value = String.fromCharCode(keyCode);
    if (e.keyCode === 8) {
      if (index === 3 && this.state[type][3] !== null) {
        index += 1;
      }
      let move = true;
      if (this.state.confirmDigit[3] !== null) {
        move = false;
      }
      this.setState(
        {
          [type]: _.map(this.state[type], (item, i) => {
            if (index - 1 === i) {
              return null;
            }
            return item;
          })
        },
        () => {
          if (type === 'newDigit') {
            index += 4;
          }
          if (type === 'confirmDigit') {
            index += 8;
          }
          if (index - 1 >= 0 && move) {
            this.element[index - 1].focus();
          }
        }
      );
    } else if (keyCode >= 48 && keyCode <= 57) {
      this.setState(
        {
          [type]: _.map(this.state[type], (item, i) => {
            if (index === i) {
              return parseInt(value);
            }
            return item;
          })
        },
        () => {
          if (
            !_.includes(this.state.oldDigit, null) &&
            !_.includes(this.state.newDigit, null) &&
            !_.includes(this.state.confirmDigit, null)
          ) {
            this.onSubmit();
          }
          if (type === 'newDigit') {
            index += 4;
          }
          if (type === 'confirmDigit') {
            index += 8;
          }
          if (index + 1 <= 11) {
            this.element[index + 1].focus();
          }
        }
      );
    }
  };

  onSubmit = () => {
    const { oldDigit, newDigit, confirmDigit } = this.state;
    const data = {
      oldDigit,
      newDigit,
      confirmDigit
    };
    this.props.onSubmit(data, this.success, this.fail);
  };

  success = () => {
    this.props.alert('Guardado');
    history.push('pin');
  };

  fail = (message = '! Revisa que coincida tu PIN !') => {
    this.setState({
      error: {
        status: true,
        msg: message
      }
    });
  };

  render() {
    const { oldDigit, newDigit, confirmDigit, error } = this.state;
    return (
      <React.Fragment>
        <Header />
        <ion-content class="change-pin-container">
          <ion-grid class="otp-page">
            <ion-row class="ion-text-left">
              <ion-col>
                <p className="title">Cambiar PIN</p>
              </ion-col>
            </ion-row>
            <ion-row class="ion-text-left">
              <ion-col>
                <p className="text-sub">PIN actual</p>
              </ion-col>
            </ion-row>
            <ion-row class="from-otp-confirm">
              {_.map(oldDigit, (item, key) => (
                <input
                  key={key}
                  type="tel"
                  maxLength="1"
                  ref={n => {
                    this.element[key] = n;
                  }}
                  className={this.checkActive('oldDigit', key)}
                  onKeyDown={e => this.onKeyPress('oldDigit', key, e)}
                />
              ))}
            </ion-row>
            <ion-row class="ion-text-left">
              <ion-col>
                <p className="text-sub">Nuevo PIN</p>
              </ion-col>
            </ion-row>
            <ion-row class="from-otp-confirm">
              {_.map(newDigit, (item, key) => (
                <input
                  key={key}
                  type="tel"
                  maxLength="1"
                  ref={n => {
                    this.element[key + 4] = n;
                  }}
                  className={this.checkActive('newDigit', key)}
                  onKeyDown={e => this.onKeyPress('newDigit', key, e)}
                />
              ))}
            </ion-row>
            <ion-row class="from-otp-confirm from-margin">
              {_.map(confirmDigit, (item, key) => (
                <input
                  key={key}
                  type="tel"
                  maxLength="1"
                  ref={n => {
                    this.element[key + 8] = n;
                  }}
                  className={this.checkActive('confirmDigit', key)}
                  onKeyDown={e => this.onKeyPress('confirmDigit', key, e)}
                />
              ))}
            </ion-row>
            {error.status && (
              <ion-row class="wapper-mess">
                <ion-col class="ion-text-center">
                  <p className="text-err">{error.msg}</p>
                </ion-col>
              </ion-row>
            )}
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.changePin,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangePin);
