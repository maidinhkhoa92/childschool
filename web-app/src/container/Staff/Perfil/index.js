import React from 'react';
import _ from 'lodash';
import { Loading, FooterStaff, HeaderTransparent } from 'components';
import { connect } from 'react-redux';
import { childActions } from 'store/actions';
import { convertIcon, convertTime } from 'utils/convert';
import { FormatTime } from 'utils/moment';
import EditChild from './Edit';
import './index.scss';

class Perfil extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      action: {
        edit: false,
        delete: false
      },
      dateLog: '',
      phoneStatus: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.props.match.params.id, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  renderNews = news => {
    let showTime = false;
    let currentTime = '';
    return _.map(news, (item, i) => {
      const formatTime = FormatTime(item.time);
      if (currentTime !== formatTime) {
        showTime = true;
        currentTime = formatTime;
      } else {
        showTime = false;
      }
      return (
        <React.Fragment key={i}>
          {showTime && (
            <ion-row class="date-now-perfil">
              <div className="date-wapper">
                <p>{formatTime}</p>
              </div>
            </ion-row>
          )}
          <ion-row class="box-news">
            <ion-col size="2" class="box-left">
              <ion-img src={convertIcon(item.type)} />
            </ion-col>
            <ion-col size="10" class="box-right">
              <ion-row class="text-right">
                <ion-col>
                  <ion-text>{item.title}</ion-text>
                  <ion-text class="ion-text-right">{convertTime(item.time)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{_.join(item.note, ',')}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{_.join(item.selected, ',')}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{this.checkGroup(item.group, item.type)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-bottom">
                    {this.checkContent(item.content, item.type)}
                  </ion-text>
                </ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
        </React.Fragment>
      );
    });
  };

  checkContent = (content, type) => {
    if (type === 'Photo' || type === '') {
      return <img src={content} />;
    }
    if (type === 'Video') {
      return (
        <video width="100%" height="240" controls>
          <source src={content} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      );
    }
    return content;
  };

  checkGroup = (content, type) => {
    if (type === 'Achievements' || type === 'CheerUp') {
      return <img src={content} />;
    }
    if (type === 'FeedingBottle') {
      return content + ' ml';
    }
    return content;
  };
  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return (
        <img
          src={url}
          allt="avatar"
          onClick={() => this.onShowAction('edit')}
          className="wapper-avatar"
        />
      );
    }
    return (
      <div className="wapper-avatar">
        <ion-img
          src="/images/icon/bebe.svg"
          allt="avatar"
          onClick={() => this.onShowAction('edit')}
        />
      </div>
    );
  };

  togglePhone = value => {
    this.setState(
      {
        phoneStatus: value
      },
      () => {
        document.addEventListener('click', this.onCloseToggle);
      }
    );
  };
  onCloseToggle = () => {
    this.setState(
      {
        phoneStatus: false
      },
      () => {
        document.removeEventListener('click', this.onCloseToggle);
      }
    );
  };

  onShowAction = key => {
    this.setState(prev => ({
      action: {
        ...prev.action,
        [key]: true
      }
    }));
  };
  onActionClose = () => {
    this.setState({
      action: {
        edit: false,
        delete: false
      }
    });
  };

  render() {
    const { detail } = this.props;
    const { loading, phoneStatus, action } = this.state;
    if (loading) {
      return <Loading />;
    }
    return (
      <React.Fragment>
        <ion-content>
          <HeaderTransparent />
          <ion-grid class="container">
            <ion-col class="banner-img-perfil">
              {this.showAvatar(detail.profile.photo)}
              <ion-text>
                <p onClick={() => this.onShowAction('edit')}>
                  {detail.profile.firstName} {detail.profile.lastName}
                </p>
              </ion-text>
            </ion-col>
            <div className="all-perfil">{this.renderNews(detail.news)}</div>
          </ion-grid>
          <EditChild status={action.edit} onClose={this.onActionClose} />
        </ion-content>
        <FooterStaff toggle={this.togglePhone} phone={phoneStatus} first={detail.family} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    detail: state.Child.detail
  };
};

const mapDispatchToProps = {
  fetch: childActions.detail
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Perfil);
