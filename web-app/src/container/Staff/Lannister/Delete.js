import React from 'react';
import { IonPopover } from '@ionic/react';

const Delete = ({ status, onClose, onSubmit, name }) => {
  return (
    <IonPopover isOpen={status} cssClass="popup-edit-class" onDidDismiss={() => onClose()}>
      <ion-content>
        <ion-grid>
          <ion-row>
            <ion-col>
              <p className="desc">
                ¿Estás segur@ que quieres borrar el evento de calendario “{name}” ?{' '}
              </p>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col class="button">
              <ion-text class="text-btn text-right" onClick={() => onClose()}>
                Cancelar
              </ion-text>
              <ion-text class="text-btn" onClick={() => onSubmit()}>
                Borrar
              </ion-text>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    </IonPopover>
  );
};

export default Delete;
