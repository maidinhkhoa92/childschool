import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { HeaderBack, Loading } from 'components';
import { checkinActions } from 'store/actions';
import { FormatDateParams } from 'utils/moment';
import { IonCheckbox } from '@ionic/react';
import './index.scss';

class Checkin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      absent: [],
      checkin: [],
      checkout: [],
      selected: [],
      selectedCheckout: []
    };
  }

  componentDidMount() {
    const { fetch, class_id } = this.props;
    const data = {
      classes: class_id,
      date: FormatDateParams(new Date())
    };
    this.setState(
      {
        loading: true
      },
      () => {
        fetch(data, this.success, this.fail);
      }
    );
  }

  success = res => {
    this.setState({
      loading: false,
      absent: res.absent,
      checkin: res.checkin,
      checkout: res.checkout
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img className="img-icon avatar" src={url} alt="logo" />;
    }
    return <ion-img class="img-icon" src="/images/child.png" alt="logo" />;
  };

  renderChild = () => {
    const { selected, absent, checkin, checkout, selectedCheckout } = this.state;
    return _.map(this.props.childs, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => this.selectChild(item.id)}>
          <div className="wapper-img">
            {this.showAvatar(item.profile.photo)}
            {_.includes(selectedCheckout, item.id) && (
              <div className="selected">
                <img src="/images/tick.png" />
              </div>
            )}
            {_.includes(selected, item.id) && (
              <div className="selected">
                <img src="/images/tick.png" />
              </div>
            )}
            {_.includes(checkin, item.id) && (
              <div className="checked-in">
                <img src="/images/checkin.svg" />
              </div>
            )}
            {_.includes(checkout, item.id) && (
              <div className="checked-out">
                <img src="/images/checkout.svg" />
              </div>
            )}
            {_.includes(absent, item.id) && (
              <div className="absence">
                <img src="/images/absence.svg" />
              </div>
            )}
          </div>
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };

  // checkin action
  buttonSelectedClass = () => {
    if (this.state.selected.length > 0) {
      return 'button active';
    }
    return 'button';
  };
  buttonSelectedCheckoutClass = () => {
    if (this.state.selectedCheckout.length > 0) {
      return 'button active';
    }
    return 'button';
  };

  selectChild = id => {
    const { selected, absent, checkin, checkout, selectedCheckout } = this.state;
    if (_.includes(checkin, id)) {
      if (!_.includes(selectedCheckout, id)) {
        this.setState(prev => ({
          selectedCheckout: [...prev.selectedCheckout, id]
        }));
      } else {
        this.setState(prev => ({
          selectedCheckout: _.filter(prev.selectedCheckout, item => item !== id)
        }));
      }
    } else if (!_.includes(absent, id) && !_.includes(checkout, id)) {
      if (!_.includes(selected, id)) {
        this.setState(prev => ({
          selected: [...prev.selected, id]
        }));
      } else {
        this.setState(prev => ({
          selected: _.filter(prev.selected, item => item !== id)
        }));
      }
    }
  };

  update = type => {
    const { update, checkin_id } = this.props;
    this.setState(
      prev => ({
        [type]: _.union(prev.selected, prev[type]),
        selected: []
      }),
      () => {
        const { absent, checkin, checkout } = this.state;
        const data = {
          absent,
          checkin,
          checkout
        };
        update(checkin_id, data, this.success, this.fail);
      }
    );
  };

  onCheckout = () => {
    const { update, checkin_id } = this.props;
    this.setState(
      prev => ({
        checkin: _.filter(prev.checkin, item => !_.includes(prev.selectedCheckout, item)),
        checkout: _.union(prev.checkout, prev.selectedCheckout),
        selectedCheckout: []
      }),
      () => {
        const { absent, checkin, checkout } = this.state;
        const data = {
          absent,
          checkin,
          checkout
        };
        update(checkin_id, data, this.success, this.fail);
      }
    );
  };

  selectAll = e => {
    const value = e.target.checked;
    const { childs } = this.props;
    const { absent, checkout, checkin } = this.state;
    if (value) {
      this.setState({
        selected: _.filter(
          _.map(childs, child => child.id),
          item =>
            !_.includes(absent, item) && !_.includes(checkout, item) && !_.includes(checkin, item)
        )
      });
    } else {
      this.setState({
        selected: []
      });
    }
  };

  render() {
    const { class_name } = this.props;
    const { loading } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title={class_name} classTitle={'atma'} />
        <ion-content class="checkin-wrapper">
          <div className="all checkin-movedown">
            <ion-grid class="vistagroup">
              <ion-row class="item-check">
                <IonCheckbox color="danger" onIonChange={e => this.selectAll(e)} />
                <ion-label class="text-selec">Seleccionar todos</ion-label>
              </ion-row>
              <ion-row class="wapper-item">{loading ? <Loading /> : this.renderChild()}</ion-row>
            </ion-grid>
          </div>
        </ion-content>
        <ion-footer class="checkin-footer">
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center-Tutores">
                <button
                  className={this.buttonSelectedClass()}
                  onClick={() => this.update('checkin')}
                >
                  CHECK-IN
                </button>
              </ion-col>
              <ion-col class="ion-text-center-Tutores">
                <button
                  className={this.buttonSelectedCheckoutClass()}
                  onClick={() => this.onCheckout()}
                >
                  CHECK-OUT
                </button>
              </ion-col>
              <ion-col class="ion-text-center-Tutores">
                <button
                  className={this.buttonSelectedClass()}
                  onClick={() => this.update('absent')}
                >
                  AUSENTE
                </button>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    class_name: state.Classes.detail.name,
    class_id: state.Classes.detail.id,
    childs: state.Classes.child,
    checkin_id: state.Checkin.detail.id
  };
};

const mapDispatchToProps = {
  fetch: checkinActions.create,
  update: checkinActions.edit
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Checkin);
