import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { HeaderClasses, Loading, FooterClassStaff } from 'components';
import { classesActions } from 'store/actions';
import './index.scss';

class Vistagroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.props.match.params.id, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  renderChild = () => {
    return _.map(this.props.childs, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => history.push('/staff/perfil/' + item.id)}>
          {this.showAvatar(item.profile.photo)}
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} allt="avatar" className="wapper-img" />;
    }
    return (
      <div className="wapper-img">
        <ion-img class="img-icon" src="/images/child.svg" alt="logo" />
      </div>
    );
  };

  groupColor = () => {
    let { detail } = this.props;

    if (this.state.loading) {
      detail = '#fff';
    }

    return { '--background': detail.color };
  };

  render() {
    const { loading } = this.state;
    const { detail, childs } = this.props;
    const count = childs.length;
    return (
      <React.Fragment>
        <HeaderClasses color={loading ? '#fff' : detail.color} />
        <ion-content class="vistagroup-wrapper" style={this.groupColor()}>
          <ion-grid class="vistagroup">
            <ion-text class="ion-text-center">
              <p className="text-title">{loading ? <Loading /> : `${detail.name} [${count}]`}</p>
            </ion-text>
            <ion-row class="wapper-item">{loading ? <Loading /> : this.renderChild()}</ion-row>
          </ion-grid>
        </ion-content>
        <FooterClassStaff />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    detail: state.Classes.detail,
    childs: state.Classes.child
  };
};

const mapDispatchToProps = {
  fetch: classesActions.detail
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Vistagroup);
