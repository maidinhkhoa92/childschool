import React from 'react';
import _ from 'lodash';
import { HeaderActivities, Loading } from 'components';
import { IonModal, IonDatetime, IonInput, IonCol } from '@ionic/react';
import { connect } from 'react-redux';
import { alertActions } from 'store/actions';

class Biberon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      note: []
    };
  }
  onClose = () => {
    this.props.onClose('biberon', false);
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setNews(key, value);
  };

  renderChild = () => {
    const { params } = this.props;
    return _.map(params.ids, (child, key) => (
      <React.Fragment key={key}>
        <span className="icon-sofia">
          {this.showAvatar(child.profile.photo)}
          <ion-text>{child.profile.firstName}</ion-text>
        </span>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return (
        <img
          src={url}
          alt="avatar"
          className="wapper-avatar"
        />
      );
    }
    return (
      <div className="wapper-img">
        <ion-img
          src="/images/icon/bebe.svg"
          allt="avatar"
        />
      </div>
    );
  };

  addArray = (key, value) => {
    this.setState(
      prev => {
        if (!_.includes(this.props.params.news[key], value)) {
          return {
            [key]: [...prev[key], value]
          };
        }
        return {
          [key]: _.filter(prev[key], item => item !== value)
        };
      },
      () => {
        this.props.setNews(key, this.state[key]);
      }
    );
  };

  buttonStyle = value => {
    if (_.includes(this.props.params.news.note, value)) {
      return { '--background': '#00d7a3' };
    }
    return {};
  };

  onSubmit = () => {
    const { params } = this.props;
    const body = {
      ids: _.map(params.ids, item => item.id),
      news: {
        ...params.news,
        title: 'BIBERÓN',
        type: 'FeedingBottle'
      }
    };
    if(body.news.note.length === 0) {
      this.props.alert('Selecciona que ha tomado');
    } else {
      this.props.onSubmit(body);
    }
  };
  render() {
    const { status, onClose, params, loading } = this.props;
    return (
      <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
        <HeaderActivities
          url="/images/icon/la-alimentacion-con-biberon.svg"
          onClose={this.onClose}
        />

        <ion-content class="bebiro-container">
          <ion-grid class="main-content">
            <div className="all">
              <ion-row class="user-select">{this.renderChild()}</ion-row>
              <ion-row class="select-user">
                <div className="wapper-img">
                  <ion-img
                    src="/images/icon/intercambiabilidad.svg"
                    alt="logo"
                    onClick={() => onClose('selectChild', true, false)}
                  />
                </div>
              </ion-row>
              <ion-row class="time">
                <ion-text class="time-text">Hora</ion-text>
                <div className="time-wapper">
                  <IonDatetime
                    displayFormat="HH:mm"
                    name="time"
                    onIonChange={e => this.onChange(e)}
                  />
                </div>
              </ion-row>
              <ion-row class="btn-options">
                <IonCol sizeMd="4" sizeXs="3">
                  <ion-button
                    class="biberon-activity"
                    style={this.buttonStyle('Leche')}
                    onClick={() => this.addArray('note', 'Leche')}
                  >
                    Leche
                  </ion-button>
                </IonCol>
                <IonCol sizeMd="4" sizeXs="3">
                  <ion-button
                    style={this.buttonStyle('Zumo')}
                    onClick={() => this.addArray('note', 'Zumo')}
                  >
                    Zumo
                  </ion-button>
                </IonCol>
                <IonCol sizeMd="4" sizeXs="3">
                  <ion-button
                    style={this.buttonStyle('Agua')}
                    onClick={() => this.addArray('note', 'Agua')}
                  >
                    Agua
                  </ion-button>
                </IonCol>
              </ion-row>
              <ion-row class="select-medicina">
                <ion-text class="time-text">Cantidad de ml</ion-text>
                <IonInput
                  type="number"
                  min="0"
                  step="1"
                  required
                  name="group"
                  onIonChange={e => this.onChange(e)}
                />
              </ion-row>
              <ion-row class="text-comidas">
                <IonCol sizeXs="10">
                  <textarea
                    rows="3"
                    cols="35"
                    name="content"
                    placeholder="Descripción"
                    onChange={e => this.onChange(e)}
                  />
                </IonCol>
              </ion-row>
            </div>
            <ion-row class="btn-submit biberon-activity">
              {loading ? (
                <Loading />
              ) : (
                <ion-button onClick={() => this.onSubmit()}>Añadir</ion-button>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Biberon);