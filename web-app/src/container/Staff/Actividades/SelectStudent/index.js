import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { HeaderSelect } from 'components';
import { IonModal, IonButton } from '@ionic/react';
import { childActions, checkinActions } from 'store/actions';
import { FormatDateParams } from 'utils/moment';

class SelectStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      childs: [],
      selectedChilds: [],
      selectedClass: {},
      absent: [],
      loading: false
    };
  }

  componentDidMount() {
    const query = {
      class_id: this.props.classes[0].id
    };
    this.props.fetchChild(query, this.fetchChildSuccess, this.fetchChildFail);

    const checkinQuery = {
      classes: this.props.classes[0].id,
      date: FormatDateParams(new Date())
    };
    this.props.fetchCheckin(checkinQuery, this.fetchCheckinSuccess, this.fetchCheckinFail);
  }

  componentDidUpdate(prevProps) {
    if (this.props.parentChilds !== prevProps.parentChilds) {
      if (this.props.parentChilds !== this.state.selectedChilds) {
        this.setState({
          selectedChilds: this.props.parentChilds
        });
      }
    }
  }

  fetchChildSuccess = childs => {
    this.setState({
      selectedClass: childs.length > 0 ? childs[0].classes : '',
      childs: childs
    });
  };

  fetchChildFail = () => {};

  fetchCheckinSuccess = res => {
    this.setState({
      loading: false,
      absent: res.absent
    });
  };

  fetchCheckinFail = () => {
    this.setState({
      loading: false
    });
  };

  renderChild = () => {
    const { selectedChilds, childs, absent } = this.state;
    return _.map(childs, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => !_.includes(absent, item.id) && this.onSelectChild(item)}>
          <div className="wapper-img">
            {this.showAvatar(item.profile.photo)}
            {_.findIndex(selectedChilds, item) !== -1 && (
              <div className="selected">
                <img src="/images/tick.png" />
              </div>
            )}
            {_.includes(absent, item.id) && <div className="selected" />}
          </div>
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };

  onSelectChild = child => {
    this.setState(prev => {
      if (_.findIndex(prev.selectedChilds, child) !== -1) {
        return {
          selectedChilds: _.filter(prev.selectedChilds, item => item !== child)
        };
      }
      return {
        selectedChilds: [...prev.selectedChilds, child]
      };
    });
  };

  // select classes

  onSelectClass = class_id => {
    this.setState(
      {
        selectedClass: class_id
      },
      () => {
        const query = {
          class_id: this.state.selectedClass
        };
        this.props.fetchChild(query, this.onSelectClassSuccess, this.fetchChildFail);

        const checkinQuery = {
          classes: this.state.selectedClass,
          date: FormatDateParams(new Date())
        };
        this.props.fetchCheckin(checkinQuery, this.fetchCheckinSuccess, this.fetchCheckinFail);
      }
    );
  };

  onSelectClassSuccess = childs => {
    this.setState({
      childs: childs
    });
  };

  submit = () => {
    this.props.onClose('selectChild', false, false);
    this.props.setChilds('ids', this.state.selectedChilds);
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img className="img-icon avatar" src={url} alt="logo" />;
    }
    return <ion-img class="img-icon" src="/images/child.svg" alt="logo" />;
  };

  render() {
    const { selectedClass } = this.state;
    const { classes, status, onClose } = this.props;
    return (
      <IonModal
        isOpen={status}
        class="main-content-add-child"
        onDidDismiss={() => onClose('selectChild', false, false)}
      >
        <HeaderSelect
          onLeftPress={() => onClose('selectChild', false, false)}
          list={classes}
          onPress={this.onSelectClass}
          selectedValue={selectedClass}
        />
        <ion-content class="select-student">
          <ion-grid class="vistagroup">
            <ion-row class="wapper-item">{this.renderChild()}</ion-row>
          </ion-grid>
        </ion-content>
        <ion-footer class="footer-add-message">
          <ion-row class="btn-wapper ion-text-center">
            <IonButton onClick={() => this.submit()} color="danger" type="submit">
              Siguiente
            </IonButton>
          </ion-row>
        </ion-footer>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {
    classes: state.Classes.list
  };
};

const mapDispatchToProps = {
  fetchChild: childActions.get,
  fetchCheckin: checkinActions.create
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectStudent);
