import React, { Component } from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { HeaderTransparent, Loading } from 'components';
import { connect } from 'react-redux';
import { childActions, alertActions } from 'store/actions';
import SelectChild from '../SelectStudent';

class Media extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: '',
      url: '',
      content: '',
      loading: false,
      selectChild: false,
      group: '',
      ids: []
    };
  }

  componentDidMount() {
    const { type, url, group } = this.props.location.state;
    this.setState({
      type,
      url,
      group,
      selectChild: group === 'class'
    });
  }

  onChange = e => {
    const value = e.target.value;
    this.setState({
      content: value
    });
  };

  onSubmit = () => {
    const { group, ids } = this.state;
    if (group === 'class' && ids.length === 0) {
      this.setState(
        {
          selectChild: true
        },
        () => {
          this.props.alert('Necesitas seleccionar niño');
        }
      );
    } else {
      this.setState(
        {
          loading: true
        },
        () => {
          const { type, url, content, ids, group } = this.state;
          const { child } = this.props;
          const data = {
            news: {
              title: type === 'Photo' ? 'FOTO' : 'VIDEO',
              time: new Date(),
              group: '',
              selected: [],
              note: [content],
              type: type,
              content: url
            }
          };
          if (group === 'class') {
            data.ids = _.map(ids, item => item.id);
          } else {
            data.ids = [child.id];
          }
          
          this.props.submit(data, this.success, this.fail);
        }
      );
    }
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        history.goBack();
        this.props.alert('Actividad publicada');
      }
    );
  };

  fail = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  onToggle = (key, value) => {
    this.setState({
      [key]: value
    });
  };

  setInformation = (key, value) => {
    this.setState({
      [key]: value
    });
  };

  render() {
    const { type, url, loading, selectChild } = this.state;
    return (
      <React.Fragment>
        <HeaderTransparent />
        <ion-content class="media-container">
          <div className="media-content">
            <div className="overflay" />
            {type === 'Photo' && <img src={url} />}
            {type === 'Video' && (
              <video width="100%" height="240" controls>
                <source src={url} type="video/mp4" />
                Your browser does not support the video tag.
              </video>
            )}
          </div>
          <div className="message-wrapper">
            <input
              type="text"
              className="message-input"
              placeholder="Escribe una nota"
              onChange={e => this.onChange(e)}
            />
            {loading ? (
              <Loading />
            ) : (
              <button className="message-button" onClick={() => this.onSubmit()}>
                <ion-img src="/images/icon-send.svg" />
              </button>
            )}
          </div>
          <SelectChild
            status={selectChild}
            onClose={this.onToggle}
            setChilds={this.setInformation}
          />
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user_id: state.Auth.infor.id,
    childs: state.Classes.child,
    child: state.Child.detail
  };
};

const mapDispatchToProps = {
  submit: childActions.postNews,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Media);
