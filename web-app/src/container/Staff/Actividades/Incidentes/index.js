import React from 'react';
import _ from 'lodash';
import { HeaderActivities, Loading } from 'components';
import { IonModal, IonInput, IonDatetime, IonTextarea, IonCol } from '@ionic/react';

export default class Incidentes extends React.Component {
  onClose = () => {
    this.props.onClose('incidentes', false);
  };

  renderChild = () => {
    const { params } = this.props;
    return _.map(params.ids, (child, key) => (
      <React.Fragment key={key}>
        <span className="icon-sofia">
          {this.showAvatar(child.profile.photo)}
          <ion-text>{child.profile.firstName}</ion-text>
        </span>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return (
        <img
          src={url}
          alt="avatar"
          className="wapper-avatar"
        />
      );
    }
    return (
      <div className="wapper-img">
        <ion-img
          src="/images/icon/bebe.svg"
          allt="avatar"
        />
      </div>
    );
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setNews(key, value);
  };

  onChangeNote = e => {
    const value = e.target.value;
    this.props.setNews('note', [value]);
  };

  onSubmit = () => {
    const { params } = this.props;
    const body = {
      ids: _.map(params.ids, item => item.id),
      news: {
        ...params.news,
        type: 'Incidents'
      }
    };
    this.props.onSubmit(body);
  };

  render() {
    const { status, onClose, loading } = this.props;
    return (
      <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
        <HeaderActivities url="/images/icon/curacion.svg" onClose={this.onClose} />
        <ion-content class="incidentes-container">
          <ion-grid class="main-content">
            <div className="all">
              <ion-row class="user-select">{this.renderChild()}</ion-row>
              <ion-row class="select-user">
                <div className="wapper-img">
                  <ion-img
                    src="/images/icon/intercambiabilidad.svg"
                    alt="logo"
                    onClick={() => onClose('selectChild', true, false)}
                  />
                </div>
              </ion-row>
              <ion-row>
                <form className="form-person">
                  <IonCol sizeXs="10">
                    <IonInput
                      placeholder="Tipo de incidente"
                      name="title"
                      onIonChange={e => this.onChange(e)}
                    />
                  </IonCol>
                  <IonCol>
                    <IonInput
                      placeholder="Ayuda suministrada"
                      name="group"
                      onIonChange={e => this.onChange(e)}
                    />
                  </IonCol>
                  <IonCol>
                    <IonInput
                      placeholder="Miembro del staff presente"
                      name="note"
                      onIonChange={e => this.onChangeNote(e)}
                    />
                  </IonCol>
                  <IonCol sizeXs="10" class="textarea">
                    <IonTextarea
                      placeholder="Nota"
                      auto-grow={true}
                      name="content"
                      onIonChange={e => this.onChange(e)}
                    />
                  </IonCol>
                  <ion-row class="time">
                    <ion-text class="time-text">Hora</ion-text>
                    <div className="time-wapper">
                      <IonDatetime
                        displayFormat="HH:mm"
                        name="time"
                        onIonChange={e => this.onChange(e)}
                      />
                    </div>
                  </ion-row>
                </form>
              </ion-row>
            </div>
            <ion-row class="btn-submit">
              {loading ? (
                <Loading />
              ) : (
                <ion-button onClick={() => this.onSubmit()}>Añadir</ion-button>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}
