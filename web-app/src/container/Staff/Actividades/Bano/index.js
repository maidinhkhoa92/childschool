import React from 'react';
import _ from 'lodash';
import { HeaderActivities, Loading } from 'components';
import { IonModal, IonDatetime, IonCheckbox, IonCol, IonRow } from '@ionic/react';

export default class Bano extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      note: [],
      selected: [],
      noteValidate: false,
      selectedValidate: false
    };
  }
  onClose = () => {
    this.props.onClose('bano', false);
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setNews(key, value);
  };

  onCheckbox = e => {
    if (e.target.checked) {
      this.props.setNews('group', 'Cambio de pañal');
    } else {
      this.props.setNews('group', '');
    }
  };

  renderChild = () => {
    const { params } = this.props;
    return _.map(params.ids, (child, key) => (
      <React.Fragment key={key}>
        <span className="icon-sofia">
          {this.showAvatar(child.profile.photo)}
          <ion-text>{child.profile.firstName}</ion-text>
        </span>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} alt="avatar" className="wapper-avatar" />;
    }
    return (
      <div className="wapper-img">
        <ion-img src="/images/icon/bebe.svg" allt="avatar" />
      </div>
    );
  };

  addNote = (key, value) => {
    this.setState(
      prev => {
        if (!_.includes(this.props.params.news[key], value)) {
          return {
            noteValidate: false,
            [key]: [value]
          };
        }
        return {
          [key]: _.filter(prev[key], item => item !== value)
        };
      },
      () => {
        this.props.setNews(key, this.state[key]);
      }
    );
  };

  addSelected = (key, value) => {
    this.setState(
      prev => {
        if (!_.includes(this.props.params.news[key], value)) {
          return {
            selectedValidate: false,
            [key]: [...prev[key], value]
          };
        }
        return {
          [key]: _.filter(prev[key], item => item !== value)
        };
      },
      () => {
        this.props.setNews(key, this.state[key]);
      }
    );
  };

  buttonStyle = (key, value) => {
    if(key === 'note' && this.state.noteValidate) {
      return { '--box-shadow': '0px 4px 8px #00d7a3' }
    }
    if(key === 'selected' && this.state.selectedValidate) {
      return { '--box-shadow': '0px 4px 8px #00d7a3' }
    }
    if (_.includes(this.props.params.news[key], value)) {
      return { '--background': '#00d7a3' };
    }
    return {};
  };

  onSubmit = () => {
    const { params } = this.props;
    const body = {
      ids: _.map(params.ids, item => item.id),
      news: {
        ...params.news,
        title: 'BAÑO',
        type: 'Bathroom'
      }
    };
    if (body.news.note.length === 0) {
      this.setState({
        noteValidate: true
      });
    } else if (body.news.selected.length === 0) {
      this.setState({
        selectedValidate: true
      });
    } else {
      this.props.onSubmit(body);
    }
  };
  render() {
    const { status, onClose, loading } = this.props;
    const { noteValidate, selectedValidate } = this.state;
    return (
      <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
        <HeaderActivities url="/images/icon/orinal.svg" onClose={this.onClose} />
        <ion-content class="bano-container">
          <ion-grid class="main-content">
            <div className="all">
              <ion-row class="user-select">{this.renderChild()}</ion-row>
              <ion-row class="select-user">
                <div className="wapper-img">
                  <ion-img
                    src="/images/icon/intercambiabilidad.svg"
                    alt="logo"
                    onClick={() => onClose('selectChild', true, false)}
                  />
                </div>
              </ion-row>
              <ion-row class="time">
                <ion-text class="time-text">Hora</ion-text>
                <div className="time-wapper">
                  <IonDatetime
                    displayFormat="HH:mm"
                    name="time"
                    onIonChange={e => this.onChange(e)}
                  />
                </div>
              </ion-row>
              <ion-row class="btn-options">
                <IonCol sizeXs="3" sizeSm="4">
                  <ion-button
                    style={this.buttonStyle('note', 'Pañal')}
                    onClick={() => this.addNote('note', 'Pañal')}
                  >
                    Pañal
                  </ion-button>
                </IonCol>
                <IonCol sizeXs="3" sizeSm="4">
                  <ion-button
                    style={this.buttonStyle('note', 'Orinal')}
                    onClick={() => this.addNote('note', 'Orinal')}
                  >
                    Orinal
                  </ion-button>
                </IonCol>
              </ion-row>
              <ion-row class="btn-option">
                <IonCol sizeXs="2" sizeSm="3">
                  <ion-button
                    style={this.buttonStyle('selected', 'Pis')}
                    onClick={() => this.addSelected('selected', 'Pis')}
                  >
                    Pis
                  </ion-button>
                </IonCol>
                <IonCol sizeXs="2" sizeSm="3">
                  <ion-button
                    style={this.buttonStyle('selected', 'Caca')}
                    onClick={() => this.addSelected('selected', 'Caca')}
                  >
                    Caca
                  </ion-button>
                </IonCol>
              </ion-row>
              <IonRow align-items-center justify-content-center class="btn-check">
                <IonCol sizeXs="10" class="checkbox-flex">
                  <IonCheckbox color="danger" name="group" onIonChange={e => this.onCheckbox(e)} />
                  <ion-text>Cambio de pañal</ion-text>
                </IonCol>
              </IonRow>
              <ion-row class="text-comidas">
                <IonCol sizeXs="10">
                  <textarea
                    rows="3"
                    cols="35"
                    placeholder="Descripción"
                    name="content"
                    onChange={e => this.onChange(e)}
                  />
                </IonCol>
              </ion-row>
            </div>
            <ion-row class="btn-submit bano-activity">
              {loading ? (
                <Loading />
              ) : (
                <ion-button onClick={() => this.onSubmit()}>Añadir</ion-button>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}
