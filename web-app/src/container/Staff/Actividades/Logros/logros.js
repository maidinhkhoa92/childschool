const logros = [
  'mates',
  'habla',
  'ciencias',
  'emocional',
  'cognitivo',
  'social',
  'escritura',
  'música',
  'motor',
  'juego',
  'arte',
  'físico',
  'sentidos',
  'artesanía',
  'humor',
  'pintura',
  'agilidad',
  'cocina'
];

export default logros;
