import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { HeaderActivities, Loading } from 'components';
import {
  IonDatetime,
  IonModal,
  IonPopover,
  IonTextarea,
  IonSelect,
  IonRow,
  IonCol
} from '@ionic/react';
import Foods from './Foods';
import { alertActions } from 'store/actions';

class Comidas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: [],
      noted: [],
      showFood: false,
      selectedFood: []
    };
  }

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setInformation(key, value);
  };

  onChangeNews = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setNews(key, value);
  };

  addArray = (key, value) => {
    this.setState(
      prev => {
        if (!_.includes(this.props.params.news[key], value)) {
          return {
            [key]: [...prev[key], value]
          };
        }
        return {
          [key]: _.filter(prev[key], item => item !== value)
        };
      },
      () => {
        this.props.setNews(key, this.state[key]);
      }
    );
  };

  buttonStyle = value => {
    if (_.includes(this.props.params.news.selected, value)) {
      return { '--background': '#00d7a3' };
    }
    return {};
  };

  onClose = () => {
    this.props.onClose('comidas', false);
  };

  renderChild = () => {
    const { params } = this.props;
    return _.map(params.ids, (child, key) => (
      <React.Fragment key={key}>
        <span className="icon-sofia">
          {this.showAvatar(child.profile.photo)}
          <ion-text>{child.profile.firstName}</ion-text>
        </span>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} alt="avatar" className="wapper-avatar" />;
    }
    return (
      <div className="wapper-img">
        <ion-img src="/images/icon/bebe.svg" allt="avatar" />
      </div>
    );
  };

  // food handle
  renderFood = () => {
    return _.map(this.props.params.news.note, (item, key) => (
      <span key={key}>
        {item}
        <img src="/images/close.svg" onClick={() => this.removeFood(item)} />
      </span>
    ));
  };
  onToggleFood = value => {
    this.setState({
      showFood: value
    });
  };

  onSelecteFood = item => {
    this.setState(prev => {
      if (_.includes(prev.selectedFood, item)) {
        return {
          selectedFood: _.filter(prev.selectedFood, food => item !== food)
        };
      }
      return {
        selectedFood: [...prev.selectedFood, item]
      };
    });
  };

  submitFood = () => {
    this.setState(
      {
        selectedFood: _.union(this.props.params.news.note, this.state.selectedFood),
        showFood: false
      },
      () => {
        this.props.setNews('note', this.state.selectedFood);
      }
    );
  };

  removeFood = item => {
    this.setState(
      {
        selectedFood: _.filter(this.state.selectedFood, food => food !== item)
      },
      () => {
        this.props.setNews('note', this.state.selectedFood);
      }
    );
  };

  onSubmit = () => {
    const { params } = this.props;
    const body = {
      ids: _.map(params.ids, item => item.id),
      news: {
        ...params.news,
        title: 'COMIDAS',
        type: 'Foods'
      }
    };
    if (body.news.group.length === 0) {
      this.props.alert('Selecciona el tipo de comida');
    } else {
      this.props.onSubmit(body);
    }
  };

  render() {
    const { status, onClose, params, loading } = this.props;
    const { showFood, selectedFood } = this.state;
    return (
      <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
        <HeaderActivities url="/images/icon/caja-de-almuerzo.svg" onClose={this.onClose} />
        <ion-content class="comidas-container">
          <ion-grid class="main-content">
            <div className="all">
              <ion-row class="user-select">{this.renderChild()}</ion-row>
              <ion-row class="select-user">
                <div className="wapper-img">
                  <ion-img
                    src="/images/icon/intercambiabilidad.svg"
                    alt="logo"
                    onClick={() => onClose('selectChild', true, false)}
                  />
                </div>
              </ion-row>
              <ion-row class="time">
                <ion-text class="time-text">Hora</ion-text>
                <div className="time-wapper">
                  <IonDatetime
                    displayFormat="HH:mm"
                    name="time"
                    onIonChange={e => this.onChangeNews(e)}
                  />
                </div>
              </ion-row>
              <IonRow class="select-medicina">
                <IonCol sizeXs="10">
                  <IonSelect
                    placeholder="Tipo de comida"
                    interface="popover"
                    name="group"
                    onIonChange={e => this.onChangeNews(e)}
                  >
                    <ion-select-option value="Desayuno">Desayuno</ion-select-option>
                    <ion-select-option value="Almuerzo">Almuerzo</ion-select-option>
                    <ion-select-option value="Comida">Comida</ion-select-option>
                    <ion-select-option value="Merienda tarde">Merienda tarde</ion-select-option>
                  </IonSelect>
                </IonCol>
              </IonRow>

              <ion-row class="btn-options">
                <IonCol sizeXs="3" sizeSm="6">
                  <ion-button
                    class="button-activity"
                    style={this.buttonStyle('Toda')}
                    onClick={() => this.addArray('selected', 'Toda')}
                  >
                    Toda
                  </ion-button>
                </IonCol>
                <IonCol sizeXs="3" sizeSm="6">
                  <ion-button
                    class="button-activity"
                    style={this.buttonStyle('Casi toda')}
                    onClick={() => this.addArray('selected', 'Casi toda')}
                  >
                    Casi toda
                  </ion-button>
                </IonCol>
                <IonCol sizeXs="3" sizeSm="6">
                  <ion-button
                    class="button-activity"
                    style={this.buttonStyle('Poca')}
                    onClick={() => this.addArray('selected', 'Poca')}
                  >
                    Poca
                  </ion-button>
                </IonCol>
                <IonCol sizeXs="3" sizeSm="6">
                  <ion-button
                    class="button-activity"
                    style={this.buttonStyle('Ninguna')}
                    onClick={() => this.addArray('selected', 'Ninguna')}
                  >
                    Ninguna
                  </ion-button>
                </IonCol>
              </ion-row>

              <ion-row class="text-comidas">
                <IonCol sizeXs="10">
                  <div className="select-food">
                    <ion-img src="/images/food.svg" onClick={() => this.onToggleFood(true)} />
                    {this.renderFood()}
                  </div>
                  <ion-text class="textarea">
                    <IonTextarea
                      rows="3"
                      cols="35"
                      placeholder="Descripción"
                      name="content"
                      onIonChange={e => this.onChangeNews(e)}
                    />
                  </ion-text>
                </IonCol>
              </ion-row>
            </div>
            <ion-row class="btn-submit btn-activity">
              {loading ? (
                <Loading />
              ) : (
                <ion-button onClick={() => this.onSubmit()}>Añadir</ion-button>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
        <IonPopover
          class="food-popover"
          isOpen={showFood}
          onDidDismiss={() => this.onToggleFood(false)}
        >
          <div className="food-wrapper">
            <div className="food-container">
              {_.map(Foods, (item, key) => (
                <div className="food-item" key={key} onClick={() => this.onSelecteFood(item)}>
                  <span
                    className={
                      _.includes(params.news.note, item) || _.includes(selectedFood, item)
                        ? 'active'
                        : ''
                    }
                  >
                    {item}
                  </span>
                </div>
              ))}
            </div>
            <button className="food-button" onClick={() => this.submitFood()}>
              Aplicar
            </button>
          </div>
        </IonPopover>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Comidas);
