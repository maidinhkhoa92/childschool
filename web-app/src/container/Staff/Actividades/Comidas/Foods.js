const foods = [
  'leche',
  'queso',
  'fruta',
  'galletas',
  'pasta',
  'yogurt',
  'vegetales',
  'zumo',
  'cereales',
  'pizza',
  'postre',
  'carne',
  'pescado',
  'huevos',
  'puré',
  'helado',
  'gelatina',
  'arroz'
];

export default foods;
