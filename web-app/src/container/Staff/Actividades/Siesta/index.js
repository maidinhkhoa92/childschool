import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { HeaderActivities } from 'components';
import { IonModal, IonDatetime } from '@ionic/react';
import { classesActions } from 'store/actions';
import moment from 'moment';

class Siesta extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 1,
      params: {
        ids: [],
        news: {
          title: 'SIESTA',
          note: [],
          type: 'Siesta',
          content: '',
          time: moment(new Date()).format('hh:mm'),
          group: '',
          selected: []
        }
      }
    };
  }

  onClose = () => {
    this.props.onClose('siesta', false);
  };

  setTime = e => {
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        ...prev.params,
        news: {
          ...prev.params.news,
          time: value
        }
      }
    }));
  };

  onSubmit = (text, child_id) => {
    const query = {
      ids: [child_id],
      news: {
        ...this.state.params.news,
        group: text,
        content: '',
        time: moment(this.state.params.news.time, 'HH:mm').format()
      }
    };
    this.props.submit(query, this.success, this.fail);
  };

  success = () => {};
  fail = () => {};
  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} alt="avatar" className="siesta-img" />;
    }
    return <ion-img src="/images/children.png" />;
  };
  render() {
    const { status, childs } = this.props;
    const { tab, params } = this.state;
    return (
      <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
        <HeaderActivities url="/images/icon/suenos.svg" onClose={this.onClose} />
        <ion-content class="siesta-container siesta-activity">
          <ion-grid class="main-content">
            <div className="all">
              <ion-row class="time">
                <div className="time-siesta">
                  <div className={tab === 1 ? 'time-wapper hide' : 'time-wapper'}>
                    <IonDatetime
                      displayFormat="HH:mm"
                      value={params.news.time}
                      onIonChange={e => this.setTime(e)}
                    />
                  </div>
                </div>
              </ion-row>

              <ion-row class="row1-siesta">
                <h3 className={tab === 1 && 'active'} onClick={() => this.setState({ tab: 1 })}>
                  Usar hora actual
                </h3>
                <h3 className={tab === 2 && 'active'} onClick={() => this.setState({ tab: 2 })}>
                  Seleccionar hora
                </h3>
              </ion-row>
              {_.map(childs, (item, key) => (
                <ion-row class="row3-siesta" key={key}>
                  <ion-col class="column1" size="4">
                    {this.showAvatar(item.profile.photo)}
                    <ion-text class="text">{item.profile.firstName}</ion-text>
                  </ion-col>
                  {item.sleeping ? (
                    <React.Fragment>
                      <ion-col class="column2" size="4">
                        <ion-img src="/images/luna.svg" />
                      </ion-col>

                      <ion-col class="column3" size="4">
                        <ion-text class="text" onClick={() => this.onSubmit('Despierto', item.id)}>
                          Despiert@
                        </ion-text>
                      </ion-col>
                    </React.Fragment>
                  ) : (
                    <ion-col class="column2" size="4">
                      <ion-text onClick={() => this.onSubmit('Durmiendo', item.id)}>
                        Durmiendo
                      </ion-text>
                    </ion-col>
                  )}
                </ion-row>
              ))}
            </div>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {
    childs: state.Classes.child
  };
};

const mapDispatchToProps = {
  submit: classesActions.postNewsStatus
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Siesta);
