import React, { Component } from 'react';
import './index.scss';

class Siesta extends Component {
  render() {
    return (
      <ion-content>
        <ion-grid class="main-content">
          <ion-row class="header">
            <ion-img
              src="/images/icon/botiquin-de-primeros-auxilios (1).svg"
              alt="logo"
              class="icon-logo"
            />
            <ion-img src="/images/icon/close.svg" alt="logo" class="icon-back" />
          </ion-row>

          <div className="all all-900">
            <ion-row class="time">
              <div className="time-siesta">
                <div className="time-wapper">
                  <ion-datetime displayFormat="HH:mm" value="2012-12-15T13:47:20.789" />
                </div>
              </div>
            </ion-row>

            <ion-row class="row1-siesta">
              <ion-text class="Usar">Usar hora actual</ion-text>
              <ion-text class="Seleccionar">Seleccionar hora</ion-text>
            </ion-row>

            <ion-row class="time">
              <div className="row2-siesta">
                <ion-img src="/images/circle.svg" />
                <ion-text class="time-text">Todos durmiendo</ion-text>
              </div>
            </ion-row>

            <ion-row class="row3-siesta">
              <ion-col class="column1" size="4">
                <ion-img src="/images/children.png" />
                <ion-text class="text">Josep</ion-text>
              </ion-col>

              <ion-col class="column2" size="4">
                <ion-text>Durmiendo</ion-text>
              </ion-col>

              <ion-col class="column3" size="4">
                <ion-text class="text">Despierto</ion-text>
              </ion-col>
            </ion-row>

            <ion-row class="row3-siesta">
              <ion-col class="column1" size="4">
                <ion-img src="/images/children.png" />
                <ion-text class="text">Josep</ion-text>
              </ion-col>

              <ion-col class="column2" size="4">
                <ion-img src="/images/luna.svg" />
              </ion-col>

              <ion-col class="column3" size="4">
                <ion-text class="text">Despierto</ion-text>
              </ion-col>
            </ion-row>
          </div>
        </ion-grid>
      </ion-content>
    );
  }
}
export default Siesta;
