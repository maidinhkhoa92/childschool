import React from 'react';
import './index.scss';

export default class Administrargrupo extends React.Component {
  render() {
    return (
      <ion-content>
        <ion-grid class="main-content">
          <ion-row class="header">
            <ion-img src="/images/icon/suenos (1).svg" alt="logo" class="icon-logo" />
            <ion-img src="/images/icon/close.svg" alt="logo" class="icon-back" />
          </ion-row>
          <ion-row>
            <ion-col />
            <ion-col class="time">
              <div id="time-pick" className="time-pick">
                <ion-datetime displayFormat="HH:mm" value="2012-12-15T13:47:20.789" />
              </div>
            </ion-col>
          </ion-row>
          <ion-row class="tab">
            <ion-col id="tab1" class="ion-text-center tab-active">
              <ion-text>Usar hora actual</ion-text>
            </ion-col>
            <ion-col id="tab2" class="ion-text-center">
              <ion-text>Seleccionar hora</ion-text>
            </ion-col>
          </ion-row>
          <ion-col class="list-items">
            <ion-row class="items">
              <ion-col class="item-group">
                <div className="wapper-img">
                  <ion-img src="{{item.avatar}}" alt="logo" />
                </div>
                <ion-text class="text-itmes">times</ion-text>
              </ion-col>
              <ion-col id="{{'noactive_' + item.id}}">
                <ion-button color="danger" class="btn-submit">
                  A dormir
                </ion-button>
              </ion-col>
              <ion-col id="{{'active_' + item.id}}" class="active">
                <div className="wapper-btn">
                  <ion-img src="/images/icon/luna (1).svg" alt="logo" />
                  <ion-button class="btn-active">De pie</ion-button>
                </div>
              </ion-col>
            </ion-row>
          </ion-col>
        </ion-grid>
      </ion-content>
    );
  }
}
