import React from 'react';
import history from 'utils/history';
import _ from 'lodash';
import { Loading } from 'components';
import { connect } from 'react-redux';
import { classesActions, authActions, childActions } from 'store/actions';
import { IonInput } from '@ionic/react';
import './index.scss';

class Ontabd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.success, this.fail);
        this.props.search({ word: '' }, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  renderClass = () => {
    return _.map(this.props.list, (item, key) => (
      <React.Fragment key={key}>
        <ion-col
          size="4"
          class="tab-icon"
          style={{ background: item.color }}
          onClick={() => history.push('/staff/vistagroup/' + item.id)}
        >
          <p className="tab-text">{item.name}</p>
        </ion-col>
      </React.Fragment>
    ));
  };
  logout = () => {
    this.props.logout(() => {
      history.push('/');
    });
  };
  search = e => {
    const body = {
      word: e.target.value
    };
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.search(body, this.success, this.fail);
      }
    );
  };
  renderChild = () => {
    return _.map(this.props.result.list, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => history.push('/staff/perfil/' + item.id)}>
          {this.showAvatar(item.profile.photo)}
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };
  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} allt="avatar" className="wapper-img" />;
    }
    return (
      <div className="wapper-img">
        <ion-img class="img-icon" src="/images/child.svg" alt="logo" />
      </div>
    );
  };
  render() {
    const { loading } = this.state;
    const { result } = this.props;
    return (
      <ion-content class="dashboard-staf">
        <ion-button class="logoutButton" onClick={() => this.logout()}>
          <ion-img src="/images/logout.svg" alt="logo" class="icon-back" />
        </ion-button>
        <ion-grid class="otp-page">
          <ion-row>
            <ion-col>
              <ion-icon ios="ios-power" md="md-power" class="icon-power" />
            </ion-col>
          </ion-row>
          <div className="desktop-wrapper">
            <ion-row>
              <ion-col>
                <ion-text>
                  <p className="text-title staff-controlpanel">Hola, seleciona tu clase</p>
                </ion-text>
              </ion-col>
            </ion-row>
            <ion-row class="search-box">
              <ion-col class="search-controlpanel">
                <ion-icon name="search" class="search-icon" />
                <IonInput
                  placeholder="Encuentra más rápido un/a niñ@"
                  onIonChange={e => this.search(e)}
                />
              </ion-col>
            </ion-row>
            <ion-row class="wapper-item">
              {loading ? <Loading /> : result.status ? this.renderChild() : this.renderClass()}
            </ion-row>
          </div>
        </ion-grid>
      </ion-content>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.Classes.list,
    result: state.Child.search
  };
};

const mapDispatchToProps = {
  fetch: classesActions.get,
  logout: authActions.logout,
  search: childActions.search
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Ontabd);
