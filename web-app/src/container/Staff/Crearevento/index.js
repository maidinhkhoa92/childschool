import React, { Component } from 'react';
import history from 'utils/history';
import { connect } from 'react-redux';
import { FormatDateParams } from 'utils/moment';
import './index.scss';
import _ from 'lodash';
import { HeaderBack } from 'components';
import TimeKeeper from 'react-timekeeper';
import { IonPopover } from '@ionic/react';
import Calendar from 'react-calendar';
import { eventActions } from 'store/actions';

class Crearevento extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: {
        start: false,
        end: false,
        date: false
      },
      time: '',
      params: {
        name: '',
        note: '',
        startTime: '12:30',
        endTime: '12:30',
        date: FormatDateParams(new Date())
      },
      editAble: false,
      id: ''
    };
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.get(this.props.match.params.id, this.successDetail, this.fail);
    } else {
      this.setState(
        {
          editAble: false
        },
        () => {
          this.setParams('group', this.props.classes[0].id);
        }
      );
    }
  }

  successDetail = value => {
    this.setState({
      editAble: true,
      id: value.id,
      params: {
        name: value.name,
        note: value.note,
        startTime: value.startTime,
        group: value.group,
        endTime: value.endTime,
        date: FormatDateParams(value.date)
      }
    });
  };

  onToggle = (key, value) => {
    this.setState(prev => ({
      toggle: {
        ...prev.toggle,
        [key]: value
      }
    }));
  };

  setTime = value => {
    this.setState({
      time: value
    });
  };

  confirmTime = key => {
    this.setState(
      {
        toggle: {
          start: false,
          end: false,
          date: false
        }
      },
      () => {
        this.setParams(key, this.state.time);
      }
    );
  };

  setParams = (key, value) => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        [key]: value
      }
    }));
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.setParams(key, value);
  };

  onChangeDate = date => {
    this.setState(
      {
        toggle: {
          start: false,
          end: false,
          date: false
        }
      },
      () => {
        this.setParams('date', FormatDateParams(date));
      }
    );
  };
  // submit actions
  onSubmit = () => {
    if (this.state.editAble) {
      this.props.update(this.state.id, this.state.params, this.actionSuccess, this.fail);
    } else {
      this.props.create(this.state.params, this.actionSuccess, this.fail);
    }
  };

  actionSuccess = () => {
    history.goBack();
  };

  fail = () => {};

  render() {
    const { toggle, params } = this.state;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <HeaderBack title="Crear evento" />
        <ion-content class="content-staff-Crearevento">
          <ion-grid>
            <div className="row1">
              <ion-row class="text">
                <input
                  name="name"
                  type="text"
                  placeholder="Nombre del evento"
                  value={params.name}
                  onChange={e => this.onChange(e)}
                />
              </ion-row>
              <ion-row class="text">
                <select value={params.group} name="group" onChange={e => this.onChange(e)}>
                  {_.map(classes, (item, key) => (
                    <React.Fragment key={key}>
                      <option value={item.id}>{item.name}</option>
                    </React.Fragment>
                  ))}
                </select>
              </ion-row>

              <ion-row class="row-time">
                <ion-col class="column-left" size="6">
                  <span className="text-time">Fecha</span>
                  <ion-text class="time" onClick={() => this.onToggle('date', true)}>
                    {params.date}
                  </ion-text>
                </ion-col>

                <ion-col class="column-right" size="6">
                  <span className="text-time">Hora inicio</span>
                  <ion-text class="time" onClick={() => this.onToggle('start', true)}>
                    {params.startTime}
                  </ion-text>
                </ion-col>
                <ion-col class="column-left" size="6" />
                <ion-col class="column-right" size="6">
                  <span className="text-time">Hora final</span>
                  <ion-text class="time" onClick={() => this.onToggle('end', true)}>
                    {params.endTime}
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="text">
                <textarea
                  value={params.note}
                  placeholder="Nota"
                  name="note"
                  onChange={e => this.onChange(e)}
                />
              </ion-row>
            </div>

            <ion-row class="row-botton">
              <button className="button" onClick={() => this.onSubmit()}>
                Añadir
              </button>
            </ion-row>
          </ion-grid>

          {/* popover */}
          <IonPopover
            isOpen={toggle.start}
            class="large"
            onDidDismiss={() => this.onToggle('start', false)}
          >
            <TimeKeeper hour24Mode onChange={newTime => this.setTime(newTime.formatted24)} />
            <div className="actions">
              <ion-text class="text-btn text-right" onClick={() => this.onToggle('start', false)}>
                Cancelar
              </ion-text>
              <ion-text class="text-btn" onClick={() => this.confirmTime('startTime')}>
                Confirmar
              </ion-text>
            </div>
          </IonPopover>
          <IonPopover
            isOpen={toggle.end}
            class="large"
            onDidDismiss={() => this.onToggle('end', false)}
          >
            <TimeKeeper hour24Mode onChange={newTime => this.setTime(newTime.formatted24)} />
            <div className="actions">
              <ion-text class="text-btn text-right" onClick={() => this.onToggle('end', false)}>
                Cancelar
              </ion-text>
              <ion-text class="text-btn" onClick={() => this.confirmTime('endTime')}>
                Confirmar
              </ion-text>
            </div>
          </IonPopover>
          <IonPopover isOpen={toggle.date} onDidDismiss={() => this.onToggle('date', false)}>
            <Calendar onChange={this.onChangeDate} />
          </IonPopover>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    classes: state.Classes.list
  };
};

const mapDispatchToProps = {
  create: eventActions.create,
  get: eventActions.detail,
  update: eventActions.update
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Crearevento);
