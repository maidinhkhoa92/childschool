import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { HeaderBack, Loading } from 'components';
import Add from '../Add';
import './index.scss';
import { IonCheckbox } from '@ionic/react';

class Vistagroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      params: {
        to_user: []
      },
      modal: false
    };
  }

  onSelect = item => {
    const { params } = this.state;
    if (_.includes(params.to_user, item)) {
      this.setState(prev => ({
        params: {
          ...prev.params,
          to_user: _.filter(prev.params.to_user, user => user._id !== item._id)
        }
      }));
    } else {
      this.setState(prev => ({
        params: {
          ...prev.params,
          to_user: [...prev.params.to_user, item]
        }
      }));
    }
  };

  renderChild = () => {
    return _.map(this.props.childs, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => this.onSelect(item)}>
          <div className="wapper-img">
            <ion-img class="img-icon" src="/images/child.svg" />
            {_.includes(this.state.params.to_user, item) && (
              <div className="selected">
                <ion-img src="/images/tick.png" />
              </div>
            )}
          </div>
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };

  onToggle = value => {
    this.setState({
      modal: value
    });
  };

  selectAll = e => {
    const value = e.target.checked;
    const { childs } = this.props;
    this.setState(prev => {
      if (value) {
        return {
          params: {
            ...prev.params,
            to_user: childs
          }
        };
      }
      return {
        params: {
          ...prev.params,
          to_user: []
        }
      };
    });
  };

  render() {
    const { loading, modal, params } = this.state;
    const { name } = this.props;
    return (
      <React.Fragment>
        <HeaderBack title={name} />
        <ion-content class="mensajes-create-wrapper">
          <ion-grid class="vistagroup">
            <ion-row class="item-check">
              <IonCheckbox color="danger" onIonChange={e => this.selectAll(e)} />
              <ion-label class="text-selec">Seleccionar todos</ion-label>
            </ion-row>
            <ion-row class="wapper-item">{loading ? <Loading /> : this.renderChild()}</ion-row>
          </ion-grid>
        </ion-content>
        <ion-footer class="add-new-tutores">
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center-Tutores">
                <button className="button" onClick={() => this.onToggle(true)}>
                  Siguiente
                </button>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-footer>
        <Add
          status={modal}
          childs={params.to_user}
          onClose={this.onToggle}
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          title={name}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    childs: state.Classes.child,
    name: state.Classes.detail.name
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Vistagroup);
