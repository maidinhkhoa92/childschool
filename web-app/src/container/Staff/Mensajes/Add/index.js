import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { IonButton, IonModal } from '@ionic/react';
import { Loading } from 'components';
import { messageActions } from 'store/actions';

class Add extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {
        classes: [],
        type: 'user'
      },
      loading: false,
      agree: false,
      error: {
        status: false,
        msg: ''
      }
    };
  }

  onChange = e => {
    const value = e.target.value;
    const name = e.target.name;
    this.setState(prev => ({
      params: {
        ...prev.params,
        [name]: value
      }
    }));
  };

  onSubmit = () => {
    this.setState(
      prev => ({
        params: {
          ...prev.params,
          to_user: _.map(this.props.childs, item => {
            return {
              id: item.family._id
            };
          })
        }
      }),
      () => {
        this.props.onSubmit(this.state.params, this.success, this.fail);
      }
    );
  };

  success = () => {
    this.props.onClose(false);
    history.goBack();
  };
  fail = () => {
    this.props.onClose(false);
  };

  render() {
    const { status, onClose, childs, title } = this.props;
    return (
      <IonModal
        isOpen={status}
        class="main-content-add-message staff-create-mensajes"
        onDidDismiss={() => onClose(false)}
      >
        <button className="close" onClick={() => onClose(false)}>
          <img src="/images/close.svg" />
        </button>
        <ion-content>
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center">
                <ion-text class="title">{title}</ion-text>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col class="child-container">
                <div className="child-wrapper">
                  {_.map(childs, (item, key) => (
                    <div key={key} className="child-element">
                      <div className="child-img">
                        <img src="/images/child.svg" />
                      </div>
                      {item.profile.firstName}
                    </div>
                  ))}
                </div>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <div className="hr-wrapper">
                  <div className="hr" />
                  <img src="/images/people.svg" onClick={() => onClose(false)} />
                </div>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <textarea
                  name="message"
                  auto-grow="true"
                  placeholder="Mensaje"
                  className="message-area-input"
                  onChange={e => this.onChange(e)}
                />
              </ion-col>
            </ion-row>
            <ion-row class="btn-wapper ion-text-center">
              {this.state.loading ? (
                <Loading />
              ) : (
                <IonButton onClick={() => this.onSubmit()} color="danger" type="submit">
                  Crear
                </IonButton>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: messageActions.create
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Add);
