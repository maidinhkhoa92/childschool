import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import history from 'utils/history';
import './index.scss';
import { HeaderBack } from 'components';
import truncate from 'utils/truncate';
import { messageActions } from 'store/actions';
import { FormatPastTime } from 'utils/moment';

class Mensajes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: []
    };
  }
  componentDidMount() {
    const { class_id } = this.props;
    this.props.fetch({ class_id: class_id }, this.success, this.fail);
  }
  success = () => {
    this.setState({
      loading: false
    });
  };
  fails = () => {
    this.setState({
      loading: false
    });
  };

  renderMessage = () => {
    return _.map(this.props.list, (item, key) => (
      <React.Fragment key={key}>
        <ion-row
          class="row-content"
          onClick={() => history.push(`/staff/chat/${item.firestore}/${item.id}`)}
        >
          <div className="icon">
            <ion-img
              src={
                item.from.profile && item.from.profile.photo
                  ? item.from.profile.photo
                  : '/images/children.png'
              }
              class="icon"
            />
          </div>

          <ion-col>
            <ion-row class="text1">
              <div>{item.from.profile.firstName}</div>
              <div>{FormatPastTime(item.updated_at)}</div>
            </ion-row>
            {item.note !== '' && (
              <ion-row>
                {item.note} <img src="/images/alert.svg" />
              </ion-row>
            )}
            <ion-row class="text2-phu">
              <div>{truncate(item.message, 50)}</div>
            </ion-row>
          </ion-col>
        </ion-row>
      </React.Fragment>
    ));
  };

  render() {
    return (
      <React.Fragment>
        <HeaderBack title="Mensajes" />
        <ion-content class="content-staff-Mensajes">
          <div className="all">
            <ion-grid class="centent">{this.renderMessage()}</ion-grid>
            <div className="fixedButton">
              <ion-img
                src="/images/add2.svg"
                onClick={() => history.push('/staff/create-mensajes')}
              />
            </div>
          </div>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.Message.list,
    class_id: state.Classes.detail.id
  };
};

const mapDispatchToProps = {
  fetch: messageActions.get
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Mensajes);
