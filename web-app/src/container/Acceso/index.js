import React from 'react';
import { Header, Loading } from 'components';
import { IonButton } from '@ionic/react';
import { connect } from 'react-redux';
import { authActions } from 'store/actions';
import history from 'utils/history';
import './index.scss';

class Acceso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {},
      error: {
        status: false,
        msg: ''
      },
      loading: false
    };
  }

  onChange = e => {
    const value = e.target.value;
    const key = e.target.name;

    this.setState({
      params: {
        ...this.state.params,
        [key]: value
      }
    });
  };

  success = active => {
    this.setState(
      {
        loading: false
      },
      () => {
        if (active === 'Pass confirmed') {
          history.push('comfirm-pin');
        } else {
          history.push('pin');
        }
      }
    );
  };

  fail = message => {
    this.setState({
      loading: false,
      error: {
        status: true,
        msg: message
      }
    });
  };

  onSubmit = e => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.onSubmit(this.state.params, this.success, this.fail);
      }
    );
    e.preventDefault();
  };

  render() {
    const { error } = this.state;
    return (
      <React.Fragment>
        <Header />
        <ion-content class="main-page-acceso">
          <ion-grid class="otp-page">
            <form onSubmit={e => this.onSubmit(e)}>
              {error.status && (
                <ion-row class="wapper-mess">
                  <ion-col class="ion-text-center">
                    <p className="text-err">{error.msg}</p>
                  </ion-col>
                </ion-row>
              )}
              <ion-row>
                <div className="form-acceso">
                  <p className="title-form ion-text-center">Acceso</p>
                  <input
                    type="email"
                    placeholder="Introduce tu email"
                    name="email"
                    required
                    onChange={e => this.onChange(e)}
                    className="text-email-acceso"
                  />
                  <input
                    type="password"
                    placeholder="Contraseña"
                    name="password"
                    required
                    onChange={e => this.onChange(e)}
                    className="text-email-acceso"
                  />
                  <ion-row class="btn-wapper">
                    <ion-text class="text-footer ion-text-center" style={{ paddingTop: '20px' }}>
                      <p onClick={() => history.push('forgot-password')}>
                        ¿Has olvidado tu contraseña?
                      </p>
                    </ion-text>
                    {this.state.loading ? (
                      <Loading />
                    ) : (
                      <button type="submit" color="danger" className="form-accede-button">
                        Accede
                      </button>
                    )}
                  </ion-row>

                  <ion-text class="text-footer ion-text-center">
                    <p onClick={() => history.push('contact')}>¿No tienes cuenta en OntáBb?</p>
                  </ion-text>
                </div>
              </ion-row>
            </form>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Acceso);
