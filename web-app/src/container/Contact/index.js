import React from 'react';
import { connect } from 'react-redux';
import history from 'utils/history';
import { Header, Loading } from 'components';
import { IonButton } from '@ionic/react';
import { authActions, alertActions } from 'store/actions';
import './index.scss';

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {
        typeOfUser: '',
        firstName: '',
        lastName: '',
        email: '',
        telephone: ''
      },
      agree: true,
      loading: false
    };
  }

  onChange = e => {
    this.setState({
      params: {
        ...this.state.params,
        [e.target.name]: e.target.value
      }
    });
  };

  agreed = () => {
    this.setState({
      agree: !this.state.agree
    });
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        history.push('wellcome');
      }
    );
  };

  fail = e => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(e);
      }
    );
  };

  onSubmit = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.onSubmit(this.state.params, this.success, this.fail);
      }
    );
  };

  checkDisable = () => {
    const { agree, params } = this.state;
    if (
      agree === false &&
      params.firstName.length > 0 &&
      params.lastName.length > 0 &&
      params.telephone.length > 0 &&
      params.email.length > 0
    ) {
      return false;
    }
    return true;
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <ion-content class="contact-page">
          <ion-grid class="otp-page">
            <ion-row>
              <div className="form-acceso">
                <p className="title-form ion-text-center">Contáctanos</p>

                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  onChange={e => this.onChange(e)}
                  autoComplete="no"
                />

                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  onChange={e => this.onChange(e)}
                  autoComplete="no"
                />

                <input
                  type="text"
                  placeholder="Teléfono"
                  name="telephone"
                  required
                  onChange={e => this.onChange(e)}
                  autoComplete="no"
                />

                <input
                  type="text"
                  placeholder="Mail"
                  name="email"
                  required
                  onChange={e => this.onChange(e)}
                  autoComplete="no"
                />

                <select
                  placeholder="¿Qué te define mejor?"
                  name="typeOfUser"
                  interface="popover"
                  required
                  onChange={e => this.onChange(e)}
                  value={this.state.params.typeOfUser}
                >
                  <option hidden>¿Qué te define mejor?</option>
                  <option value="director">Director/a</option>
                  <option value="staff">Staff</option>
                  <option value="family">Familiar</option>
                </select>

                <label className="check-box">
                  <input type="checkbox" name="checkBox" required onChange={() => this.agreed()} />
                  <span className="checkmark" />
                  <ion-text>
                    Estoy de acuerdo con los{' '}
                    <a className="text" href="/terms-and-conditions" target="blank">
                      términos y condiciones
                    </a>{' '}
                    y la{' '}
                    <a className="text" href="/privacy-policy" target="blank">
                      política de privacidad
                    </a>{' '}
                    de Ontább
                  </ion-text>
                </label>

                <ion-row class="btn-wapper">
                  {this.state.loading ? (
                    <Loading />
                  ) : (
                      <IonButton
                        onClick={() => this.onSubmit()}
                        color="danger"
                        type="submit"
                        disabled={this.checkDisable()}
                      >
                        Enviar
                    </IonButton>
                    )}
                </ion-row>
              </div>
            </ion-row>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.contact,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact);
