import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { Public, Private, Normal } from './authRouter';

// import component
import history from 'utils/history';
import RootContainer from './rootContainer';
import HomePage from './HomePage';
import Acceso from './Acceso';
import Pin from './Pin';
import Register from './Register';
import Wellcome from './Wellcome';
import Contact from './Contact';
import ComfirmPin from './ComfirmPin';
import ChangePin from './ChangePin';
import Forgot from './Forgot';
import ResetPassword from './ResetPassword';

import ActividadesS from './Staff/Actividades';
import MediaS from './Staff/Actividades/Media';
import AdministrargrupoS from './Staff/Administrargrupo';
import OntabdS from './Staff/Ontabd';
import VistagroupS from './Staff/Vistagroup';
import CreareventoS from './Staff/Crearevento';
import LannisterS from './Staff/Lannister';
import MensajesS from './Staff/Mensajes';
import CreateMensajesS from './Staff/Mensajes/Create';
import DiarioS from './Staff/Diario';
import PerfilS from './Staff/Perfil';
import ChatS from './Staff/Chat';
import CheckinS from './Staff/Checkin';

import CompleteCenterD from './Director/CompleteCenter';
import ControlPanelD from './Director/ControlPanel';
import CreateClassD from './Director/CreateClass';
import VistagroupD from './Director/Vistagroup';
import PerfilD from './Director/Perfil';
import TutoresD from './Director/Tutores';
import MenuD from './Director/Menu';
import MessageD from './Director/Message';
import CreareventoD from './Director/Crearevento';
import LannisterD from './Director/Lannister';
import ChatD from './Director/Chat';
import CheckinD from './Director/Checkin';
import ActividadesD from './Director/Actividades';
import MediaD from './Director/Actividades/Media';
import DiarioD from './Director/Diario';
import AvisosD from './Director/Avisos';

import VistagroupF from './Family/Vistagroup';
import PerfilF from './Family/Perfil';
import DiarioF from './Family/Diario';
import MensajesF from './Family/Mensajes';
import AvisosF from './Family/Avisos';
import ChatF from './Family/Chat';
import AgendaF from './Family/Agenda';

import Terms from './Terms';
import Privacy from './Privacy';

const AppRouter = props => {
  return (
    <Router history={history}>
      <RootContainer>
        <Switch>
          <Normal exact path="/" component={HomePage} />
          <Route exact path="/terms-and-conditions" component={Terms} />
          <Route exact path="/privacy-policy" component={Privacy} />
          <Public {...props} exact path="/acceso" component={Acceso} />
          <Public {...props} exact path="/contact" component={Contact} />
          <Public {...props} exact path="/register" component={Register} />
          <Public {...props} exact path="/register-family" component={Register} />
          <Public {...props} exact path="/register-staff" component={Register} />
          <Public {...props} exact path="/wellcome" component={Wellcome} />
          <Private {...props} exact path="/pin" component={Pin} />
          <Private {...props} exact path="/comfirm-pin" component={ComfirmPin} />
          <Private {...props} exact path="/change-pin" component={ChangePin} />
          <Public {...props} exact path="/forgot-password" component={Forgot} />
          <Public {...props} exact path="/reset-password" component={ResetPassword} />

          <Private {...props} exact path="/family" component={VistagroupF} />
          <Private {...props} path="/family/diario" component={DiarioF} />
          <Private {...props} path="/family/mensajes" component={MensajesF} />
          <Private {...props} path="/family/avisos" component={AvisosF} />
          <Private {...props} path="/family/perfil/:id" component={PerfilF} />
          <Private {...props} path="/family/Chat/:firestore_id/:message_id" component={ChatF} />
          <Private {...props} path="/family/agenda" component={AgendaF} />

          <Private {...props} exact path="/staff" component={OntabdS} />
          <Private {...props} exact path="/staff/actividades" component={ActividadesS} />
          <Private {...props} path="/staff/actividades/media" component={MediaS} />
          <Private {...props} path="/staff/administrargrupo" component={AdministrargrupoS} />
          <Private {...props} exact path="/staff/vistagroup/:id" component={VistagroupS} />
          <Private {...props} exact path="/staff/perfil/:id" component={PerfilS} />
          <Private {...props} exact path="/staff/crearevento" component={CreareventoS} />
          <Private {...props} exact path="/staff/crearevento/:id" component={CreareventoS} />
          <Private {...props} path="/staff/lannister" component={LannisterS} />
          <Private {...props} path="/staff/create-mensajes" component={CreateMensajesS} />
          <Private {...props} path="/staff/mensajes" component={MensajesS} />
          <Private {...props} path="/staff/diario" component={DiarioS} />
          <Private {...props} path="/staff/Chat/:firestore_id/:message_id" component={ChatS} />
          <Private {...props} exact path="/staff/Checkin" component={CheckinS} />

          <Private {...props} exact path="/director" component={ControlPanelD} />
          <Private {...props} exact path="/director/center" component={CompleteCenterD} />
          <Private {...props} path="/director/center/:id" component={CompleteCenterD} />
          <Private {...props} path="/director/classes" component={CreateClassD} />
          <Private {...props} path="/director/tutores" component={TutoresD} />
          <Private {...props} path="/director/menu" component={MenuD} />
          <Private {...props} path="/director/message" component={MessageD} />
          <Private {...props} exact path="/director/vistagroup/:id" component={VistagroupD} />
          <Private {...props} exact path="/director/perfil/:id" component={PerfilD} />
          <Private {...props} exact path="/director/crearevento" component={CreareventoD} />
          <Private {...props} path="/director/crearevento/:id" component={CreareventoD} />
          <Private {...props} path="/director/lannister" component={LannisterD} />
          <Private {...props} path="/director/Chat/:firestore_id/:message_id" component={ChatD} />
          <Private {...props} path="/director/Checkin" component={CheckinD} />
          <Private {...props} exact path="/director/actividades" component={ActividadesD} />
          <Private {...props} path="/director/actividades/media" component={MediaD} />
          <Private {...props} path="/director/diario" component={DiarioD} />
          <Private {...props} path="/director/avisos" component={AvisosD} />

          <Route path="*">
            <Redirect to="/" />
          </Route>
        </Switch>
      </RootContainer>
    </Router>
  );
};

const mapStateToProps = state => {
  return {
    status: state.Auth.infor.active,
    isAuthentication: state.Auth.token === null ? false : true
  };
};

export default connect(mapStateToProps)(AppRouter);
