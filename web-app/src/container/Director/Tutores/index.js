import React from 'react';
import _ from 'lodash';
import constant from 'utils/constant';
import { connect } from 'react-redux';
import { HeaderBack, Loading } from 'components';
import AddStaff from './Add';
import { staffActions } from 'store/actions';
import './index.scss';

class Tutores extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false,
      isEditable: false,
      params: {
        typeOfUser: 'staff',
        email: '',
        profile: {
          photo: null,
          firstName: '',
          lastName: '',
          telephone: ''
        }
      }
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onToggle = (
    value,
    isEditable = false,
    params = {
      typeOfUser: 'staff',
      email: '',
      profile: {
        photo: null,
        firstName: '',
        lastName: '',
        telephone: ''
      }
    }
  ) => {
    this.setState({
      modal: value,
      isEditable,
      params
    });
  };

  renderClass = () => {
    return _.map(this.props.list, (item, index) => (
      <React.Fragment key={index}>
        <ion-row class="wapper-item">
          <ion-col size="12">
            <div className="left-content">
              <ion-img
                src={item.profile.photo ? item.profile.photo : constant.user}
                alt="logo"
                class="icon-back"
              />
              {item.profile.firstName}
            </div>
            <div className="right-content">
              <ion-img
                src="/images/icon/pen-edit.svg"
                alt="logo"
                class="icon-back"
                onClick={() => this.onToggle(true, true, item)}
              />
            </div>
          </ion-col>
        </ion-row>
      </React.Fragment>
    ));
  };

  onSubmit = (success, fail) => {
    const { params, isEditable } = this.state;
    const body = {
      email: params.email,
      profile: params.profile,
      typeOfUser: 'staff'
    };
    if (isEditable) {
      delete body.typeOfUser;
      this.props.edit(params.id, body, success, fail);
    } else {
      this.props.create(body, success, fail);
    }
  };

  onChange = e => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      params: {
        ...this.state.params,
        profile: {
          ...this.state.params.profile,
          [key]: value
        }
      }
    });
  };

  onChangeEmail = e => {
    const value = e.target.value;
    this.setState({
      params: {
        ...this.state.params,
        email: value
      }
    });
  };

  render() {
    const { loading, modal, params, isEditable } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Tutores" />
        <ion-content>
          <ion-grid class="main-content-tutores">
            {loading ? <Loading /> : this.renderClass()}
          </ion-grid>
          <AddStaff
            status={modal}
            onSubmit={this.onSubmit}
            onClose={this.onToggle}
            onChange={this.onChange}
            onChangeEmail={this.onChangeEmail}
            params={params}
            isEditable={isEditable}
            onDeactive={this.props.onDeactive}
          />
        </ion-content>
        <ion-footer class="add-new-tutores">
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center-Tutores">
                <ion-img
                  src="/images/new.svg"
                  allt="avatar"
                  class="add-new-tutores"
                  onClick={() => this.onToggle(true)}
                />
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.Staff.list
  };
};

const mapDispatchToProps = {
  fetch: staffActions.get,
  create: staffActions.create,
  edit: staffActions.edit,
  onDeactive: staffActions.deactive
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tutores);
