import React from 'react';
import { connect } from 'react-redux';
import { IonButton, IonModal, IonPopover } from '@ionic/react';
import { Loading, Upload } from 'components';
import { alertActions } from 'store/actions';

class AddStaff extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      agree: false,
      error: {
        status: false,
        msg: ''
      },
      showDeactive: false
    };
  }

  agreed = () => {
    this.setState({
      agree: !this.state.agree
    });
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.onClose(false);
        this.props.alert('Guardado');
      }
    );
  };

  fail = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  onSubmit = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.onSubmit(this.success, this.fail);
      }
    );
  };

  uploadSuccess = value => {
    const e = {
      target: {
        name: 'photo',
        value: value
      }
    };
    this.props.onChange(e);
  };

  uploadFail = () => {};

  // deactive handle
  showDeactive = value => {
    this.setState({
      showDeactive: value
    });
  };
  onDeactive = () => {
    this.props.onDeactive(this.props.params.id, this.deactiveSuccess, this.fail);
  };
  deactiveSuccess = () => {
    this.setState(
      {
        showDeactive: false
      },
      () => {
        this.props.onClose(false);
      }
    );
  };

  render() {
    const { status, onClose, params, isEditable } = this.props;
    const { error, showDeactive } = this.state;
    return (
      <React.Fragment>
        <IonModal
          isOpen={status}
          class="main-content-add-child"
          onDidDismiss={() => onClose(false)}
        >
          <button className="close" onClick={() => onClose(false)}>
            <img src="/images/close.svg" />
          </button>
          <ion-content>
            <ion-grid>
              <ion-row>
                <ion-col class="ion-text-center ">
                  <ion-text class="title">{isEditable ? 'Editar tutor' : 'Añadir tutor'}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row class="row">
                <div className="form-person">
                  <Upload
                    photo={params.profile.photo}
                    success={this.uploadSuccess}
                    fail={this.uploadFail}
                  />
                  <input
                    type="text"
                    placeholder="Nombre"
                    name="firstName"
                    required
                    value={params.profile.firstName}
                    onChange={e => this.props.onChange(e)}
                  />
                  <input
                    type="text"
                    placeholder="Apellidos"
                    name="lastName"
                    required
                    value={params.profile.lastName}
                    onChange={e => this.props.onChange(e)}
                  />
                  <input
                    type="text"
                    placeholder="Mail*"
                    name="email"
                    required
                    value={params.email}
                    onChange={e => this.props.onChangeEmail(e)}
                  />
                  <input
                    type="text"
                    placeholder="Teléfono"
                    name="telephone"
                    required
                    value={params.profile.telephone}
                    onChange={e => this.props.onChange(e)}
                  />
                </div>
              </ion-row>
              {error.status && (
                <ion-row class="wapper-mess">
                  <ion-col class="ion-text-center">
                    <p className="text-err">{error.msg}</p>
                  </ion-col>
                </ion-row>
              )}
              {isEditable && (
                <ion-row>
                  <ion-col class="deactive-wrapper">
                    <div className="deactive-staff" onClick={() => this.showDeactive(true)}>
                      Inactivar tutor
                    </div>
                  </ion-col>
                </ion-row>
              )}
              <ion-row class="btn-wapper ion-text-center">
                {this.state.loading ? (
                  <Loading />
                ) : (
                  <IonButton
                    onClick={() => this.onSubmit()}
                    color="danger"
                    type="submit"
                    disabled={this.state.agree}
                  >
                    {isEditable ? 'Guardar' : 'Crear'}
                  </IonButton>
                )}
              </ion-row>
            </ion-grid>
          </ion-content>
        </IonModal>
        <IonPopover
          isOpen={showDeactive}
          cssClass="popup-edit-class"
          onDidDismiss={() => this.showDeactive(false)}
        >
          <ion-content>
            <ion-grid>
              <ion-row>
                <ion-col>
                  <h3 className="title edit-message">¿ Seguro que quieres inactivar este tutor ?</h3>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col class="button">
                  <ion-text class="text-btn text-right" onClick={() => this.showDeactive(false)}>
                    Cancelar
                  </ion-text>
                  <ion-text class="text-btn" onClick={() => this.onDeactive()}>
                    Inactivar
                  </ion-text>
                </ion-col>
              </ion-row>
            </ion-grid>
          </ion-content>
        </IonPopover>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddStaff);
