import React from 'react';
import _ from 'lodash';
import { IonModal, IonCol, IonRow } from '@ionic/react';

export default class Detail extends React.Component {
  total = () => {
    const { list, type } = this.props;
    let total = 0;
    let child = 0;
    _.forEach(list, item => {
      total += item[type].length;
      child += item.classes.child.length;
    });
    return `${total}/${child}`;
  };
  render() {
    const { status, onClose, list, type } = this.props;
    return (
      <IonModal isOpen={status} class="report-container" onDidDismiss={() => onClose(false)}>
        <button className="close" onClick={() => onClose(false)}>
          <img src="/images/close.svg" />
        </button>
        <ion-content>
          <ion-grid>
            <ion-row>
              <ion-col class="title">
                <ion-text>Información actual de clases</ion-text>
              </ion-col>
            </ion-row>
            <ion-row class="thead">
              <ion-col>Nombre de clase</ion-col>
              <ion-col>Niñ@s</ion-col>
            </ion-row>
            <IonRow class="trow">
              <IonCol size="11">
                {_.map(list, item => (
                  <ion-row class="tbody">
                    <ion-col>{item.classes.name}</ion-col>
                    <ion-col>
                      {item[type].length}/{item.classes.child.length}
                    </ion-col>
                  </ion-row>
                ))}
              </IonCol>
            </IonRow>
            <IonRow class="trow">
              <IonCol size="11">
                <ion-row class="tbody">
                  <ion-col>Total</ion-col>
                  <ion-col>{this.total()}</ion-col>
                </ion-row>
              </IonCol>
            </IonRow>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}
