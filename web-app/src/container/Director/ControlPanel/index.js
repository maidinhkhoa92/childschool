import React from 'react';
import _ from 'lodash';
import { HeaderDirector, Loading } from 'components';
import { connect } from 'react-redux';
import { authActions, checkinActions, childActions } from 'store/actions';
import { IonInput } from '@ionic/react';
import history from 'utils/history';
import Detail from './detail';
import './index.scss';

class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
      type: 'checkin',
      loading: false
    };
  }
  componentDidMount() {
    this.props.getCheckins(this.success, this.fail);
    this.props.search({ word: '' }, this.success, this.fail);
  }

  success = () => {
    this.setState({ loading: false });
  };

  fail = () => {
    this.setState({ loading: false });
  };

  logout = () => {
    this.props.logout(() => {
      history.push('/');
    });
  };

  countCheckin = type => {
    const { checkins } = this.props;
    let count = 0;

    _.forEach(checkins, item => {
      count += item[type].length;
    });

    if (count === 0) {
      return '--';
    }
    return count;
  };
  onToggle = (value, type = 'checkin') => {
    this.setState({
      toggle: value,
      type
    });
  };
  search = e => {
    const body = {
      word: e.target.value
    };
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.search(body, this.success, this.fail);
      }
    );
  };
  renderChild = () => {
    return _.map(this.props.result.list, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => history.push('/director/perfil/' + item.id)}>
          {this.showAvatar(item.profile.photo)}
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };
  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} allt="avatar" className="wapper-img" />;
    }
    return (
      <div className="wapper-img">
        <ion-img class="img-icon" src="/images/child.svg" alt="logo" />
      </div>
    );
  };
  render() {
    const { center, checkins, result } = this.props;
    const { toggle, type, loading } = this.state;
    return (
      <React.Fragment>
        <ion-content>
          <HeaderDirector onLogout={this.logout} name={center.profile.centerName} />
          <ion-grid class="main-content">
            <ion-row>
              <ion-col class="ion-text-center title">
                <ion-text class="control-panel-title">EN VIVO</ion-text>
              </ion-col>
            </ion-row>
            <div className="box-info">
              <div className="box" onClick={() => this.onToggle(true, 'checkin')}>
                <ion-text class="text-number">{this.countCheckin('checkin')}</ion-text>
                <ion-text class="text-bottom">Check-in</ion-text>
              </div>
              <div className="box" onClick={() => this.onToggle(true, 'absent')}>
                <ion-text class="text-number">{this.countCheckin('absent')}</ion-text>
                <ion-text class="text-bottom">No vendrán</ion-text>
              </div>
              <div className="box" onClick={() => this.onToggle(true, 'checkout')}>
                <ion-text class="text-number">{this.countCheckin('checkout')}</ion-text>
                <ion-text class="text-bottom">Check-out</ion-text>
              </div>
            </div>
            <div className="all-menu">
              <ion-row class="search-box">
                <ion-icon name="search" class="search-icon" />
                <IonInput
                  placeholder="Encuentra más rápido un/a niñ@"
                  onIonChange={e => this.search(e)}
                />
              </ion-row>
              {loading ? (
                <Loading />
              ) : result.status ? (
                <ion-row class="wapper-item">{this.renderChild()}</ion-row>
              ) : (
                <React.Fragment>
                  <ion-row class="menu">
                    <ion-col class="menu-item" onClick={() => history.push('/director/classes')}>
                      <ion-img src="/images/icon/setting.svg" alt="logo" class="icon-back" />
                      <ion-text>Administrar clases</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="menu">
                    <ion-col class="menu-item" onClick={() => history.push('/director/message')}>
                      <ion-img src="/images/icon/charlar (1).svg" alt="logo" class="icon-back" />
                      <ion-text>Enviar mensaje colectivo</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="menu">
                    <ion-col class="menu-item" onClick={() => history.push('/director/tutores')}>
                      <ion-img src="/images/icon/total.svg" alt="logo" class="icon-back" />
                      <ion-text>Añadir tutor</ion-text>
                    </ion-col>
                  </ion-row>
                  <ion-row class="menu">
                    <ion-col class="menu-item" onClick={() => history.push('/director/menu')}>
                      <ion-img
                        src="/images/icon/caja-de-almuerzo.svg"
                        alt="logo"
                        class="icon-back"
                      />
                      <ion-text>Menú</ion-text>
                    </ion-col>
                  </ion-row>
                </React.Fragment>
              )}
            </div>
          </ion-grid>
          <Detail status={toggle} onClose={this.onToggle} list={checkins} type={type} />
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    center: state.Auth.infor,
    checkins: state.Checkin.list,
    result: state.Child.search
  };
};

const mapDispatchToProps = {
  logout: authActions.logout,
  getCheckins: checkinActions.list,
  search: childActions.search
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ControlPanel);
