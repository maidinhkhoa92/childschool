import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { IonButton } from '@ionic/react';
import { Loading, Upload } from 'components';
import { authActions, alertActions } from 'store/actions';
import { District, Cities } from 'utils/cities';
import './index.scss';

class CompleteCenter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      profile: {
        centerName: '',
        firstName: '',
        lastName: '',
        telephone: '',
        postcode: '',
        city: '',
        district: '',
        email: '',
        address: '',
        photo: null
      },
      loading: false,
      agree: false,
      isEditable: false,
      selectedDistrict: 'Andalucía'
    };
  }

  componentDidMount() {
    const { params, match } = this.props;
    const isEditable = match.params.id === 'edit' ? true : false;
    if (isEditable) {
      this.setState(prev => ({
        isEditable: isEditable,
        profile: {
          ...prev.profile,
          ...params.profile
        },
        email: params.email,
        selectedDistrict: params.profile.district
      }));
    }
  }

  onEmail = e => {
    const value = e.target.value;

    this.setState({
      email: value
    });
  };

  onChange = e => {
    const value = e.target.value;
    const key = e.target.name;

    this.setState({
      profile: {
        ...this.state.profile,
        [key]: value
      }
    });
  };

  agreed = () => {
    this.setState({
      agree: !this.state.agree
    });
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        history.push('/director/');
        this.props.alert('Guardado');
      }
    );
  };

  fail = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  onSubmit = () => {
    const { profile, email } = this.state;
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.onSubmit({ profile, email }, this.success, this.fail);
      }
    );
  };

  uploadSuccess = value => {
    this.setState({
      profile: {
        ...this.state.profile,
        photo: value
      }
    });
  };

  uploadFail = message => {
    this.props.alert(message);
  };

  onSelectDistrict = e => {
    const value = e.target.value;
    this.setState({
      selectedDistrict: value,
      profile: {
        ...this.state.profile,
        district: value,
        city: Cities.get(value)[0]
      }
    });
  };

  renderCities = () => {
    const selectedCities = Cities.get(this.state.selectedDistrict);
    return _.map(selectedCities, (item, key) => (
      <option key={key} value={item}>
        {item}
      </option>
    ));
  };

  render() {
    const { profile, email, isEditable } = this.state;
    return (
      <ion-content>
        <ion-grid class="main-content-complete">
          <ion-row>
            <ion-col class="ion-text-center ">
              <ion-text class="title bold">Perfil de la guardería</ion-text>
            </ion-col>
          </ion-row>
          <ion-row class="row">
            <ion-text class="title-person">Datos del centro</ion-text>
            <div className="form-person">
              <input
                type="text"
                placeholder="Nombre del centro"
                name="centerName"
                value={profile.centerName}
                required
                onChange={e => this.onChange(e)}
              />
              <input
                type="email"
                placeholder="Email de contacto"
                name="email"
                value={email}
                required
                onChange={e => this.onEmail(e)}
              />
              <input
                type="text"
                placeholder="Número de contacto"
                name="hotline"
                value={profile.hotline}
                required
                onChange={e => this.onChange(e)}
              />
              <select
                placeholder="Comunidad autónoma"
                name="district"
                interface="popover"
                value={profile.district}
                required
                onChange={e => this.onSelectDistrict(e)}
              >
                {_.map(District, (item, key) => (
                  <option key={key} value={item}>
                    {item}
                  </option>
                ))}
              </select>
              <select
                placeholder="Ciudad"
                name="city"
                interface="popover"
                value={profile.city}
                required
                onChange={e => this.onChange(e)}
              >
                {this.renderCities()}
              </select>
              <input
                type="text"
                placeholder="Código postal"
                name="postcode"
                value={profile.postcode}
                required
                onChange={e => this.onChange(e)}
              />
              <label>Dirección</label>
              <textarea
                rows="2"
                name="address"
                value={profile.address}
                onChange={e => this.onChange(e)}
              />
            </div>
          </ion-row>
          <ion-row class="row">
            <ion-text class="title-person">Datos del director/a</ion-text>
            <div className="form-person">
              <Upload photo={profile.photo} success={this.uploadSuccess} fail={this.uploadFail} />
              <input
                type="text"
                placeholder="Nombre"
                name="firstName"
                value={profile.firstName}
                required
                onChange={e => this.onChange(e)}
              />
              <input
                type="text"
                placeholder="Apellidos"
                name="lastName"
                value={profile.lastName}
                required
                onChange={e => this.onChange(e)}
              />
              <input
                type="text"
                placeholder="Email"
                name="email"
                value={profile.email}
                required
                onChange={e => this.onChange(e)}
              />
              <input
                type="text"
                placeholder="Teléfono"
                name="telephone"
                value={profile.telephone}
                required
                onChange={e => this.onChange(e)}
              />
            </div>
          </ion-row>
          <ion-row class="btn-wapper ion-text-center">
            {this.state.loading ? (
              <Loading />
            ) : (
              <IonButton
                onClick={() => this.onSubmit()}
                color="danger"
                type="submit"
                disabled={this.state.agree}
              >
                {isEditable ? 'Guardar ' : 'Accede'}
              </IonButton>
            )}
          </ion-row>
        </ion-grid>
      </ion-content>
    );
  }
}

const mapStateToProps = state => {
  return {
    params: state.Auth.infor
  };
};

const mapDispatchToProps = {
  onSubmit: authActions.completeDirector,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompleteCenter);
