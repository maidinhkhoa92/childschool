import React from 'react';
import { connect } from 'react-redux';
import { HeaderBack, Loading } from 'components';
import { menuActions, alertActions } from 'store/actions';
import './index.scss';

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch({}, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onSubmit = e => {
    const file = e.target.files[0];
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.create(file, this.uploadSuccess, this.uploadFail);
      }
    );
  };

  uploadSuccess = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert('Guardado');
      }
    );
  };

  uploadFail = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  render() {
    const { menu, name } = this.props;
    const { loading } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Menú vigente" />
        <ion-content>
          <ion-grid class="main-content-menu">
            <ion-row class="wapper-item">
              <ion-col>
                <a href={menu} className="left-menu" target="blank">
                  <ion-img src="/images/pdf.svg" />
                  {name}
                </a>
              </ion-col>
              <ion-col class="right-menu">
                {loading ? (
                  <Loading />
                ) : (
                  <div className="upload-btn-wrapper">
                    <button className="btn">Cargar menú</button>
                    <input type="file" name="myfile" onChange={e => this.onSubmit(e)} />
                  </div>
                )}
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <div className="note">
                  *Los padres tendrán acceso a la información que aparezca en el archivo.
                </div>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    menu: state.Menu.url ? state.Menu.url : '',
    name: state.Menu.name
  };
};

const mapDispatchToProps = {
  fetch: menuActions.get,
  create: menuActions.create,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Menu);
