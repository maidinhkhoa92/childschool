import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { IonButton, IonPopover } from '@ionic/react';
import { HeaderBack } from 'components';
import TimeKeeper from 'react-timekeeper';
import './index.scss';

class Avisos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      params: {},
      modal: false,
      noteId: '',
      time: '12:30'
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false,
      modal: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  renderNote = () => {
    const { list } = this.props;
    return _.map(list, (item, key) => (
      <React.Fragment key={key}>
        <ion-row>
          <ion-col>
            <div className="note-wrapper">
              <div className="note-text" onClick={() => this.toggleModal(true, item.id)}>
                {item.description}
              </div>
              {item.time && <div className="note-time">{item.time}</div>}
            </div>
          </ion-col>
        </ion-row>
      </React.Fragment>
    ));
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        ...prev.params,
        [key]: value
      }
    }));
  };

  onCreate = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.create(this.state.params, this.success, this.fail);
      }
    );
  };

  setTime = value => {
    this.setState({
      time: value
    });
  };

  onUpdate = () => {
    const { noteId, time } = this.state;
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.update(noteId, { time: time }, this.success, this.fail);
      }
    );
  };

  toggleModal = (value, noteId = '') => {
    this.setState({
      modal: value,
      noteId: noteId
    });
  };

  render() {
    const { modal } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Avisos" />
        <Ion-content>
          <ion-grid class="main-content-avisos">
            {this.renderNote()}
            <div className="bottom">
              <ion-row>
                <ion-col>
                  <textarea
                    name="description"
                    onChange={e => this.onChange(e)}
                    className="message-area-input"
                    placeholder="Nota opcional"
                  />
                </ion-col>
              </ion-row>
              <ion-row class="btn-wapper">
                <IonButton onClick={() => this.onCreate()} color="danger" type="submit">
                  Enviar aviso
                </IonButton>
              </ion-row>
            </div>
          </ion-grid>
        </Ion-content>
        <IonPopover class="large" isOpen={modal} onDidDismiss={() => this.toggleModal(false, '')}>
          <TimeKeeper hour24Mode onChange={newTime => this.setTime(newTime.formatted24)} />
          <div className="actions">
            <ion-text class="text-btn text-right" onClick={() => this.toggleModal(false)}>
              Cancelar
            </ion-text>
            <ion-text class="text-btn" onClick={() => this.onUpdate()}>
              Borrar
            </ion-text>
          </div>
        </IonPopover>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.Note.list
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Avisos);
