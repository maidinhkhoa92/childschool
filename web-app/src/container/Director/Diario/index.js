import React, { Component } from 'react';
import './index.scss';
import _ from 'lodash';
import moment from 'moment';
import { FormatMonth, FormatDay } from 'utils/moment';
import { connect } from 'react-redux';
import { HeaderBack, Loading } from 'components';
import { newsActions } from 'store/actions';
import { convertImage } from 'utils/convert';
import Detail from './Detail';

class Diario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTime: new Date(),
      loading: false,
      toggle: false,
      list: []
    };
  }
  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        const data = {
          child_id: this.props.child_id,
          date: FormatMonth(this.state.currentTime),
          type: 'month'
        };
        this.props.fetch(data, this.success, this.success);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  renderAgenda = () => {
    const { list } = this.props;
    return _.map(list, (item, key) => {
      return (
        <ion-col
          key={key}
          id="text"
          size="4"
          class="image-content"
          onClick={() => this.onToggle(true, item.items)}
        >
          <img src={convertImage(item.items[0])} />
          <h1>{FormatDay(item.day)}</h1>
        </ion-col>
      );
    });
  };
  changeMonth = value => {
    let time = moment(this.state.currentTime);
    if (value === true) {
      time = time.add(1, 'months');
    } else {
      time = time.subtract(1, 'months');
    }
    this.setState(
      {
        currentTime: time
      },
      () => {
        const data = {
          child_id: this.props.child_id,
          date: FormatMonth(this.state.currentTime),
          type: 'month'
        };
        this.props.fetch(data, this.success, this.success);
      }
    );
  };

  onToggle = (value, list = []) => {
    this.setState({
      toggle: value,
      list: list
    });
  };

  render() {
    const { currentTime, loading, toggle, list } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Diario" />
        <ion-content>
          <ion-grid class="diario-main-content">
            <ion-row class="content">
              <ion-img
                src="/images/left.svg"
                class="image-header-content"
                onClick={() => this.changeMonth(false)}
              />
              <span className="text-content">{FormatMonth(currentTime)}</span>
              <ion-img
                src="/images/right.svg"
                class="image-header-content"
                onClick={() => this.changeMonth(true)}
              />
            </ion-row>

            <ion-row>{loading ? <Loading /> : this.renderAgenda()}</ion-row>
          </ion-grid>
        </ion-content>
        <Detail list={list} status={toggle} onClose={this.onToggle} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.News.list,
    child_id: state.Child.detail.id
  };
};

const mapDispatchToProps = {
  fetch: newsActions.get
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Diario);
