import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { HeaderBack, Loading } from 'components';
import { classesActions, alertActions } from 'store/actions';
import './index.scss';

class CreateClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      toggle: false,
      params: {},
      error: {
        status: false,
        msg: ''
      }
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.success, this.fail);
      }
    );
  }

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.setState({
      params: {
        [key]: value
      }
    });
  };

  success = () => {
    this.setState({
      loading: false,
      toggle: false
    });
  };

  fail = () => {
    this.setState({
      loading: false,
      toggle: false
    });
  };

  onToggle = value => {
    this.setState({
      toggle: value
    });
  };

  renderClass = () => {
    return _.map(this.props.list, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="4">
          <div
            className="tab-icon"
            style={{ background: item.color }}
            onClick={() => history.push('/director/vistagroup/' + item.id)}
          >
            <p className="tab-text">{item.name}</p>
          </div>
        </ion-col>
      </React.Fragment>
    ));
  };

  onSubmit = () => {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.create(this.state.params, this.createSuccess, this.failSuccess);
      }
    );
  };

  createSuccess = () => {
    this.setState(
      {
        loading: false,
        toggle: false
      },
      () => {
        this.props.alert('Nueva clase creada');
      }
    );
  };

  failSuccess = message => {
    this.setState(
      {
        loading: false,
        toggle: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  render() {
    const { loading, toggle, error } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Administrar clases" />
        <ion-content class="director-classes">
          <ion-grid class="main-content">
            <ion-row class="wapper-item">
              {loading ? <Loading /> : this.renderClass()}
              <ion-col size="4">
                <div className="tab-icon icon-add" onClick={() => this.onToggle(true)}>
                  <ion-img src="/images/icon/mas.svg" allt="avatar" />
                </div>
              </ion-col>
            </ion-row>
          </ion-grid>

          {toggle && (
            <div className="backdrop">
              <div className="popup">
                <div className="title">
                  <ion-text>Nuevo grupo</ion-text>
                </div>
                <div className="form-add">
                  <input
                    type="text"
                    placeholder="Escribe el nombre"
                    name="name"
                    required
                    className="border-bottom"
                    onChange={e => this.onChange(e)}
                  />
                  {error.status && <div className="text-err">{error.msg}</div>}
                  <div className="btn">
                    <ion-text class="text-btn text-right" onClick={() => this.onToggle(false)}>
                      Cancelar
                    </ion-text>
                    {loading ? (
                      <Loading />
                    ) : (
                      <ion-text class="text-btn" onClick={() => this.onSubmit()}>
                        Crear
                      </ion-text>
                    )}
                  </div>
                </div>
              </div>
            </div>
          )}
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.Classes.list
  };
};

const mapDispatchToProps = {
  fetch: classesActions.get,
  create: classesActions.create,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateClass);
