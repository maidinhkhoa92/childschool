import React from 'react';
import { IonPopover } from '@ionic/react';

const Delete = ({ status, onClose, onSubmit }) => {
  return (
    <IonPopover isOpen={status} cssClass="popup-edit-class" onDidDismiss={() => onClose()}>
      <ion-content>
        <ion-grid>
          <ion-row>
            <ion-col>
              <h3 className="title">¿Estás segur@ que quieres eliminar el perfil?</h3>
              <p className="desc">*Ten en cuenta que puedes perder datos guardados</p>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col class="button">
              <ion-text class="text-btn text-right" onClick={() => onClose()}>
                Cancelar
              </ion-text>
              <ion-text class="text-btn" onClick={() => onSubmit()}>
                Borrar
              </ion-text>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    </IonPopover>
  );
};

export default Delete;
