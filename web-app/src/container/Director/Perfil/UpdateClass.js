import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { IonButton, IonModal } from '@ionic/react';
import { Loading } from 'components';
import { childActions, alertActions } from 'store/actions';

class AddChild extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      children: {
        firstName: '',
        lastName: ''
      },
      firstTeacher: '',
      secondTeacher: '',
      family: {
        profile: {
          firstName: '',
          lastName: '',
          telephone: ''
        },
        email: ''
      },
      loading: false,
      agree: false
    };
  }

  componentDidMount() {
    const { child } = this.props;
    this.setState({
      children: {
        firstName: child.profile.firstName,
        lastName: child.profile.lastName
      },
      firstTeacher: _.isNull(child.firstTeacher) ? '' : child.firstTeacher._id,
      secondTeacher: _.isNull(child.secondTeacher) ? '' : child.secondTeacher._id,
      family: {
        profile: child.family.profile,
        email: child.family.email
      }
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.status !== this.props.status) {
      const { child } = this.props;
      this.setState({
        children: {
          firstName: child.profile.firstName,
          lastName: child.profile.lastName
        },
        firstTeacher: _.isNull(child.firstTeacher) ? '' : child.firstTeacher._id,
        secondTeacher: _.isNull(child.secondTeacher) ? '' : child.secondTeacher._id,
        family: {
          profile: child.family.profile,
          email: child.family.email
        }
      });
    }
  }

  agreed = () => {
    this.setState({
      agree: !this.state.agree
    });
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.onClose(false);
      }
    );
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onSubmit = () => {
    const { children, firstTeacher, secondTeacher, family } = this.state;
    const { child, staffs } = this.props;
    this.setState(
      {
        loading: true
      },
      () => {
        const defaultTeacher = staffs.length > 0 ? staffs[0].id : '';
        const body = {
          children,
          firstTeacher: firstTeacher === '' ? defaultTeacher : firstTeacher,
          secondTeacher: secondTeacher === '' ? defaultTeacher : secondTeacher,
          family
        };
        this.props.onSubmit(child.id, body, this.addChildSuccess, this.addChildFail);
      }
    );
  };

  addChildSuccess = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert('Guardado');
        this.props.onClose(false);
      }
    );
  };

  addChildFail = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  onChange = e => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      children: {
        ...this.state.children,
        [key]: value
      }
    });
  };

  onChangeEmail = (state, e) => {
    const value = e.target.value;
    this.setState({
      [state]: {
        ...this.state[state],
        email: value
      }
    });
  };

  onChangeProfile = (state, e) => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      [state]: {
        ...this.state[state],
        profile: {
          ...this.state[state].profile,
          [key]: value
        }
      }
    });
  };

  onSelectTeacher = e => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      [key]: value
    });
  };

  render() {
    const { status, onClose, staffs } = this.props;
    const { children, firstTeacher, secondTeacher, family } = this.state;
    return (
      <IonModal isOpen={status} class="main-content-add-child" onDidDismiss={() => onClose()}>
        <button className="close" onClick={() => onClose()}>
          <img src="/images/close.svg" />
        </button>
        <ion-content>
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center ">
                <ion-text class="title">Editar perfil</ion-text>
              </ion-col>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Datos de niñ@</ion-text>
              <div className="form-person">
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  value={children.firstName}
                  onChange={e => this.onChange(e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  value={children.lastName}
                  onChange={e => this.onChange(e)}
                />
              </div>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Tutor 1</ion-text>
              <div className="form-person">
                <select
                  name="firstTeacher"
                  value={firstTeacher}
                  onChange={e => this.onSelectTeacher(e)}
                >
                  {_.map(staffs, (item, key) => (
                    <option key={key} value={item.id}>
                      {item.profile.firstName}
                    </option>
                  ))}
                </select>
              </div>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Tutor 2</ion-text>
              <div className="form-person">
                <select
                  name="secondTeacher"
                  value={secondTeacher}
                  onChange={e => this.onSelectTeacher(e)}
                >
                  {_.map(staffs, (item, key) => (
                    <option key={key} value={item.id}>
                      {item.profile.firstName}
                    </option>
                  ))}
                </select>
              </div>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Perfil del padre principal</ion-text>
              <div className="form-person">
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  value={family.profile.firstName}
                  onChange={e => this.onChangeProfile('family', e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  value={family.profile.lastName}
                  onChange={e => this.onChangeProfile('family', e)}
                />
                <input
                  type="text"
                  placeholder="Teléfono"
                  name="telephone"
                  required
                  value={family.profile.telephone}
                  onChange={e => this.onChangeProfile('family', e)}
                />
                <input
                  type="text"
                  placeholder="Mail*"
                  name="email"
                  required
                  value={family.email}
                  onChange={e => this.onChangeEmail('family', e)}
                />
                <div className="note">
                  *se le enviará un mail al padre principal para que complete los datos de la ficha
                  del niñ@
                </div>
              </div>
            </ion-row>
            <ion-row class="btn-wapper ion-text-center">
              {this.state.loading ? (
                <Loading />
              ) : (
                <IonButton
                  onClick={() => this.onSubmit()}
                  color="danger"
                  type="submit"
                  disabled={this.state.agree}
                >
                  Guardar
                </IonButton>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {
    staffs: state.Staff.list,
    child: state.Child.detail
  };
};

const mapDispatchToProps = {
  onSubmit: childActions.updatePerson,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddChild);
