import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { Loading, HeaderTransparent, FooterDirector } from 'components';
import { connect } from 'react-redux';
import { childActions, classesActions } from 'store/actions';
import { convertIcon, convertTime } from 'utils/convert';
import DeleteChild from './Delete';
import EditChild from './Edit';
import UpdateClass from './UpdateClass';
import { FormatTime } from 'utils/moment';
import './index.scss';
import actions from '../../../store/child/actions';

class Perfil extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      headerDropdown: false,
      phoneStatus: false,
      action: {
        edit: false,
        delete: false,
        update: false
      }
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.props.match.params.id, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  renderNews = news => {
    let showTime = false;
    let currentTime = '';
    return _.map(news, (item, i) => {
      const formatTime = FormatTime(item.time);
      if (currentTime !== formatTime) {
        showTime = true;
        currentTime = formatTime;
      } else {
        showTime = false;
      }
      return (
        <React.Fragment key={i}>
          {showTime && (
            <ion-row class="date-now-perfil">
              <div className="date-wapper">
                <p>{formatTime}</p>
              </div>
            </ion-row>
          )}
          <ion-row class="box-news">
            <ion-col size="2" class="box-left">
              <ion-img src={convertIcon(item.type)} />
            </ion-col>
            <ion-col size="10" class="box-right">
              <ion-row class="text-right">
                <ion-col>
                  <ion-text>{item.title}</ion-text>
                  <ion-text class="ion-text-right">{convertTime(item.time)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{_.join(item.note, ',')}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{_.join(item.selected, ',')}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-mid">{this.checkGroup(item.group, item.type)}</ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col>
                  <ion-text class="text-bottom">
                    {this.checkContent(item.content, item.type)}
                  </ion-text>
                </ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
        </React.Fragment>
      );
    });
  };

  checkContent = (content, type) => {
    if (type === 'Photo' || type === '') {
      return <img src={content} />;
    }
    if (type === 'Video') {
      return (
        <video width="100%" height="240" controls>
          <source src={content} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      );
    }
    return content;
  };

  checkGroup = (content, type) => {
    if (type === 'Achievements' || type === 'CheerUp') {
      return <img src={content} />;
    }
    if (type === 'FeedingBottle') {
      return content + ' ml';
    }
    return content;
  };

  // child action
  onShowSelect = () => {
    this.setState(
      {
        headerDropdown: !this.state.headerDropdown
      },
      () => {
        document.addEventListener('click', this.onCloseSelect);
      }
    );
  };

  onCloseSelect = () => {
    this.setState({ headerDropdown: false }, () => {
      document.removeEventListener('click', this.onCloseSelect);
    });
  };

  onActionClose = () => {
    this.setState({
      headerDropdown: false,
      action: {
        edit: false,
        delete: false,
        update: false
      }
    });
  };
  onShowAction = key => {
    this.setState(prev => ({
      action: {
        ...prev.action,
        [key]: true
      }
    }));
  };
  onDelete = () => {
    this.props.delete(this.props.detail.id, this.onDeleteSuccess, this.onDeleteFail);
  };
  onDeleteSuccess = () => {
    history.goBack();
  };

  onDeleteFail = () => {
    this.onActionClose();
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return (
        <img
          src={url}
          allt="avatar"
          onClick={() => this.onShowAction('edit')}
          className="wapper-avatar"
        />
      );
    }
    return (
      <div className="wapper-avatar">
        <ion-img
          src="/images/icon/bebe.svg"
          allt="avatar"
          onClick={() => this.onShowAction('edit')}
        />
      </div>
    );
  };

  // phone toggle
  togglePhone = value => {
    this.setState(
      {
        phoneStatus: value
      },
      () => {
        document.addEventListener('click', this.onClosePhoneToggle);
      }
    );
  };
  onClosePhoneToggle = () => {
    this.setState(
      {
        phoneStatus: false
      },
      () => {
        document.removeEventListener('click', this.onClosePhoneToggle);
      }
    );
  };

  render() {
    const { detail } = this.props;
    const { loading, headerDropdown, action, phoneStatus } = this.state;
    if (loading) {
      return <Loading />;
    }
    return (
      <React.Fragment>
        <ion-content>
          <HeaderTransparent
            right
            popup={headerDropdown}
            onPopup={this.onShowSelect}
            onAction={this.onShowAction}
          />
          <ion-grid class="container">
            <ion-col class="banner-img-perfil">
              {this.showAvatar(detail.profile.photo)}
              <ion-text>
                <p onClick={() => this.onShowAction('edit')}>
                  {detail.profile.firstName} {detail.profile.lastName}
                </p>
              </ion-text>
            </ion-col>
            <div className="all-perfil">{this.renderNews(detail.news)}</div>
          </ion-grid>
          <DeleteChild
            status={action.delete}
            onClose={this.onActionClose}
            onSubmit={this.onDelete}
          />
          <EditChild status={action.edit} onClose={this.onActionClose} />
          <UpdateClass status={action.update} onClose={this.onActionClose} />
        </ion-content>
        <FooterDirector toggle={this.togglePhone} phone={phoneStatus} first={detail.family} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    detail: state.Child.detail
  };
};

const mapDispatchToProps = {
  fetch: childActions.detail,
  delete: classesActions.removeChild
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Perfil);
