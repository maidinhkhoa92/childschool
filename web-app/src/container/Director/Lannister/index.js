import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import { FormatTime, FormatMonth } from 'utils/moment';
import history from 'utils/history';
import { HeaderSelect } from 'components';
import { eventActions } from 'store/actions';
import DeleteEvent from './Delete';
import './index.scss';

class Lannister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: -1,
      query: {
        date: new Date()
      },
      loading: false,
      selectedId: '',
      selectedName: '',
      showDelete: false
    };
  }

  componentDidMount() {
    const { classes } = this.props;
    this.setState(
      prev => ({
        query: {
          ...prev.query,
          class_id: classes[0].id
        },
        selectedValue: classes[0].name
      }),
      () => {
        this.props.fetch(this.state.query, this.success, this.fail);
      }
    );
  }

  onToggle = (key, item) => {
    if (this.state.toggle === key) {
      key = -1;
    }
    this.setState(
      {
        toggle: key,
        item: item
      },
      () => {
        document.addEventListener('click', this.onCloseToggle);
      }
    );
  };

  onCloseToggle = () => {
    this.setState(
      {
        toggle: -1,
        item: {}
      },
      () => {
        document.removeEventListener('click', this.onCloseToggle);
      }
    );
  };

  onRightPress = () => {
    history.push('/director/crearevento');
  };

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onChangeDate = (status = true) => {
    let time = moment(this.state.query.date);
    if (status === true) {
      time = time.add(1, 'months');
    } else {
      time = time.subtract(1, 'months');
    }
    this.setState(
      prev => ({
        query: {
          ...prev.query,
          date: time
        }
      }),
      () => {
        this.props.fetch(this.state.query, this.success, this.fail);
      }
    );
  };

  renderEvent = () => {
    return _.map(this.props.events, (item, key) => (
      <React.Fragment key={key}>
        <ion-row class="row2">
          <span className="text-row2">{FormatTime(item.date)}</span>
        </ion-row>

        <ion-row class="row3">
          <ion-row class="row3-1">
            <span className="text-row3">{item.name}</span>
            <span className="text-row3"><b>desde</b>{item.startTime} hrs</span>
            <span className="text-row3"><b>hasta</b>{item.endTime} hrs</span>
            <span className="popup-wrapper">
              <ion-img
                src="/images/dot.svg"
                class="icon-row3"
                onClick={() => this.onToggle(key, item)}
              />
              {this.state.toggle === key && (
                <div className="popup-action">
                  <span onClick={() => history.push('/director/crearevento/' + item.id)}>
                    Editar
                  </span>
                  <span onClick={() => this.onShowdelete(true, item.id, item.name)}>Borrar</span>
                </div>
              )}
            </span>
          </ion-row>

          <ion-row class="row3-2">
            <span className="textin-row3">Grupo: </span>
            <span className="text-row3">{item.group.name}</span>
          </ion-row>

          <ion-row class="row3-3">
            <span className="textin-row3">Nota: </span>
            <span className="text-row3">{item.note}</span>
          </ion-row>
        </ion-row>
      </React.Fragment>
    ));
  };

  // delete action
  onShowdelete = (value, id = '', name = '') => {
    this.setState({
      showDelete: value,
      selectedId: id,
      selectedName: name
    });
  };

  onDelete = () => {
    this.props.delete(this.state.selectedId, this.deleteSuccess, this.deleteFail);
  };

  deleteSuccess = () => {
    this.setState({
      showDelete: false,
      toggle: -1
    });
  };

  // select classes
  onSelect = item => {
    this.setState(
      prev => ({
        query: {
          ...prev.query,
          class_id: item
        },
        selectedValue: item.name
      }),
      () => {
        this.props.fetch(this.state.query, this.success, this.fail);
      }
    );
  };

  deleteFail = () => {};

  render() {
    const { showDelete, query, selectedValue, selectedName } = this.state;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <HeaderSelect
          onPress={this.onSelect}
          right
          onRightPress={this.onRightPress}
          list={classes}
          selectedValue={selectedValue}
        />

        <ion-content class="staff-lannister">
          <ion-grid class="all-lannister">
            <ion-row class="row1">
              <ion-img
                src="/images/left.svg"
                class="icon-row1"
                onClick={() => this.onChangeDate(false)}
              />
              <ion-text class="text-row1">{FormatMonth(query.date)}</ion-text>
              <ion-img
                src="/images/right.svg"
                class="icon-row1"
                onClick={() => this.onChangeDate(true)}
              />
            </ion-row>
            {this.renderEvent()}
            {/* <ion-row class="icon-group">
              <ion-img src="/images/group.svg" />
            </ion-row> */}
          </ion-grid>
          <DeleteEvent
            status={showDelete}
            onClose={() => this.onShowdelete(false)}
            onSubmit={this.onDelete}
            name={selectedName}
          />
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    classes: state.Classes.list,
    events: state.Event.list
  };
};

const mapDispatchToProps = {
  fetch: eventActions.get,
  delete: eventActions.delete
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Lannister);
