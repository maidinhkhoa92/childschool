import React from 'react';
import { IonPopover } from '@ionic/react';

class EditClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {}
    };
  }
  componentDidMount() {
    const name = this.props.name;
    this.setState({
      params: {
        name: name
      }
    });
  }
  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        ...prev.params,
        [key]: value
      }
    }));
  };
  onSubmit = () => {
    this.props.onSubmit(this.state.params);
  };
  render() {
    const { status, onClose } = this.props;
    const { params } = this.state;
    return (
      <IonPopover isOpen={status} cssClass="popup-edit-class">
        <ion-content>
          <ion-grid>
            <ion-row>
              <ion-col>
                <h3 className="title">Editar nombre</h3>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <input
                  type="text"
                  name="name"
                  onChange={e => this.onChange(e)}
                  value={params.name}
                />
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col class="button">
                <ion-text class="text-btn text-right" onClick={() => onClose()}>
                  Cancelar
                </ion-text>
                <ion-text class="text-btn" onClick={() => this.onSubmit()}>
                  Guardar
                </ion-text>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonPopover>
    );
  }
}

export default EditClass;
