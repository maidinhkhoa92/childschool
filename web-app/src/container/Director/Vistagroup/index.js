import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { HeaderClasses, Loading, FooterClassDirector } from 'components';
import { classesActions, alertActions } from 'store/actions';
import AddChild from './AddChild';
import DeleteClass from './Delete';
import EditClass from './Edit';
import './index.scss';

class Vistagroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false,
      header: false,
      classes: {
        edit: false,
        delete: false
      }
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetch(this.props.match.params.id, this.success, this.fail);
      }
    );
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onShowModal = value => {
    this.setState({
      modal: value
    });
  };

  renderChild = () => {
    return _.map(this.props.childs, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="3" onClick={() => history.push('/director/perfil/' + item.id)}>
          {this.showAvatar(item.profile.photo)}
          <p className="text-item">{item.profile.firstName}</p>
        </ion-col>
      </React.Fragment>
    ));
  };

  // classes handle
  onActionClose = () => {
    this.setState({
      classes: {
        edit: false,
        delete: false
      }
    });
  };

  onPopupHeader = event => {
    event.preventDefault();
    this.setState(
      {
        header: !this.state.header
      },
      () => {
        document.addEventListener('click', this.onPopupClose);
      }
    );
  };

  onPopupClose = () => {
    this.setState({ header: false }, () => {
      document.removeEventListener('click', this.onPopupClose);
    });
  };

  onActionHeader = key => {
    this.setState(prev => ({
      header: false,
      classes: {
        ...prev.classes,
        [key]: true
      }
    }));
  };

  onDelete = () => {
    this.props.delete(this.props.detail.id, this.onDeleteSuccess, this.onDeleteFail);
  };

  onDeleteSuccess = () => {
    history.goBack();
    this.props.alert('Clase borrada');
  };

  onDeleteFail = message => {
    this.onActionClose();
    this.props.alert(message);
  };

  onEdit = params => {
    this.props.update(this.props.detail.id, params, this.onEditSuccess, this.onEditFail);
  };

  onEditSuccess = () => {
    this.onActionClose();
    this.props.alert('Clase editada');
  };

  onEditFail = message => {
    this.props.alert(message);
  };

  groupColor = () => {
    let { detail } = this.props;

    if (this.state.loading) {
      detail = '#fff';
    }

    return { '--background': detail.color };
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return <img src={url} allt="avatar" className="wapper-img" />;
    }
    return (
      <div className="wapper-img">
        <ion-img class="img-icon" src="/images/child.svg" alt="logo" />
      </div>
    );
  };

  render() {
    const { loading, modal, header, classes } = this.state;
    const { detail, childs } = this.props;
    const count = childs.length;
    if (loading) {
      return <Loading />;
    }
    return (
      <React.Fragment>
        <HeaderClasses
          right={true}
          popup={header}
          onPopup={this.onPopupHeader}
          onAction={this.onActionHeader}
          color={loading ? '#fff' : detail.color}
        />
        <ion-content class="vistagroup-wrapper" style={this.groupColor()}>
          <ion-grid class="vistagroup">
            <ion-text class="ion-text-center">
              <p className="text-title">{`${detail.name} [${count}]`}</p>
            </ion-text>
            <ion-row class="wapper-item">{this.renderChild()}</ion-row>
          </ion-grid>
          <AddChild status={modal} onClose={this.onShowModal} />
          <EditClass
            status={classes.edit}
            onClose={this.onActionClose}
            onSubmit={this.onEdit}
            name={detail.name}
          />
          <DeleteClass
            status={classes.delete}
            onClose={this.onActionClose}
            onSubmit={this.onDelete}
          />
        </ion-content>
        <FooterClassDirector onPress={this.onShowModal} />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    detail: state.Classes.detail,
    childs: state.Classes.child
  };
};

const mapDispatchToProps = {
  fetch: classesActions.detail,
  delete: classesActions.delete,
  update: classesActions.update,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Vistagroup);
