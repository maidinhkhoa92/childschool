import React from 'react';
import { IonPopover } from '@ionic/react';

const DeleteClass = ({ status, onClose, onSubmit }) => {
  return (
    <IonPopover isOpen={status} cssClass="popup-edit-class" onDidDismiss={() => onClose()}>
      <ion-content>
        <img src="/images/alert.svg" className="alert-image" />
        <ion-grid>
          <ion-row>
            <ion-col>
              <h3 className="title title-delete">¿Estás segur@ que quieres borrar la clase seleccionada?</h3>
              <p className="desc">
                *En caso de borrar la clase perderás los datos relativos a todos los estudiantes que
                pertenecen a esta
              </p>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col class="button">
              <ion-text class="text-btn text-right" onClick={() => onClose()}>
                Cancelar
              </ion-text>
              <ion-text class="text-btn" onClick={() => onSubmit()}>
                Borrar
              </ion-text>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    </IonPopover>
  );
};

export default DeleteClass;
