import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { IonButton, IonModal } from '@ionic/react';
import { Loading } from 'components';
import { classesActions, staffActions, alertActions } from 'store/actions';

class AddChild extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      children: {},
      firstTeacher: '',
      secondTeacher: '',
      family: {},
      loading: false,
      agree: false
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetchStaff(this.fetchStaffSuccess, this.fetchStaffFail);
      }
    );
  }

  fetchStaffSuccess = () => {
    this.setState({
      loading: false
    });
  };
  fetchStaffFail = () => {
    this.setState({
      loading: false
    });
  };

  agreed = () => {
    this.setState({
      agree: !this.state.agree
    });
  };

  success = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.onClose(false);
      }
    );
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onSubmit = () => {
    const { children, firstTeacher, secondTeacher, family } = this.state;
    if (firstTeacher === secondTeacher) {
      this.props.alert('Tutor repetido');
    } else {
      this.setState(
        {
          loading: true
        },
        () => {
          const defaultTeacher = this.props.staffs.length > 0 ? this.props.staffs[0].id : '';
          const body = {
            children,
            firstTeacher: firstTeacher === '' ? defaultTeacher : firstTeacher,
            secondTeacher: secondTeacher === '' ? defaultTeacher : secondTeacher,
            family
          };
          this.props.onSubmit(body, this.addChildSuccess, this.addChildFail);
        }
      );
    }
  };

  addChildSuccess = () => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert('Nuevo perfil de niñ@ creado');
        this.props.onClose(false);
      }
    );
  };

  addChildFail = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  onChange = e => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      children: {
        ...this.state.children,
        [key]: value
      }
    });
  };

  onChangeEmail = (state, e) => {
    const value = e.target.value;
    this.setState({
      [state]: {
        ...this.state[state],
        email: value
      }
    });
  };

  onChangeProfile = (state, e) => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      [state]: {
        ...this.state[state],
        profile: {
          ...this.state[state].profile,
          [key]: value
        }
      }
    });
  };

  onSelectTeacher = e => {
    const value = e.target.value;
    const key = e.target.name;
    this.setState({
      [key]: value
    });
  };

  render() {
    const { status, onClose, staffs } = this.props;
    return (
      <IonModal isOpen={status} class="main-content-add-child" onDidDismiss={() => onClose(false)}>
        <button className="close" onClick={() => onClose(false)}>
          <img src="/images/close.svg" />
        </button>
        <ion-content>
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center ">
                <ion-text class="title">Crear perfil de niñ@</ion-text>
              </ion-col>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Datos de niñ@</ion-text>
              <div className="form-person">
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  onChange={e => this.onChange(e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  onChange={e => this.onChange(e)}
                />
              </div>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Tutor 1</ion-text>
              <div className="form-person">
                <select name="firstTeacher" onChange={e => this.onSelectTeacher(e)}>
                  <option>Seleccionar profesor</option>
                  {_.map(staffs, (item, key) => (
                    <option key={key} value={item.id}>
                      {item.profile.firstName}
                    </option>
                  ))}
                </select>
              </div>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Tutor 2</ion-text>
              <div className="form-person">
                <select name="secondTeacher" onChange={e => this.onSelectTeacher(e)}>
                  <option>Seleccionar profesor</option>
                  {_.map(staffs, (item, key) => (
                    <option key={key} value={item.id}>
                      {item.profile.firstName}
                    </option>
                  ))}
                </select>
              </div>
            </ion-row>
            <ion-row class="row">
              <ion-text class="title-person">Perfil del padre principal</ion-text>
              <div className="form-person">
                <input
                  type="text"
                  placeholder="Nombre"
                  name="firstName"
                  required
                  onChange={e => this.onChangeProfile('family', e)}
                />
                <input
                  type="text"
                  placeholder="Apellidos"
                  name="lastName"
                  required
                  onChange={e => this.onChangeProfile('family', e)}
                />
                <input
                  type="text"
                  placeholder="Teléfono"
                  name="telephone"
                  required
                  onChange={e => this.onChangeProfile('family', e)}
                />
                <input
                  type="text"
                  placeholder="Mail*"
                  name="email"
                  required
                  onChange={e => this.onChangeEmail('family', e)}
                />
                <div className="note">
                  *se le enviará un mail al padre principal para que complete los datos de la ficha
                  del niñ@
                </div>
              </div>
            </ion-row>
            <ion-row class="btn-wapper ion-text-center">
              {this.state.loading ? (
                <Loading />
              ) : (
                <IonButton
                  onClick={() => this.onSubmit()}
                  color="danger"
                  type="submit"
                  disabled={this.state.agree}
                >
                  Crear
                </IonButton>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {
    staffs: state.Staff.list
  };
};

const mapDispatchToProps = {
  onSubmit: classesActions.addChild,
  fetchStaff: staffActions.get,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddChild);
