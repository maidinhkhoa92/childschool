import React, { Component } from 'react';
import './index.scss';
import { HeaderBack } from 'components';
import { detail } from 'services/message';
import { connect } from 'react-redux';
import _ from 'lodash';
import { messageActions } from 'store/actions';

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      new_message: '',
      loading: false,
      firestore_id: '',
      message_id: ''
    };
  }
  componentDidMount() {
    const { match } = this.props;
    detail(match.params.firestore_id).onSnapshot(
      result => {
        this.setState({
          messages: result.data().message,
          firestore_id: match.params.firestore_id,
          message_id: match.params.message_id
        });
      },
      err => {
        console.log(`Encountered error: ${err}`);
      }
    );
  }

  renderMessage = () => {
    const { user_id } = this.props;
    return _.map(this.state.messages, (item, key) => (
      <React.Fragment key={key}>
        {item.user_id === user_id ? (
          <ion-row class="mes2">
            <div className="mes2-content">{item.message}</div>
          </ion-row>
        ) : (
          <ion-row class="mes1">
            <div className="mes1-content">{item.message}</div>
          </ion-row>
        )}
      </React.Fragment>
    ));
  };

  onChange = e => {
    this.setState({
      new_message: e.target.value
    });
  };

  onSubmit = () => {
    const { new_message, messages, firestore_id, message_id } = this.state;
    const data = {
      currentMessage: new_message,
      oldMessage: messages,
      firestore: firestore_id
    };
    this.props.update(message_id, data, this.success, this.fail);
  };
  onKeyPress = e => {
    if (e.key === 'Enter' && this.state.new_message !== '') {
      this.onSubmit();
    }
  };
  success = () => {
    this.setState({
      new_message: ''
    });
  };
  fail = () => {};
  render() {
    return (
      <React.Fragment>
        <HeaderBack title="Profesor 1" />
        <ion-content>
          <ion-grid class="content">
            {this.renderMessage()}
            <div className="message-wrapper">
              <input
                type="text"
                className="message-input"
                placeholder="Escribe un mensaje"
                value={this.state.new_message}
                onChange={e => this.onChange(e)}
                onKeyPress={e => this.onKeyPress(e)}
              />
              <button className="message-button">
                <ion-img src="/images/icon-send.svg" onClick={() => this.onSubmit()} />
              </button>
            </div>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user_id: state.Auth.infor.id
  };
};

const mapDispatchToProps = {
  update: messageActions.update
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat);
