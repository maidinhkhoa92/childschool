const logros = [
  'alegre',
  'tímid@',
  'enojad@',
  'lloros@',
  'travies@',
  'enferm@',
  'molest@',
  'cariños@',
  'atent@',
  'soñolient@',
  'callad@',
  'triste',
  'cansad@',
  'confundid@',
  'agresiv@',
  'divertid@',
  'participativ@',
  'desobediente'
];

export default logros;
