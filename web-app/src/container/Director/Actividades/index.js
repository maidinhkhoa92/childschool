import React from 'react';
import { connect } from 'react-redux';
import history from 'utils/history';
import { HeaderBack, Loading } from 'components';
import { IonPopover } from '@ionic/react';
import { mediaActions, childActions, alertActions } from 'store/actions';
import Comidas from './Comidas';
import Biberon from './Biberon';
import Bano from './Bano';
import Medicinas from './Medicinas';
import Incidentes from './Incidentes';
import Logros from './Logros';
import Animo from './Animo';
import Siesta from './Siesta';
import SelectChild from './SelectStudent';

const stateDefault = {
  photo: false,
  video: false,
  comidas: false,
  medicinas: false,
  incidentes: false,
  biberon: false,
  bano: false,
  logros: false,
  animo: false,
  siesta: false,
  selectChild: false,
  loading: false,
  uploadType: '',
  params: {
    ids: [],
    news: {
      title: '',
      note: [],
      type: '',
      content: '',
      time: '',
      group: '',
      selected: []
    }
  }
};

class Actividades extends React.Component {
  constructor(props) {
    super(props);
    this.state = stateDefault;
  }

  componentDidMount() {
    const { location } = this.props;
    this.setState({
      uploadType: location.state.type
    });
  }

  onToggle = (type, value, status = true) => {
    this.setState(() => {
      if (status) {
        return {
          [type]: value,
          params: {
            ids: [],
            news: {
              title: '',
              note: [],
              type: '',
              content: '',
              time: '',
              group: '',
              selected: []
            }
          }
        };
      }
      return {
        [type]: value
      };
    });
  };

  onUpload = (e, type) => {
    const file = e.target.files[0];
    this.setState(
      {
        type,
        loading: true
      },
      () => {
        if (type === 'Photo') {
          this.props.uploadImage(file, this.successUpload, this.failUpload);
        } else {
          this.props.uploadVideo(file, this.successUpload, this.failUpload);
        }
      }
    );
  };

  successUpload = url => {
    this.setState(
      {
        loading: false
      },
      () => {
        const data = {
          type: this.state.type,
          url: url,
          group: this.state.uploadType
        };
        history.push({ pathname: '/staff/actividades/media', state: data });
      }
    );
  };

  failUpload = () => {
    this.setState({
      loading: false
    });
  };

  // handle activities
  setInformation = (key, value) => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        [key]: value
      }
    }));
  };

  setNews = (key, value) => {
    this.setState(prev => ({
      params: {
        ...prev.params,
        news: {
          ...prev.params.news,
          [key]: value
        }
      }
    }));
  };

  // submit news
  onSubmit = body => {
    if (this.state.params.ids.length === 0) {
      this.props.alert('Selecciona al menos un estudiante');
    } else {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props.submit(body, this.success, this.fail);
        }
      );
    }
  };

  success = () => {
    this.setState(stateDefault);
    this.props.alert('Actividad publicada');
  };
  fail = message => {
    this.setState({
      loading: false
    }, () => {
      this.props.alert(message);
    });
    
  };

  render() {
    const {
      photo,
      video,
      comidas,
      biberon,
      bano,
      medicinas,
      incidentes,
      selectChild,
      logros,
      animo,
      siesta,
      params,
      loading
    } = this.state;

    return (
      <React.Fragment>
        <HeaderBack title="" />
        <ion-content>
          <ion-grid class="main-content-staff-activities">
            <ion-row class="ion-text-center">
              <ion-text class="title">Actividades</ion-text>
            </ion-row>
            <div className="all">
              <ion-row class="box-wapper margin-left">
                <div className="box" onClick={() => this.onToggle('photo', true)}>
                  <ion-img src="/images/icon/camara-de-fotos.svg" alt="logo" />
                  <ion-text>Foto</ion-text>
                </div>
                <div className="box" onClick={() => this.onToggle('video', true)}>
                  <ion-img src="/images/icon/camara.svg" alt="logo" />
                  <ion-text>Video</ion-text>
                </div>
              </ion-row>
              <ion-row class="box-wapper">
                <div className="box" onClick={() => this.onToggle('comidas', true)}>
                  <ion-img src="/images/icon/caja-de-almuerzo.svg" alt="logo" />
                  <ion-text class="text-box">Comidas</ion-text>
                </div>
                <div className="box" onClick={() => this.onToggle('biberon', true)}>
                  <ion-img src="/images/icon/la-alimentacion-con-biberon.svg" alt="logo" />
                  <ion-text class="text-box">Biberón</ion-text>
                </div>
                <div className="box" onClick={() => this.onToggle('bano', true)}>
                  <ion-img src="/images/icon/orinal.svg" alt="logo" />
                  <ion-text class="text-box">Baño</ion-text>
                </div>
              </ion-row>
              <ion-row class="box-wapper">
                <div className="box" onClick={() => this.onToggle('logros', true)}>
                  <ion-img src="/images/icon/trofeo (1).svg" alt="logo" />
                  <ion-text class="text-box">Logros</ion-text>
                </div>
                <div className="box" onClick={() => this.onToggle('animo', true)}>
                  <ion-img src="/images/icon/emoji (1).svg" alt="logo" />
                  <ion-text class="text-box">Ánimo</ion-text>
                </div>
                <div className="box" onClick={() => this.onToggle('siesta', true)}>
                  <ion-img src="/images/icon/suenos.svg" alt="logo" />
                  <ion-text class="text-box">Siesta</ion-text>
                </div>
              </ion-row>
              <ion-row class="box-wapper margin-left">
                <div className="box" onClick={() => this.onToggle('medicinas', true)}>
                  <ion-img src="/images/icon/botiquin-de-primeros-auxilios (1).svg" alt="logo" />
                  <ion-text class="text-box">Medicinas</ion-text>
                </div>
                <div className="box" onClick={() => this.onToggle('incidentes', true)}>
                  <ion-img src="/images/icon/curacion.svg" alt="logo" />
                  <ion-text class="text-box">Incidentes</ion-text>
                </div>
              </ion-row>
            </div>
          </ion-grid>
          <IonPopover isOpen={photo} onDidDismiss={() => this.onToggle('photo', false)}>
            <div className="media-popup">
              <div className="title">
                <ion-text>Nueva foto</ion-text>
              </div>
              {loading ? (
                <Loading />
              ) : (
                <React.Fragment>
                  <div className="photo">
                    <input type="file" onChange={e => this.onUpload(e, 'Photo')} accept="image/*" />
                    <div className="photo-activities">
                      <ion-img src="/images/icon/camera.svg" alt="logo" />
                    </div>
                    <div className="text">
                      <ion-text>Tomar foto</ion-text>
                    </div>
                  </div>
                  <div className="photo">
                    <input type="file" onChange={e => this.onUpload(e, 'Photo')} accept="image/*" />
                    <div className="photo-activities">
                      <ion-img src="/images/icon/imagen.svg" alt="logo" />
                    </div>
                    <div className="text">
                      <ion-text>Escoger imagen</ion-text>
                    </div>
                  </div>
                </React.Fragment>
              )}
            </div>
          </IonPopover>
          <IonPopover isOpen={video} onDidDismiss={() => this.onToggle('video', false)}>
            <div className="media-popup">
              <div className="title">
                <ion-text>Nuevo video</ion-text>
              </div>
              {loading ? (
                <Loading />
              ) : (
                <React.Fragment>
                  <div className="photo">
                    <input type="file" onChange={e => this.onUpload(e, 'Video')} accept="video/*" />
                    <div className="photo-activities">
                      <ion-img src="/images/icon/camera.svg" alt="logo" />
                    </div>
                    <div className="text">
                      <ion-text>Hacer vídeo</ion-text>
                    </div>
                  </div>
                  <div className="photo">
                    <input type="file" onChange={e => this.onUpload(e, 'Video')} accept="video/*" />
                    <div className="photo-activities">
                      <ion-img src="/images/icon/imagen.svg" alt="logo" />
                    </div>
                    <div className="text">
                      <ion-text>Cargar vídeo</ion-text>
                    </div>
                  </div>
                </React.Fragment>
              )}
            </div>
          </IonPopover>
          <Comidas
            onClose={this.onToggle}
            status={comidas}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            loading={loading}
          />
          <Biberon
            onClose={this.onToggle}
            status={biberon}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            loading={loading}
          />
          <Bano
            onClose={this.onToggle}
            status={bano}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            loading={loading}
          />
          <Medicinas
            onClose={this.onToggle}
            status={medicinas}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            loading={loading}
          />
          <Incidentes
            onClose={this.onToggle}
            status={incidentes}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            loading={loading}
          />
          <Logros
            onClose={this.onToggle}
            status={logros}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            uploadImage={this.props.uploadImage}
            loading={loading}
          />
          <Animo
            onClose={this.onToggle}
            status={animo}
            setInformation={this.setInformation}
            setNews={this.setNews}
            params={params}
            onSubmit={this.onSubmit}
            uploadImage={this.props.uploadImage}
            loading={loading}
          />
          <Siesta onClose={this.onToggle} status={siesta} />
          <SelectChild
            status={selectChild}
            onClose={this.onToggle}
            setChilds={this.setInformation}
            parentChilds={params.ids}
          />
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    child: state.Child.detail
  };
};

const mapDispatchToProps = {
  uploadImage: mediaActions.uploadImage,
  uploadVideo: mediaActions.uploadVideo,
  submit: childActions.postNews,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Actividades);
