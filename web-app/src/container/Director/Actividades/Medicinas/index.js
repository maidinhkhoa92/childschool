import React from 'react';
import _ from 'lodash';
import { HeaderActivities, Loading } from 'components';
import { IonModal, IonDatetime, IonInput, IonCol } from '@ionic/react';

class Medicinas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {
        type: 'Medicines'
      }
    };
  }

  onClose = () => {
    this.props.onClose('medicinas', false);
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setNews(key, value);
  };

  renderChild = () => {
    const { params } = this.props;
    return _.map(params.ids, (child, key) => (
      <React.Fragment key={key}>
        <span className="icon-sofia">
          {this.showAvatar(child.profile.photo)}
          <ion-text>{child.profile.firstName}</ion-text>
        </span>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return (
        <img
          src={url}
          alt="avatar"
          className="wapper-avatar"
        />
      );
    }
    return (
      <div className="wapper-img">
        <ion-img
          src="/images/icon/bebe.svg"
          allt="avatar"
        />
      </div>
    );
  };

  onSubmit = () => {
    const { params } = this.props;
    const body = {
      ids: _.map(params.ids, item => item.id),
      news: {
        ...params.news,
        type: 'Medicines'
      }
    };
    this.props.onSubmit(body);
  };

  render() {
    const { onClose, status, loading } = this.props;
    return (
      <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
        <HeaderActivities
          url="/images/icon/botiquin-de-primeros-auxilios (1).svg"
          onClose={this.onClose}
        />
        <ion-content class="medicines-container">
          <ion-grid class="main-content">
            <div className="all">
              <ion-row class="user-select">{this.renderChild()}</ion-row>
              <ion-row class="select-user">
                <div className="wapper-img">
                  <ion-img
                    src="/images/icon/intercambiabilidad.svg"
                    alt="logo"
                    onClick={() => onClose('selectChild', true, false)}
                  />
                </div>
              </ion-row>
              <ion-row>
                <form className="form-person">
                  <IonCol sizeXs="10">
                    <IonInput
                      placeholder="Nombre de la medicina"
                      name="title"
                      onIonChange={e => this.onChange(e)}
                    />
                  </IonCol>
                  <ion-row class="time">
                    <ion-text class="time-text">Hora</ion-text>
                    <div className="time-wapper">
                      <IonDatetime
                        displayFormat="HH:mm"
                        name="time"
                        onIonChange={e => this.onChange(e)}
                      />
                    </div>
                  </ion-row>
                  <IonCol sizeXs="10" class="textarea">
                    <textarea
                      placeholder="Notas"
                      auto-grow="true"
                      name="content"
                      onChange={e => this.onChange(e)}
                    />
                  </IonCol>
                </form>
              </ion-row>
            </div>
            <ion-row class="btn-submit medicine-activity">
              {loading ? (
                <Loading />
              ) : (
                <ion-button onClick={() => this.onSubmit()}>Añadir</ion-button>
              )}
            </ion-row>
          </ion-grid>
        </ion-content>
      </IonModal>
    );
  }
}

export default Medicinas;
