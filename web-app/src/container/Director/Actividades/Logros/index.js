import React from 'react';
import _ from 'lodash';
import { HeaderActivities, Loading } from 'components';
import { IonModal, IonDatetime, IonTextarea, IonPopover, IonCol } from '@ionic/react';
import logros from './logros';
import { connect } from 'react-redux';
import { alertActions } from 'store/actions';

class Logros extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      selected: [],
      photo: false,
      confirmPhoto: false,
      photoUrl: ''
    };
  }
  onClose = () => {
    this.props.onClose('logros', false);
  };

  onChange = e => {
    const key = e.target.name;
    const value = e.target.value;
    this.props.setNews(key, value);
  };

  renderChild = () => {
    const { params } = this.props;
    return _.map(params.ids, (child, key) => (
      <React.Fragment key={key}>
        <span className="icon-sofia">
          {this.showAvatar(child.profile.photo)}
          <ion-text>{child.profile.firstName}</ion-text>
        </span>
      </React.Fragment>
    ));
  };

  showAvatar = url => {
    if (!_.isUndefined(url)) {
      return (
        <img
          src={url}
          alt="avatar"
          className="wapper-avatar"
        />
      );
    }
    return (
      <div className="wapper-img">
        <ion-img
          src="/images/icon/bebe.svg"
          allt="avatar"
        />
      </div>
    );
  };

  // logros handle
  renderLogros = () => {
    return _.map(this.props.params.news.note, (item, key) => (
      <span key={key}>
        {item}
        <img src="/images/close.svg" onClick={() => this.removeLogros(item)} />
      </span>
    ));
  };

  onToggleLogros = value => {
    this.setState({
      show: value
    });
  };

  onSelecteLogros = item => {
    this.setState(prev => {
      if (_.includes(prev.selected, item)) {
        return {
          selected: _.filter(prev.selected, logros => item !== logros)
        };
      }
      return {
        selected: [...prev.selected, item]
      };
    });
  };

  submitLogros = () => {
    this.setState(
      {
        selected: _.union(this.props.params.news.note, this.state.selected),
        show: false
      },
      () => {
        this.props.setNews('note', this.state.selected);
      }
    );
  };

  removeLogros = item => {
    this.setState(
      {
        selected: _.filter(this.state.selected, element => element !== item)
      },
      () => {
        this.props.setNews('note', this.state.selected);
      }
    );
  };

  // upload image handle
  toggleImage = (key, value) => {
    this.setState({
      [key]: value
    });
  };

  onUpload = (e, type) => {
    const file = e.target.files[0];
    this.setState(
      {
        type
      },
      () => {
        this.props.uploadImage(file, this.successUpload, this.failUpload);
      }
    );
  };

  successUpload = url => {
    this.setState({
      confirmPhoto: true,
      photoUrl: url,
      photo: false
    });
  };

  failUpload = () => {
    this.setState({
      photo: false
    });
  };

  confirmImage = () => {
    this.setState(
      {
        confirmPhoto: false,
        photo: false
      },
      () => {
        this.props.setNews('group', this.state.photoUrl);
      }
    );
  };

  onSubmit = () => {
    const { params } = this.props;
    const body = {
      ids: _.map(params.ids, item => item.id),
      news: {
        ...params.news,
        title: 'LOGROS',
        type: 'Achievements'
      }
    };
    if(body.news.note.length === 0) {
      this.props.alert('Selecciona el tipo de aprendizaje');
    } else {
      this.props.onSubmit(body);
    }
  };
  render() {
    const { status, onClose, params, loading } = this.props;
    const { show, selected, photo, confirmPhoto, photoUrl } = this.state;
    return (
      <React.Fragment>
        <IonModal isOpen={status} onDidDismiss={() => this.onClose()}>
          <HeaderActivities url="/images/icon/trofeo (1).svg" onClose={this.onClose} />
          <ion-content class="logros-container">
            <ion-grid class="main-content">
              <div className="all">
                <ion-row class="user-select">{this.renderChild()}</ion-row>
                <ion-row class="select-user">
                  <div className="wapper-img">
                    <ion-img
                      src="/images/icon/intercambiabilidad.svg"
                      alt="logo"
                      onClick={() => onClose('selectChild', true, false)}
                    />
                  </div>
                </ion-row>
                <ion-row>
                  <form className="form-person">
                    <ion-row class="time">
                      <ion-text class="time-text">Hora</ion-text>
                      <div className="time-wapper">
                        <IonDatetime
                          displayFormat="HH:mm"
                          name="time"
                          onIonChange={e => this.onChange(e)}
                        />
                      </div>
                    </ion-row>
                    <IonCol sizeXs="10">
                      <div className="tipo" onClick={() => this.onToggleLogros(true)}>
                        Tipo de Aprendizaje
                      </div>
                    </IonCol>
                    <IonCol sizeXs="10" class="select-logros">
                      {this.renderLogros()}
                    </IonCol>
                    <IonCol sizeXs="10" class="textarea">
                      <IonTextarea
                        placeholder="Descripción"
                        auto-grow={true}
                        name="content"
                        onIonChange={e => this.onChange(e)}
                      />
                    </IonCol>
                  </form>
                </ion-row>
                {params.news.group === '' ? (
                  <ion-row class="btn-submit">
                    <ion-img
                      src="/images/icon/camera.svg"
                      alt="logo"
                      onClick={() => this.toggleImage('photo', true)}
                    />
                  </ion-row>
                ) : (
                  <ion-row>
                    <ion-col>
                      <img src={params.news.group} className="preview-image" />
                    </ion-col>
                  </ion-row>
                )}
              </div>
              <ion-row class="btn-submit">
                {loading ? (
                  <Loading />
                ) : (
                  <ion-button onClick={() => this.onSubmit()}>Añadir</ion-button>
                )}
              </ion-row>
            </ion-grid>
          </ion-content>
          <IonPopover
            class="food-popover"
            isOpen={show}
            onDidDismiss={() => this.onToggleLogros(false)}
          >
            <div className="food-wrapper">
              <div className="food-container">
                {_.map(logros, (item, key) => (
                  <div className="food-item" key={key} onClick={() => this.onSelecteLogros(item)}>
                    <span
                      className={
                        _.includes(params.news.note, item) || _.includes(selected, item)
                          ? 'active'
                          : ''
                      }
                    >
                      {item}
                    </span>
                  </div>
                ))}
              </div>
              <button className="food-button" onClick={() => this.submitLogros()}>
                Aplicar
              </button>
            </div>
          </IonPopover>
          <IonPopover isOpen={photo} onDidDismiss={() => this.toggleImage('photo', false)}>
            <div className="media-popup">
              <div className="title">
                <ion-text>Nueva foto</ion-text>
              </div>
              <div className="photo">
                <input type="file" onChange={e => this.onUpload(e)} accept="image/*" />
                <div className="photo-activities">
                  <ion-img src="/images/icon/camera.svg" alt="logo" />
                </div>
                <div className="text">
                  <ion-text>Hacer Foto</ion-text>
                </div>
              </div>
              <div className="photo">
                <input type="file" onChange={e => this.onUpload(e)} accept="image/*" />
                <div className="photo-activities">
                  <ion-img src="/images/icon/imagen.svg" alt="logo" />
                </div>
                <div className="text">
                  <ion-text>Cargar Foto</ion-text>
                </div>
              </div>
            </div>
          </IonPopover>
        </IonModal>
        <IonModal
          isOpen={confirmPhoto}
          onDidDismiss={() => this.toggleImage('confirmPhoto', false)}
        >
          <ion-content class="confirm-container">
            <div className="confirm-content">
              <div className="overflay" />
              <img src={photoUrl} />
            </div>
            <div className="confirm-wrapper">
              <button
                className="message-button"
                onClick={() => this.toggleImage('confirmPhoto', false)}
              >
                <ion-img src="/images/icon/cancel.svg" />
              </button>
              <button className="message-button" onClick={() => this.confirmImage()}>
                <ion-img src="/images/icon/tick.svg" />
              </button>
            </div>
          </ion-content>
        </IonModal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Logros);