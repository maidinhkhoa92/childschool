import React from 'react';
import _ from 'lodash';
import history from 'utils/history';
import { connect } from 'react-redux';
import { HeaderBack } from 'components';
import AddMessage from './Add';
import truncate from 'utils/truncate';
import { messageActions } from 'store/actions';
import { FormatPastTime } from 'utils/moment';
import './index.scss';

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modal: false,
      list: []
    };
  }

  componentDidMount() {
    this.props.fetch({}, this.success, this.fail);
  }

  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onToggle = value => {
    this.setState({
      modal: value
    });
  };

  onSubmit = () => {};

  renderMessage = () => {
    return _.map(this.props.list, (item, key) => (
      <React.Fragment key={key}>
        <ion-row class="wapper-item">
          <ion-col class="message-wrapper">
            <div
              className="tab-icon"
              style={
                item.classes.length > 0
                  ? { background: item.classes[0].color }
                  : { background: 'green' }
              }
            >
              <p className="tab-text">
                {item.classes.length > 0 ? item.classes[0].name : 'classes'}
              </p>
            </div>
            <div className="tab-content">
              <h3 className="title">
                <span>
                  {_.map(item.classes, (class_child, i) => (
                    <React.Fragment key={i}>{class_child.name}, </React.Fragment>
                  ))}
                </span>
                <span>{FormatPastTime(item.updated_at)}</span>
              </h3>
              <p>{truncate(item.message, 100)}</p>
            </div>
          </ion-col>
        </ion-row>
      </React.Fragment>
    ));
  };

  render() {
    const { modal } = this.state;
    return (
      <React.Fragment>
        <HeaderBack title="Mensajes" />
        <ion-content>
          <ion-grid class="main-content-message">{this.renderMessage()}</ion-grid>
          <AddMessage status={modal} onClose={this.onToggle} />
        </ion-content>
        <ion-footer class="add-new-message">
          <ion-grid>
            <ion-row>
              <ion-col class="ion-text-center">
                <ion-img
                  src="/images/message.png"
                  allt="avatar"
                  onClick={() => this.onToggle(true)}
                />
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    list: state.Message.list,
    userId: state.Auth.infor.id,
    type: state.Auth.infor.typeOfUser
  };
};

const mapDispatchToProps = {
  fetch: messageActions.get
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Message);
