import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { IonModal, IonButton, IonCheckbox } from '@ionic/react';
import { Loading } from 'components';
import { messageActions, classesActions, alertActions } from 'store/actions';

class AddMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      params: {
        message: '',
        classes: [],
        to_user: [],
        type: 'class'
      },
      loading: false,
      error: {
        status: false,
        msg: ''
      },
      step: 1
    };
  }

  componentDidMount() {
    this.setState(
      {
        loading: true
      },
      () => {
        this.props.fetchClasses(this.success, this.fail);
      }
    );
  }
  // step 1
  success = () => {
    this.setState({
      loading: false
    });
  };

  fail = () => {
    this.setState({
      loading: false
    });
  };

  onSelected = item => {
    if (_.findIndex(this.state.params.classes, item) === -1) {
      this.setState(prev => ({
        params: {
          ...prev.params,
          classes: [...prev.params.classes, item]
        }
      }));
    } else {
      this.setState(prev => ({
        params: {
          ...prev.params,
          classes: _.filter(prev.params.classes, classes => classes.id !== item.id)
        }
      }));
    }
  };

  classStyle = item => {
    const style = { background: item.color };
    if (_.findIndex(this.state.params.classes, item) !== -1) {
      return { ...style, border: '4px solid #4A452A' };
    }
    return style;
  };

  renderClass = () => {
    return _.map(this.props.classes, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="4" class="column-add-new-message">
          <div
            className="tab-icon"
            style={this.classStyle(item)}
            onClick={() => this.onSelected(item)}
          >
            <p className="tab-text">{item.name}</p>
          </div>
        </ion-col>
      </React.Fragment>
    ));
  };

  // step 2
  renderClassSeleted = () => {
    const { params } = this.state;
    const maxLength = this.props.classes.length;

    if (params.classes.length === maxLength) {
      return (
        <ion-col size="12">
          <div className="allClass">
            <img src="/images/allClass.svg" />
            Todos las clases
          </div>
        </ion-col>
      );
    }
    return _.map(this.state.params.classes, (item, index) => (
      <React.Fragment key={index}>
        <ion-col size="4">
          <div className="tab-icon" style={{ background: item.color }}>
            <p className="tab-text">{item.name}</p>
          </div>
        </ion-col>
      </React.Fragment>
    ));
  };

  onChange = e => {
    const value = e.target.value;
    this.setState(prev => ({
      params: {
        ...prev.params,
        message: value
      }
    }));
  };

  onSubmit = () => {
    const { step } = this.state;
    if (step === 1) {
      this.setState({
        step: 2
      });
    } else {
      this.setState(
        {
          step: 1,
          loading: true
        },
        () => {
          this.props.onSubmit(this.state.params, this.successSendMessage, this.failSendMessage);
        }
      );
    }
  };
  successSendMessage = () => {
    this.props.onClose(false);
    this.props.alert('nuevo mensaje colectivo');
  };

  failSendMessage = message => {
    this.setState(
      {
        loading: false
      },
      () => {
        this.props.alert(message);
      }
    );
  };

  selectAll = e => {
    const { classes } = this.props;
    this.setState(prev => {
      if (!e.target.checked) {
        return {
          params: {
            ...prev.params,
            classes: []
          }
        };
      }
      return {
        params: {
          ...prev.params,
          classes: classes
        }
      };
    });
  };

  setStep = value => {
    this.setState({
      step: value
    });
  };

  render() {
    const { status, onClose } = this.props;
    const { step, loading, params } = this.state;
    return (
      <IonModal isOpen={status} class="main-content-add-child" onDidDismiss={() => onClose(false)}>
        {step === 1 && (
          <React.Fragment>
            <button className="close" onClick={() => onClose(false)}>
              <img src="/images/close.svg" />
            </button>
            <ion-content class="director-add-new-message">
              <ion-grid>
                <ion-row>
                  <ion-col class="ion-text-center ">
                    <ion-text class="title">Selecciona clase/s</ion-text>
                  </ion-col>
                </ion-row>
                <ion-row class="item-check">
                  <IonCheckbox color="danger" onIonChange={e => this.selectAll(e)} />
                  <ion-label class="text-selec">Seleccionar todos</ion-label>
                </ion-row>
                <ion-row class="row">{loading ? <Loading /> : this.renderClass()}</ion-row>
              </ion-grid>
            </ion-content>
            <ion-footer class="footer-add-message">
              <ion-row class="btn-wapper ion-text-center">
                {loading ? (
                  <Loading />
                ) : (
                  <IonButton
                    onClick={() => this.onSubmit()}
                    color="danger"
                    type="submit"
                    disabled={params.classes.length > 0 ? false : true}
                  >
                    Siguiente
                  </IonButton>
                )}
              </ion-row>
            </ion-footer>
          </React.Fragment>
        )}
        {step === 2 && (
          <React.Fragment>
            <button className="back" onClick={() => this.setStep(1)}>
              <img src="/images/back.svg" />
            </button>
            <ion-content class="director-classes">
              <ion-grid>
                <ion-row>
                  <ion-col class="ion-text-center ">
                    <ion-text class="title-message">Nuevo mensaje</ion-text>
                  </ion-col>
                </ion-row>
                <ion-row class="row">{this.renderClassSeleted()}</ion-row>
                <ion-row>
                  <ion-col>
                    <textarea
                      placeholder="Mensaje"
                      className="message-area-input"
                      onChange={e => this.onChange(e)}
                    />
                  </ion-col>
                </ion-row>
              </ion-grid>
            </ion-content>
            <ion-footer class="footer-add-message">
              <ion-row class="btn-wapper ion-text-center">
                {loading ? (
                  <Loading />
                ) : (
                  <IonButton onClick={() => this.onSubmit()} color="danger" type="submit">
                    Enviar
                  </IonButton>
                )}
              </ion-row>
            </ion-footer>
          </React.Fragment>
        )}
      </IonModal>
    );
  }
}

const mapStateToProps = state => {
  return {
    classes: state.Classes.list
  };
};

const mapDispatchToProps = {
  fetchClasses: classesActions.get,
  onSubmit: messageActions.create,
  alert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddMessage);
