import React from 'react';
import { Header, Loading } from 'components';
import { connect } from 'react-redux';
import { authActions, alertActions } from 'store/actions';
import history from 'utils/history';
import { Formik } from 'formik';
import './index.scss';
import { initialValues, validationSchema } from './validate';
import queryString from 'query-string';

class Acceso extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSubmit = async (values, actions) => {
    const { onSubmit, openAlert, location } = this.props;
    const parsed = queryString.parse(location.search);
    const payload = {
      params: { password: values.password },
      token: parsed.token
    };

    try {
      actions.setSubmitting(true);

      await onSubmit(payload);
      openAlert('Tu contraseña ha sido cambiada correctamente');
    } catch (e) {
      actions.setErrors({
        email: e.message || e
      });
    } finally {
      actions.setSubmitting(false);
    }
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <ion-content class="main-page-acceso">
          <ion-grid class="otp-page">
            <Formik
              initialValues={initialValues}
              validateOnBlur={false}
              validateOnChange={false}
              validationSchema={validationSchema}
              onSubmit={this.onSubmit}
            >
              {props => (
                <form onSubmit={props.handleSubmit}>
                  {props.errors.passwordConfirmation && (
                    <ion-row class="wapper-mess">
                      <ion-col class="ion-text-center">
                        <p className="text-err">{props.errors.passwordConfirmation}</p>
                      </ion-col>
                    </ion-row>
                  )}
                  {props.errors.password && (
                    <ion-row class="wapper-mess">
                      <ion-col class="ion-text-center">
                        <p className="text-err">{props.errors.password}</p>
                      </ion-col>
                    </ion-row>
                  )}
                  <ion-row>
                    <div className="form-acceso">
                      <p className="title-form ion-text-center">Restablecer contraseña</p>
                      <input
                        type="password"
                        placeholder="Define una contraseña"
                        name="password"
                        required
                        onChange={props.handleChange}
                        className="text-email-acceso"
                      />
                      <input
                        type="password"
                        placeholder="Repite contraseña"
                        name="passwordConfirmation"
                        required
                        onChange={props.handleChange}
                        className="text-email-acceso"
                      />
                      <ion-row class="btn-wapper">
                        {props.isSubmitting ? (
                          <Loading />
                        ) : (
                          <button type="submit" color="danger" className="form-accede-button">
                            Enviar
                          </button>
                        )}
                      </ion-row>

                      <ion-text class="text-footer ion-text-center">
                        <p onClick={() => history.push('acceso')}>Ve a Acceso</p>
                      </ion-text>
                    </div>
                  </ion-row>
                </form>
              )}
            </Formik>
          </ion-grid>
        </ion-content>
      </React.Fragment>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = {
  onSubmit: authActions.resetPassword,
  openAlert: alertActions.open
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Acceso);
