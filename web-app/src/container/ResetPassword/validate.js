import * as Yup from 'yup';

export const initialValues = {
  password: ''
};

export const validationSchema = Yup.object({
  password: Yup.string().required('Password is required'),
  passwordConfirmation: Yup.string().oneOf([Yup.ref('password'), null], 'La contraseña debe coincidir')
});
