import React from 'react';
import history from 'utils/history';
import { IonButton } from '@ionic/react';
import { Link } from 'react-router-dom';
import './index.scss';

const Welcome = () => {
  return (
    <ion-content>
      <ion-grid class="main-content-welcome">
        <ion-row>
          <ion-col class="text-logo">
            <ion-text>Gracias!</ion-text>
            <ion-img
              onClick={() => history.push('/')}
              src="/images/icon/alimentador.svg"
              alt="logo"
            />
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col class="ion-text-center">
            <ion-text>Te contactaremos</ion-text>
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col class="ion-text-center">
            <ion-text>lo antes posible</ion-text>
          </ion-col>
        </ion-row>
        <ion-row>
          <ion-col>
            <Link to="/">
              <IonButton color="danger">Volver a la página principal</IonButton>
            </Link>
          </ion-col>
        </ion-row>
      </ion-grid>
    </ion-content>
  );
};

export default Welcome;
