import actions from './actions';
import _ from 'lodash';

const initialState = {
  list: [],
  centers: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_PAYMENT_SUCCESS:
      return { ...state, list: action.payload };
    case actions.CREATE_PAYMENT_SUCCESS:
      return { ...state, list: [action.payload, ...state.list] };
    case actions.EDIT_PAYMENT_SUCCESS:
      return { ...state, list: _.map(state.list, item => item.id === action.payload.id ? action.payload : item) };
    case actions.GET_PAYMENT_CENTER_SUCCESS:
      return { ...state, centers: action.payload };
    default:
      return state;
  }
};
