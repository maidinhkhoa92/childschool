import actions from './actions';
import { all, fork, takeLatest, put, call, select } from 'redux-saga/effects';
import { List, Create, Update } from 'services/payment';
import { List as ListCenter } from 'services/user';

export function* getPaymentCentersSaga() {
  yield takeLatest(actions.GET_PAYMENT_CENTER_REQUEST, function* ({ params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(ListCenter, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.GET_PAYMENT_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.PAYMENT_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* getPaymentSaga() {
  yield takeLatest(actions.GET_PAYMENT_REQUEST, function* ({ params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(List, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.GET_PAYMENT_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.PAYMENT_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* createPaymentSaga() {
  yield takeLatest(actions.CREATE_PAYMENT_REQUEST, function* ({ params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Create, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.CREATE_PAYMENT_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.PAYMENT_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* updatePaymentSaga() {
  yield takeLatest(actions.EDIT_PAYMENT_REQUEST, function* ({ id, params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Update, id, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.EDIT_PAYMENT_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.PAYMENT_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getPaymentCentersSaga), fork(getPaymentSaga), fork(createPaymentSaga), fork(updatePaymentSaga)]);
}
