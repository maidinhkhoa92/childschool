import actions from './actions';
import { all, fork, takeLatest, put, call, select, takeEvery } from 'redux-saga/effects';
import { List, Detail, Create, Setting, Count, Remove, Update, UpdateStatus, Resend } from 'services/user';

export function* getCenterSaga() {
  yield takeLatest(actions.GET_CENTER_REQUEST, function* ({ params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(List, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.GET_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* detailCenterSaga() {
  yield takeLatest(actions.DETAIL_CENTER_REQUEST, function* ({ id, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Detail, id, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.DETAIL_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* createCenterSaga() {
  yield takeLatest(actions.CREATE_CENTER_REQUEST, function* ({ params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Create, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.CREATE_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* updateSettingSaga() {
  yield takeLatest(actions.UPDATE_SETTING_CENTER_REQUEST, function* ({ id, params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Setting, id, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.UPDATE_SETTING_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* countChildSaga() {
  yield takeLatest(actions.COUNT_CHILD_REQUEST, function* ({ id, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Count, id, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.COUNT_CHILD_SUCCESS,
        payload: res.data.count,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* updateStatusSaga() {
  yield takeEvery(actions.STATUS_CENTER_REQUEST, function* ({ id, params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(UpdateStatus, id, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.STATUS_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* removeCenterSaga() {
  yield takeEvery(actions.REMOVE_CENTER_REQUEST, function* ({ id, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Remove, id, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.REMOVE_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* updateCenterSaga() {
  yield takeEvery(actions.UPDATE_CENTER_REQUEST, function* ({ id, params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Update, id, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.UPDATE_CENTER_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export function* resendEmailSaga() {
  yield takeEvery(actions.RESEND_EMAIL_REQUEST, function* ({ params, meta }) {
    try {
      const token = yield select(state => state.Admin.token);
      // call request to API
      const res = yield call(Resend, params, token);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({
        type: actions.RESEND_EMAIL_SUCCESS,
        payload: res.data,
        meta
      });
    } catch (e) {
      yield put({ type: actions.USER_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export default function* rootSaga() {
  yield all([
    fork(getCenterSaga),
    fork(detailCenterSaga),
    fork(createCenterSaga),
    fork(updateSettingSaga),
    fork(countChildSaga),
    fork(updateStatusSaga),
    fork(removeCenterSaga),
    fork(updateCenterSaga),
    fork(resendEmailSaga)
  ]);
}
