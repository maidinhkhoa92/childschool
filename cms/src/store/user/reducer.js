import actions from './actions';
import User from 'models/user';
import _ from 'lodash';

const initialState = {
  list: [],
  detail: User
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_CENTER_SUCCESS:
      return { ...state, list: action.payload };
    case actions.DETAIL_CENTER_SUCCESS:
      return { ...state, detail: action.payload };
    case actions.CREATE_CENTER_SUCCESS:
      return { ...state, list: [...state.list, action.payload] };
    case actions.UPDATE_SETTING_CENTER_SUCCESS:
      return { ...state, detail: action.payload };
    case actions.STATUS_CENTER_SUCCESS:
      return {
        ...state,
        detail: action.payload
      };
    case actions.REMOVE_CENTER_SUCCESS:
      return {
        ...state,
        list: _.filter(state.list, item => item.id !== action.payload)
      };
    default:
      return state;
  }
};
