import { all } from 'redux-saga/effects';
import Adminsaga from './admin/saga';
import Usersaga from './user/saga';
import PaymentSaga from './payment/saga';

export default function* rootSaga() {
  yield all([Adminsaga(), Usersaga(), PaymentSaga()]);
}
