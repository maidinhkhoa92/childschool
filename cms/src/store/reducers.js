import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import Admin from './admin/reducer';
import User from './user/reducer';
import Alert from './alert/reducer';
import Payment from './payment/reducer';

const reducers = combineReducers({
  User,
  Admin,
  Alert,
  Payment
});

const persistConfig = {
  key: 'root',
  storage
};
const persistedReducer = persistReducer(persistConfig, reducers);
export default persistedReducer;
