import actions from './actions';

const initialState = {
  token: null
};

const Agency = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN_SUCCESS:
      return { ...state, token: action.payload.token };
    case actions.LOGIN_OUT:
      return initialState;
    default:
      return state;
  }
};

export default Agency;
