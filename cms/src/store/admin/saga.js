import actions from './actions';
import { all, fork, takeLatest, put, call } from 'redux-saga/effects';
import { Login } from 'services/admin';

export function* LoginSaga() {
  yield takeLatest(actions.LOGIN_REQUEST, function*({ params, meta }) {
    try {
      // call request to API
      const res = yield call(Login, params);

      if (res.status !== 200) {
        throw new Error(res.data.message);
      }

      yield put({ type: actions.LOGIN_SUCCESS, payload: res.data, meta });
    } catch (e) {
      yield put({ type: actions.ADMIN_FAILURE, payload: e.message || e, error: true, meta });
    }
  });
}

export default function* rootSaga() {
  yield all([fork(LoginSaga)]);
}
