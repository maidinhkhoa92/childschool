import actions from './actions';

const initialState = {
  status: false,
  message: '',
  variant: 'danger'
};

const Alert = (state = initialState, action) => {
  switch (action.type) {
    case actions.OPEN_SUCCESS_ALERT:
      return { ...state, message: action.alert, status: true, variant: 'success' };
    case actions.OPEN_FAIL_ALERT:
      return { ...state, message: action.alert, status: true, variant: 'danger' };
    case actions.CLOSE_ALERT:
      return initialState;
    default:
      return state;
  }
};

export default Alert;
