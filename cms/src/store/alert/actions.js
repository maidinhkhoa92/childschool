const actions = {
  OPEN_SUCCESS_ALERT: 'OPEN_SUCCESS_ALERT',
  OPEN_FAIL_ALERT: 'OPEN_FAIL_ALERT',
  CLOSE_ALERT: 'CLOSE_ALERT'
};
export default actions;
