import request from 'utils/request';

export const List = (params, token) => {
  return request({
    url: '/user',
    method: 'get',
    params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Detail = (id, token) => {
  return request({
    url: '/user/' + id,
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Create = (data, token) => {
  return request({
    url: '/user/',
    method: 'post',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Update = (id, data, token) => {
  return request({
    url: '/user/' + id,
    method: 'put',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Setting = (id, data, token) => {
  return request({
    url: '/setting/' + id,
    method: 'put',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const UpdateStatus = (id, data, token) => {
  return request({
    url: '/user/' + id,
    method: 'patch',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Remove = (id, token) => {
  return request({
    url: '/user/' + id,
    method: 'delete',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Count = (id, token) => {
  return request({
    url: '/user/' + id + '/child',
    method: 'get',
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Resend = (data, token) => {
  return request({
    url: '/resend',
    method: 'post',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};