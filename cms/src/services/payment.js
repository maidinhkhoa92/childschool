import request from 'utils/request';

export const List = (params, token) => {
  return request({
    url: '/payment',
    method: 'get',
    params,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Create = (data, token) => {
  return request({
    url: '/payment',
    method: 'post',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};

export const Update = (id, data, token) => {
  return request({
    url: '/payment/' + id,
    method: 'put',
    data,
    headers: {
      Authorization: 'Bearer ' + token
    }
  });
};