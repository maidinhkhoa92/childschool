import request from 'utils/request';

const Login = params => {
  return request({
    url: '/login',
    method: 'post',
    data: params
  });
};

export { Login };
