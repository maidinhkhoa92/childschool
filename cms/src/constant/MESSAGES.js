export default {
  login_success: 'You logged in successfully',
  register_success: 'You registered in successfully',
  add_success: 'You added successfully',
  edit_success: 'You edited successfully',
  finish_success: 'You finish'
};
