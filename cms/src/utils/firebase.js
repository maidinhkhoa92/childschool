import firebase from 'firebase';
import 'firebase/storage';
const {
  REACT_APP_FIREBASE_KEY,
  REACT_APP_AUTH_DOMAIN,
  REACT_APP_DATABASE_URL,
  REACT_APP_PROJECT_ID,
  REACT_APP_STORAGE_BUCKET
} = process.env;

// const app = firebase.initializeApp({
//   apiKey: REACT_APP_FIREBASE_KEY || 'AIzaSyDNzZis7J30BJSm479iLYAvDKh_co4EFOs',
//   authDomain: REACT_APP_AUTH_DOMAIN || 'https://accounts.google.com/o/oauth2/auth',
//   databaseURL: REACT_APP_DATABASE_URL || 'https://childschool-webapp.firebaseio.com',
//   projectId: REACT_APP_PROJECT_ID || 'childschool-webapp',
//   storageBucket: REACT_APP_STORAGE_BUCKET || 'gs://childschool-webapp.appspot.com'
// });

const firebaseConfig = {
  apiKey: 'AIzaSyDHKpI70MoAW5ANa54nGIa9cgWIiJ1Pqic',
  //   authDomain: '<your-auth-domain>',
  databaseURL: 'https://mai-dinh-danh.firebaseio.com/',
  storageBucket: 'gs://mai-dinh-danh.appspot.com',
  projectId: 'Xk3Nf9GzmTkcAlfqa32J'
};
firebase.initializeApp(firebaseConfig);

// Get a reference to the storage service, which is used to create references in your storage bucket
export const storageRef = firebase.storage().ref();
export const firebaseStorage = firebase.storage;
