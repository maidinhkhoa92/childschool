export const statusMapping = (value) => {
    if (value === 'Pending') {
        return 'Pendiente'
    }
    if (value === 'Completed') {
        return 'Pagado'
    }
    return 'Reclamado'
}