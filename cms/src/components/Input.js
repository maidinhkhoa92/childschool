import React from 'react';
import { FormControl } from 'react-bootstrap';

export default ({ error, ...props }) => (
  <>
    <FormControl {...props} />
    {error && <small className="text-warning">{error}</small>}
  </>
);
