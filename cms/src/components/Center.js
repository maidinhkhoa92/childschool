import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';
import { Image, Col, Button } from 'react-bootstrap';
import Styled from 'styled-components';

const StyledLink = Styled(Link)`
    box-shadow: 0px -2px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25);
    height: 156px;
`;

const StyledImage = Styled(Image)`
  height: 110px;
`;

export default ({ src, href, name, onStatus, id, status, ...props }) => {
  const changeStatus = useCallback(() => {
    onStatus(id, { status: !status });
  }, [onStatus, id, status]);
  return (
    <Col {...props}>
      <StyledLink to={href} className="d-flex flex-column">
        <StyledImage src={src} />
        <h3 className="px-2 m-0">{name}</h3>
      </StyledLink>
      <Button block variant={status ? 'danger' : 'success'} onClick={changeStatus}>
        {status ? 'Inactive' : 'Active'}
      </Button>
    </Col>
  );
};
