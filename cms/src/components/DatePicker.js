import React, { useCallback } from 'react';
import DatePicker from 'react-datepicker';
import Styled from 'styled-components';

const StyledDatePicker = Styled.div`
  .react-datepicker-wrapper {
    display: block;
  }
`;

export default ({ onChange, name, error, ...props }) => {
  const onSetField = useCallback(
    date => {
      onChange(name, date);
    },
    [onChange]
  );
  return (
    <StyledDatePicker>
      <DatePicker {...props} onChange={onSetField} />
      {error && <small className="text-warning">{error}</small>}
    </StyledDatePicker>
  );
};
