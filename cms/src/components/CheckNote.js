import React from 'react';
import Styled from 'styled-components';
import { Image, FormCheck } from 'react-bootstrap';

const StyledImage = Styled(Image)`
    width: auto;
    top: 10px;
    right: 10px;
`;

export default () => (
  <td className="position-relative p-3">
    <FormCheck />
    <StyledImage src="/images/note.svg" className="position-absolute" />
  </td>
);
