import React, { useCallback } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Layout from './Layout';

const useConnect = () => {
  const stateToProps = {
    isLogedIn: useSelector(state => state.Admin.token !== null)
  };
  return {
    ...stateToProps
  };
};

export const Public = props => {
  const { isLogedIn } = useConnect();
  if (isLogedIn) {
    return <Redirect to="/" />;
  }

  return <Route {...props} />;
};

export const Private = props => {
  const { isLogedIn } = useConnect();

  if (!isLogedIn) {
    return <Redirect to="/login" />;
  }
  return <Route {...props} />;
};
