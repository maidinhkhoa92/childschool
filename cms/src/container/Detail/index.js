import React, { useMemo, useEffect } from 'react';
import Template from './Template';
import { userActions } from 'store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

const useConnect = () => {
  const stateToProps = {
    center: useSelector(state => state.User.detail)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onFetch: id =>
        dispatch({ type: userActions.DETAIL_CENTER_REQUEST, id, meta: { thunk: true } })
    }),
    [dispatch]
  );
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

export default () => {
  const { onFetch, center } = useConnect();
  const { id } = useParams();

  // fetch center
  useEffect(() => {
    (async function fetchCenter() {
      try {
        onFetch(id);
      } catch (e) {
        console.log(e);
      }
    })();
  }, [onFetch, id]);

  return <Template center={center} />;
};
