import React from 'react';
import { Row, Col, Tab, Nav } from 'react-bootstrap';
import { History, Payment, Information } from './components';
import Layout from '../Layout';
import Styled from 'styled-components';

const StyledNav = Styled(Nav)`
  && {
    .nav-link {
      background: transparent;
      &.active {
        color: #000;
        border: 0px;
        border-bottom: 2px solid #EE5B5B;
        background: transparent;
      }
    }
  }
  
`;

export default ({ center }) => (
  <Layout>
    <Row className="mb-4">
      <Col xs="12">
        <h2 className="text-center">{center && center.profile && center.profile.centerName ? center.profile.centerName : ''} de la escuela</h2>
      </Col>
      <Col xs="12">
        <Tab.Container defaultActiveKey="infor">
          <Row>
            <Col xs={12}>
              <StyledNav variant="pills" className="flex-row justify-content-between mt-4">
                <Nav.Item>
                  <Nav.Link eventKey="infor">Datos de contacto</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="payment">Datos para pago</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="history">Registro de pagos</Nav.Link>
                </Nav.Item>
              </StyledNav>
            </Col>
            <Col xs={12}>
              <Tab.Content>
                <Tab.Pane eventKey="infor" className="pt-4">
                  <Information />
                </Tab.Pane>
                <Tab.Pane eventKey="payment" className="pt-4">
                  <Payment />
                </Tab.Pane>
                <Tab.Pane eventKey="history" className="pt-4">
                  <History />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </Col>
    </Row>
  </Layout>
);
