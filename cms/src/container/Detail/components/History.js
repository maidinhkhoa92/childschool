import React, { useCallback, useState, useMemo, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Loading } from 'components';
import DatePicker from 'react-datepicker';
import HistoryForm from './HistoryForm';
import { paymentActions } from 'store/actions';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import { FormatDateParams } from 'utils/moment';
import { useParams } from 'react-router-dom';
import { statusMapping } from 'utils/generals';

const useConnect = () => {
  const stateToProps = {
    payments: useSelector(state => state.Payment.list)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onFetch: params =>
        dispatch({ type: paymentActions.GET_PAYMENT_REQUEST, params, meta: { thunk: true } })
    }),
    [dispatch]
  );
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

export default () => {
  const [status, setStatus] = useState(false);
  const [loading, setLoading] = useState(false);
  const { id } = useParams();
  const [params, setParams] = useState({ director: id, startDate: undefined, endDate: undefined });
  const [payment, setPayment] = useState(null);

  const { onFetch, payments } = useConnect();

  // fetch center
  useEffect(() => {
    (async function fetchCenter() {
      try {
        setLoading(true);
        await onFetch(params);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, [onFetch, params, setLoading]);

  const openEditable = useCallback((item) => {
    setStatus(true)
    setPayment(item)
  }, [])

  const openAdd = useCallback(() => {
    setPayment(null)
    setStatus(true)
  }, [])

  return (
    <>
      <div className="my-3 d-flex flex-row align-items-center justify-content-end">

        <Button variant="primary" onClick={openAdd}>
          Añadir
        </Button>
      </div>

      {loading ? (
        <Loading />
      ) : (
          <Table striped bordered>
            <thead>
              <tr>
                <td>Mes de pago</td>
                <td>Importe</td>
                <td>Status</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {_.map(payments, (item, key) => (
                <tr key={key}>
                  <td>{FormatDateParams(item.datePaid)}</td>
                  <td>{item.note}</td>
                  <td>{statusMapping(item.status)}</td>
                  <td>
                    <Button variant="primary" onClick={() => openEditable(item)}>Editar</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      <HistoryForm status={status} onClose={() => setStatus(false)} id={id} payment={payment} />
    </>
  );
};
