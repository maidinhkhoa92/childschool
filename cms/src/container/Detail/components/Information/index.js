import React, { useMemo, useCallback, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Template from './template';
import { userActions, alertActions } from 'store/actions';
import { useParams, useHistory } from 'react-router-dom';

const useConnect = () => {
  const stateToProps = {
    center: useSelector(state => state.User.detail)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onUpdate: (id, params) =>
        dispatch({ type: userActions.UPDATE_CENTER_REQUEST, id, params, meta: { thunk: true } }),
      onChangeStatus: (id, params) => dispatch({ type: userActions.STATUS_CENTER_REQUEST, id, params, meta: { thunk: true } }),
      onDelete: (id) => dispatch({ type: userActions.REMOVE_CENTER_REQUEST, id, meta: { thunk: true } }),
      success: alert => dispatch({ type: alertActions.OPEN_SUCCESS_ALERT, alert }),
      fail: alert => dispatch({ type: alertActions.OPEN_FAIL_ALERT, alert }),
      resendEmail: params => dispatch({ type: userActions.RESEND_EMAIL_REQUEST, params, meta: { thunk: true } })
    }),
    [dispatch]
  );

  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

export default () => {
  const { id } = useParams();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [popup, setPopup] = useState({ remove: false, change: false })
  const { center, onUpdate, onChangeStatus, onDelete, success, fail, resendEmail } = useConnect();

  const onRemove = useCallback(async () => {
    try {
      await onDelete(id);
      history.push('/')
      success('Updated');
    } catch (e) {
      fail(e)
    }
  }, [id, onDelete])

  const changeStatus = useCallback(async () => {
    try {
      const data = {
        status: !center.status
      }
      await onChangeStatus(id, data)
      setPopup({
        change: false,
        remove: false
      })
      success('Updated');
    } catch (e) {
      fail(e)
    }
  }, [id, onChangeStatus, center])

  const onSubmit = useCallback(async (values, actions) => {
    try {
      actions.setSubmitting(true);
      const data = {
        profile: {
          ...center.profile,
          ...values.profile
        },
        email: values.email
      }
      await onUpdate(id, data)
      success('Updated');
    } catch (e) {
      fail(e)
    } finally {
      actions.setSubmitting(false);
    }
  }, [id, onUpdate, center])

  const onClosePopup = useCallback(() => {
    setPopup({
      change: false,
      remove: false
    })
  }, [])

  const onOpenPopup = useCallback((name, value) => {
    setPopup({
      ...popup,
      [name]: value
    })
  }, [popup])

  const onResend = useCallback(async () => {
    try {
      setLoading(true);
      const data = {
        email: center.email
      }
      await resendEmail(data)
      success('Updated');
    } catch (e) {
      fail(e)
    } finally {
      setLoading(false);
    }
  }, [resendEmail, center])

  return <Template
    popup={popup}
    onClosePopup={onClosePopup}
    onOpenPopup={onOpenPopup}
    initialValues={center}
    onSubmit={onSubmit}
    changeStatus={changeStatus}
    onRemove={onRemove}
    onResend={onResend}
    loading={loading}
  />;
};
