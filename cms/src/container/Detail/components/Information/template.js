import React from 'react';
import { Form, Row, Col, Button, Modal, Spinner } from 'react-bootstrap';
import { Formik } from 'formik';
import validationSchema from './validate'

export default ({ onClosePopup, onOpenPopup, initialValues, onSubmit, changeStatus, onRemove, popup, onResend, loading }) => (
  <>
    <Formik initialValues={initialValues} enableReinitialize validationSchema={validationSchema} onSubmit={onSubmit}>
      {
        ({ values, errors, handleSubmit, handleChange }) => (
          <Form onSubmit={handleSubmit}>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.profile.centerName} placeholder="Nombre del centro" name="profile.centerName" onChange={handleChange} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.profile.businessName} placeholder="Razón social" name="profile.businessName" onChange={handleChange} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.profile.nif} placeholder="NIF" name="profile.nif" onChange={handleChange} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.profile.firstName} placeholder="Nombre  y apellidos del Responsable" name="profile.firstName" onChange={handleChange} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.email} placeholder="Email" name="email" onChange={handleChange} />
              </Col>
              <Col sm={3}>
                {loading ? <Spinner animation="border" role="status">
                  <span className="sr-only">Loading...</span>
                </Spinner> : <a className="btn btn-primary text-white" onClick={onResend}>Reenviar mail de bienvenida</a>}
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.profile.telephone} placeholder="Teléfono" name="profile.telephone" onChange={handleChange} />
              </Col>
            </Form.Group>
            <Form.Group as={Row}>
              <Col sm={6}>
                <Form.Control value={values.profile.postCode} placeholder="Código postal" name="profile.postCode" onChange={handleChange} />
              </Col>
            </Form.Group>
            <Row>
              <Col sm={6}>
                <Form.Group>
                  <Form.Control value={values.profile.city} placeholder="Ciudad" name="profile.city" onChange={handleChange} />
                </Form.Group>
                <Form.Group>
                  <Form.Control as="textarea" value={values.profile.address} placeholder="Dirección" name="profile.address" onChange={handleChange} />
                </Form.Group>
              </Col>
              <Col sm={6}>
                <Form.Control as="textarea" value={values.profile.note} placeholder="Notas internas" name="profile.note" onChange={handleChange} />
              </Col>
            </Row>
            <Form.Group as={Row}>
              <Col sm={4}>
                <Button variant="primary" block onClick={() => onOpenPopup('change', true)}>{values.status ? 'Inactivar centro' : 'Activar centro'}</Button>
              </Col>
              <Col sm={4}>
                <Button variant="primary" block onClick={() => onOpenPopup('remove', true)}>Eliminar centro</Button>
              </Col>
              <Col sm={4}>
                <Button type="submit" block variant="primary">Guardar cambios</Button>
              </Col>
            </Form.Group>
          </Form>
        )
      }

    </Formik>
    <Modal show={popup.change} onHide={onClosePopup}>
      <Modal.Dialog >
        <Modal.Body>
          <p>¿Estás seguro que quieres inactivar este centro?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClosePopup}>Cancelar</Button>
          <Button variant="primary" onClick={changeStatus}>Confirmar</Button>
        </Modal.Footer>
      </Modal.Dialog>
    </Modal>
    <Modal show={popup.remove} onHide={onClosePopup}>
      <Modal.Dialog >
        <Modal.Body>
          <p>¿Estás seguro que quieres eliminar este centro?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={onClosePopup}>Cancelar</Button>
          <Button variant="primary" onClick={onRemove}>Confirmar</Button>
        </Modal.Footer>
      </Modal.Dialog>
    </Modal>
  </>
);
