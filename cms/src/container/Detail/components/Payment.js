import React, { useState, useMemo, useCallback } from 'react';
import { Form, Row, Col, Image, Button } from 'react-bootstrap';
import { Loading, Input } from 'components';
import Styled from 'styled-components';
import { userActions } from 'store/actions';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';

const StyledImage = Styled(Image)`
  width: auto;
`;

const useConnect = () => {
  const stateToProps = {
    settings: useSelector(state => state.User.detail.settings)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onUpdate: (id, params) =>
        dispatch({
          type: userActions.UPDATE_SETTING_CENTER_REQUEST,
          id,
          params,
          meta: { thunk: true }
        }),
      onCount: id =>
        dispatch({
          type: userActions.COUNT_CHILD_REQUEST,
          id,
          meta: { thunk: true }
        })
    }),
    [dispatch]
  );
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

const validationSchema = Yup.object({
  current: Yup.number().required('Required'),
  before: Yup.number().required('Required'),
  paymentMethod: Yup.string().required('Required')
});

export default () => {
  const [loading, setLoading] = useState(false);
  const { onUpdate, onCount, settings } = useConnect();
  const { id } = useParams();

  const onRefresh = useCallback(async () => {
    try {
      setLoading(true);
      await onCount(id);
    } catch (e) {
      console.log(e);
    } finally {
      setLoading(false);
    }
  }, [onCount, id]);

  const onSubmit = useCallback(
    async (values, actions) => {
      try {
        actions.setSubmitting(true);
        const params = {
          settings: {
            ...values
          }
        };
        await onUpdate(id, params);
      } catch (e) {
        console.log(e);
      } finally {
        actions.setSubmitting(false);
      }
    },
    [onUpdate, id]
  );

  return (
    <Formik initialValues={settings} onSubmit={onSubmit} validationSchema={validationSchema}>
      {props => (
        <Form onSubmit={props.handleSubmit}>
          <Form.Group as={Row}>
            <Form.Label column sm={5}>
              Cantidad de niños declarados por el Director
            </Form.Label>
            <Col sm={3}>
              <Input
                type="text"
                name="before"
                onChange={props.handleChange}
                error={props.errors.before}
                value={props.values.before}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm={5}>
              Cantidad de niños actual (solo comprobar el dia de facturación)
            </Form.Label>
            <Col sm={3}>
              {loading ? (
                <Loading />
              ) : (
                  <Input
                    type="text"
                    name="current"
                    onChange={props.handleChange}
                    error={props.errors.current}
                    value={props.values.current}
                  />
                )}
            </Col>
            <Col sm={2}>
              <StyledImage src="/images/refresh.svg" onClick={onRefresh} />
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={2}>
              Forma de pago
            </Form.Label>
            <Col sm={2} className="d-flex align-items-center">
              <Form.Check
                type="radio"
                label="Domiciliación"
                name="paymentMethod"
                value="Domiciliación"
                onChange={props.handleChange}
                checked={props.values.paymentMethod === 'Domiciliación'}
              />
            </Col>
            <Col sm={2} className="d-flex align-items-center">
              <Form.Check
                type="radio"
                label="Efectivo"
                name="paymentMethod"
                value="Efectivo"
                onChange={props.handleChange}
                checked={props.values.paymentMethod === 'Efectivo'}
              />

            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Form.Label column sm={2} />
            <Col sm={2} className="d-flex align-items-center">
              <Form.Check
                type="radio"
                label="Transferencia"
                name="paymentMethod"
                value="Transferencia"
                onChange={props.handleChange}
                checked={props.values.paymentMethod === 'Transferencia'}
              />
            </Col>
            <Col sm={2} className="d-flex align-items-center">
              <Form.Check
                type="radio"
                label="Cheque"
                name="paymentMethod"
                value="cheque"
                onChange={props.handleChange}
                checked={props.values.paymentMethod === 'cheque'}
              />
              {props.errors && props.errors.paymentMethod && (
                <small className="text-dange">{props.errors.paymentMethod}</small>
              )}
            </Col>
          </Form.Group>

          <Form.Group className="text-center">
            {props.isSubmitting ? (
              <Loading />
            ) : (
                <Button type="submit" variant="primary">
                  Guardar cambios
              </Button>
              )}
          </Form.Group>
        </Form>
      )
      }
    </Formik >
  );
};
