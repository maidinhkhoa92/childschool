import React, { useCallback, useMemo, useEffect } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { Formik } from 'formik';
import { Input, Loading, DatePicker } from 'components';
import { initialValues, validationSchema } from './validate';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { paymentActions, alertActions } from 'store/actions';
import { FormatDateParams } from 'utils/moment';

const useConnect = () => {
  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onCreate: params =>
        dispatch({ type: paymentActions.CREATE_PAYMENT_REQUEST, params, meta: { thunk: true } }),
      onEdit: (id, params) => dispatch({ type: paymentActions.EDIT_PAYMENT_REQUEST, id, params, meta: { thunk: true } }),
      success: alert => dispatch({ type: alertActions.OPEN_SUCCESS_ALERT, alert }),
      fail: alert => dispatch({ type: alertActions.OPEN_FAIL_ALERT, alert })
    }),
    [dispatch]
  );
  return {
    ...dispatchToProps
  };
};

export default ({ status, onClose, payment }) => {
  const { id } = useParams();

  const { onCreate, onEdit, success, fail } = useConnect();

  const reinitialValues = useMemo(() => {
    if (payment) {
      return {
        ...payment,
        datePaid: new Date(payment.datePaid)
      }
    }
    return initialValues
  }, [payment])

  const onSubmit = useCallback(
    async (values, actions) => {
      try {
        actions.setSubmitting(true);
        const params = {
          status: values.status,
          note: values.note,
          datePaid: FormatDateParams(values.datePaid),
          director: id
        };
        if (payment) {
          await onEdit(values.id, params)
        } else {
          await onCreate(params);
        }

        success('Success');
        onClose();
      } catch (e) {
        fail(e || e.message);
      } finally {
        actions.setSubmitting(false);
      }
    },
    [onCreate, onEdit, payment, success, fail, id, onClose]
  );

  return (
    <Modal show={status} onHide={onClose}>
      <Modal.Body>
        <h3 className="text-center">Registro de pago</h3>
        <Formik
          initialValues={reinitialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
          enableReinitialize
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>

              <Form.Group>
                <DatePicker
                  className="form-control w-100"
                  placeholderText="Mes de pago"
                  name="datePaid"
                  onChange={props.setFieldValue}
                  error={props.errors.datePaid}
                  selected={props.values.datePaid}
                />
              </Form.Group>
              <Form.Group>
                <Input
                  type="text"
                  placeholder="Importe"
                  name="note"
                  value={props.values.note}
                  onChange={props.handleChange}
                  error={props.errors.note}
                />
              </Form.Group>
              <Form.Group>
                <Form.Control
                  as="select"
                  onChange={props.handleChange}
                  name="status"
                  value={props.values.status}
                >
                  <option value="">Status</option>
                  <option value="Pending">Pendiente</option>
                  <option value="Completed">Pagado</option>
                  <option value="Open">Reclamado</option>
                </Form.Control>
                {props.errors.status && (
                  <small className="text-warning">{props.errors.status}</small>
                )}
              </Form.Group>
              <Form.Group className="text-center">
                {props.isSubmitting ? (
                  <Loading />
                ) : (
                    <Button type="submit" variant="primary" className="mt-5" block>
                      Guardar registro
                  </Button>
                  )}
              </Form.Group>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
};
