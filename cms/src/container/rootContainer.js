import React, { Suspense } from 'react';
import { Loading } from 'components';

// import style
import 'react-datepicker/dist/react-datepicker.css';
import 'style/index.scss';

const RootContainer = props => {
  return <Suspense fallback={<Loading />}>{props.children}</Suspense>;
};

export default RootContainer;
