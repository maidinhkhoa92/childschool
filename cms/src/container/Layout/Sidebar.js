import React, { useMemo } from 'react';
import { Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { adminActions } from 'store/actions';
import { useDispatch } from 'react-redux';

const useConnect = () => {
  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onLogout: () => dispatch({ type: adminActions.LOGIN_OUT })
    }),
    [dispatch]
  );
  return {
    ...dispatchToProps
  };
};

export default () => {
  const { onLogout } = useConnect();
  return (
    <Col md="3" className="position-fixed h-100 bg-primary text-right">
      <Link className="d-block px-1 py-3 m-0 h3 text-white" to="/">
        Centros
      </Link>
      <Link className="d-block px-1 py-3 m-0 h3 text-white" to="/history">
        Registro de pagos
      </Link>
      <Link className="d-block px-1 py-3 m-0 h3 text-white" to="/history">
        Noticias y blog
      </Link>
      <Button
        variant="link"
        block
        className="d-block px-1 py-3 m-0 mt-5 h3 text-white text-right"
        onClick={onLogout}
      >
        Cerrar sesión
      </Button>
    </Col>
  );
};
