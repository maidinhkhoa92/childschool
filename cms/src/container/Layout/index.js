import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Sidebar from './Sidebar';

export default ({ children }) => (
  <Container fluid>
    <Row>
      <Sidebar />
      <Col md={{ span: 9, offset: 3 }} className="py-3">
        {children}
      </Col>
    </Row>
  </Container>
);
