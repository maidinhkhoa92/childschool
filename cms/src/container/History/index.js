import React, { useCallback, useState, useMemo, useEffect } from 'react';
import { paymentActions } from 'store/actions';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import { FormatDateParams } from 'utils/moment';
import Template from './Template';

const useConnect = () => {
  const stateToProps = {
    payments: useSelector(state => state.Payment.list)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onFetch: params =>
        dispatch({ type: paymentActions.GET_PAYMENT_REQUEST, params, meta: { thunk: true } })
    }),
    [dispatch]
  );
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

export default () => {
  const [status, setStatus] = useState(false);
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState({ startDate: undefined, endDate: undefined });

  const { onFetch, payments } = useConnect();

  const [payment, setPayment] = useState(null);


  // fetch center
  useEffect(() => {
    (async function fetchCenter() {
      try {
        setLoading(true);
        await onFetch(params);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    })();
  }, [onFetch, params, setLoading]);

  const onFilter = useCallback(
    async (date, type) => {
      try {
        setLoading(true);
        const filter = {
          ...params,
          [type]: FormatDateParams(date)
        };
        setParams({
          ...params,
          [type]: date
        });

        await onFetch(filter);
      } catch (e) {
        console.log(e);
      } finally {
        setLoading(false);
      }
    },
    [onFetch, params, setLoading]
  );

  const openEditable = useCallback((item) => {
    setStatus(true)
    setPayment(item)
  }, [])

  const openAddnew = useCallback((item) => {
    setStatus(true)
    setPayment(null)
  }, [])

  return (
    <Template
      status={status}
      setStatus={setStatus}
      onFilter={onFilter}
      payments={payments}
      loading={loading}
      params={params}
      openEditable={openEditable}
      payment={payment}
      openAddnew={openAddnew}
    />
  );
};
