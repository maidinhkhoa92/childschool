import React, { useCallback, useMemo, useEffect } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { Formik } from 'formik';
import { Input, Loading, DatePicker } from 'components';
import { initialValues, validationSchema } from './validate';
import { useDispatch, useSelector } from 'react-redux';
import { paymentActions, alertActions } from 'store/actions';
import { FormatDateParams } from 'utils/moment';
import _ from 'lodash'

const useConnect = () => {
  const stateToProps = {
    centers: useSelector(state => state.Payment.centers)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onCreate: params =>
        dispatch({ type: paymentActions.CREATE_PAYMENT_REQUEST, params, meta: { thunk: true } }),
      onEdit: (id, params) => dispatch({ type: paymentActions.EDIT_PAYMENT_REQUEST, id, params, meta: { thunk: true } }),
      success: alert => dispatch({ type: alertActions.OPEN_SUCCESS_ALERT, alert }),
      fail: alert => dispatch({ type: alertActions.OPEN_FAIL_ALERT, alert }),
      onFetch: params =>
        dispatch({ type: paymentActions.GET_PAYMENT_CENTER_REQUEST, params, meta: { thunk: true } })
    }),
    [dispatch]
  );
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

export default ({ status, onClose, payment }) => {
  const { onCreate, centers, onEdit, success, fail, onFetch } = useConnect();

  // fetch center
  useEffect(() => {
    (async function fetchCenter() {
      try {
        await onFetch({});
      } catch (e) {
        console.log(e);
      }
    })();
  }, [onFetch]);

  const reinitialValues = useMemo(() => {
    if (payment) {
      return {
        ...payment,
        datePaid: new Date(payment.datePaid),
        director: payment.director._id
      }
    }
    return initialValues
  }, [payment])

  const onSubmit = useCallback(
    async (values, actions) => {
      try {
        actions.setSubmitting(true);
        const params = {
          status: values.status,
          note: values.note,
          datePaid: FormatDateParams(values.datePaid),
          director: values.director
        };
        if (payment) {
          await onEdit(values.id, params)
        } else {
          await onCreate(params);
        }

        success('Success');
        onClose();
      } catch (e) {
        fail(e || e.message);
      } finally {
        actions.setSubmitting(false);
      }
    },
    [onCreate, onEdit, payment, success, fail, onClose]
  );

  return (
    <Modal show={status} onHide={onClose}>
      <Modal.Body>
        <h3 className="text-center">Registro de pago</h3>
        <Formik
          initialValues={reinitialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
          enableReinitialize
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              <Form.Group>
                <Form.Control as="select"
                  value={props.values.director}
                  onChange={props.handleChange}
                  name="director"
                  value={props.values.director}
                >
                  <option>Nombre del centro</option>
                  {_.map(centers, (item, key) => (
                    <option value={item.id} key={key}>
                      {item.profile.centerName}
                    </option>
                  ))}
                </Form.Control>
                {props.errors.director && (
                  <small className="text-warning">{props.errors.director}</small>
                )}
              </Form.Group>
              <Form.Group>
                <DatePicker
                  className="form-control w-100"
                  placeholderText="Mes de pago"
                  name="datePaid"
                  onChange={props.setFieldValue}
                  error={props.errors.datePaid}
                  selected={props.values.datePaid}
                />
              </Form.Group>
              <Form.Group>
                <Input
                  type="text"
                  placeholder="Importe"
                  name="note"
                  value={props.values.note}
                  onChange={props.handleChange}
                  error={props.errors.note}
                />
              </Form.Group>
              <Form.Group>
                <Form.Control
                  as="select"
                  onChange={props.handleChange}
                  name="status"
                  value={props.values.status}
                >
                  <option value="">Status</option>
                  <option value="Pending">Pendiente</option>
                  <option value="Completed">Pagado</option>
                  <option value="Open">Reclamado</option>
                </Form.Control>
                {props.errors.status && (
                  <small className="text-warning">{props.errors.status}</small>
                )}
              </Form.Group>
              <Form.Group className="text-center">
                {props.isSubmitting ? (
                  <Loading />
                ) : (
                    <Button type="submit" variant="primary" className="mt-5" block>
                      Guardar registro
                  </Button>
                  )}
              </Form.Group>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
};
