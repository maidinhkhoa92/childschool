import * as Yup from 'yup';

export const initialValues = {
  note: '',
  datePaid: '',
  status: ''
};

export const validationSchema = Yup.object({
  note: Yup.string().required('Campo requerido'),
  datePaid: Yup.string().required('Campo requerido'),
  status: Yup.string().required('Campo requerido')
});
