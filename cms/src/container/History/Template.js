import React from 'react';
import { Table, Button } from 'react-bootstrap';
import { Loading } from 'components';
import DatePicker from 'react-datepicker';
import _ from 'lodash';
import { FormatDateParams } from 'utils/moment';
import HistoryForm from './HistoryForm';
import Layout from 'container/Layout';
import { statusMapping } from 'utils/generals';

export default ({ status, setStatus, onFilter, payments, loading, params, openEditable, openAddnew, payment }) => {

  return (
    <Layout>
      <div className="my-3 d-flex flex-row align-items-center justify-content-between">
        <div className="d-flex flex-row align-items-center ">
          Start Date:
          <DatePicker
            className="mr-3 ml-1"
            placeholderText="Start date"
            onChange={date => onFilter(date, 'startDate')}
            selected={params.startDate}
          />
          End Date:
          <DatePicker
            className="ml-1"
            placeholderText="End date"
            onChange={date => onFilter(date, 'endDate')}
            selected={params.endDate}
          />
        </div>
        <Button variant="primary" onClick={openAddnew}>
          Add
        </Button>
      </div>

      {loading ? (
        <Loading />
      ) : (
          <Table striped bordered>
            <thead>
              <tr>
                <td>Nombre centro</td>
                <td>Importe</td>
                <td>Paid date</td>
                <td>Status</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {_.map(payments, (item, key) => (
                <tr key={key}>
                  <td>{(item.director && item.director.profile && item.director.profile.centerName) && item.director.profile.centerName}</td>
                  <td>{item.note}</td>
                  <td>{FormatDateParams(item.datePaid)}</td>
                  <td>{statusMapping(item.status)}</td>
                  <td>
                    <Button variant="primary" onClick={() => openEditable(item)}>Editar</Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      <HistoryForm payment={payment} status={status} onClose={() => setStatus(false)} />
    </Layout>
  );
};
