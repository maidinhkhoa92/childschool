import React, { useState, useMemo, useEffect } from 'react';
import _ from 'lodash';
import Template from './Template';
import { userActions } from 'store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const useConnect = () => {
  const stateToProps = {
    centers: useSelector(state => state.User.list)
  };

  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onFetch: params =>
        dispatch({ type: userActions.GET_CENTER_REQUEST, params, meta: { thunk: true } }),
    }),
    [dispatch]
  );
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};

export default () => {
  const { onFetch, centers } = useConnect();
  const [status, setStatus] = useState(false);
  const history = useHistory();

  // fetch center
  useEffect(() => {
    (async function fetchCenter() {
      try {
        await onFetch({});
      } catch (e) {
        console.log(e);
      }
    })();
  }, [onFetch]);

  return (
    <Template
      status={status}
      setStatus={setStatus}
      centers={centers}
      history={history}
    />
  );
};
