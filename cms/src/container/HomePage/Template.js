import React from 'react';
import _ from 'lodash';
import { Search, AddButton, AddForm } from './components';
import { Row, Col, Table } from 'react-bootstrap';
import Layout from '../Layout';

export default ({ status, setStatus, centers, history }) => (
  <Layout>
    <Row className="mb-4">
      <Col xs="12">
        <h2>Centros</h2>
      </Col>
    </Row>

    <Row className="mb-3 ">
      <Col xs="5" className="d-flex align-items-center">
        <Search />
        <AddButton onClick={() => setStatus(true)} />
      </Col>
    </Row>
    <Row>
      <Col>
        <Table responsive>
          <thead>
            <tr>
              <th>Nombre centro</th>
              <th>Responsable</th>
              <th>Email</th>
              <th>Teléfono</th>
            </tr>
          </thead>
          <tbody>
            {_.map(centers, (center, i) => (
              <tr key={i} onClick={() => history.push(`/center/${center.id}`)}>
                <td>{center.profile && center.profile.centerName ? center.profile.centerName : ''}</td>
                <td>{center.profile && center.profile.firstName ? center.profile.firstName : ''}</td>
                <td>{center.email}</td>
                <td>{center.profile && center.profile.telephone ? center.profile.telephone : ''}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Col>
    </Row>
    <AddForm status={status} onClose={() => setStatus(false)} />
  </Layout>
);
