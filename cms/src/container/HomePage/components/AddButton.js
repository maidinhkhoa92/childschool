import React from 'react';
import Styled from 'styled-components';
import { Button, Image } from 'react-bootstrap';

const StyledButton = Styled(Button)`
  && {
    img {
        height: 48px;
    }
  }
`;

export default ({ onClick }) => (
  <StyledButton
    className="d-flex justify-content-center align-items-center border-0 bg-transparent"
    onClick={onClick}
  >
    <Image src="/images/add.svg" />
  </StyledButton>
);
