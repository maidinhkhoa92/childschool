import * as Yup from 'yup';

export const initialValues = {
  email: '',
  profile: {
    centerName: '',
    firstName: '',
    telephone: ''
  }
};

export const validationSchema = Yup.object({
  email: Yup.string()
    .email()
    .required('Required'),
  profile: Yup.object().shape({
    centerName: Yup.string().required('Required'),
    firstName: Yup.string().required('Required'),
    telephone: Yup.string().required('Required')
  })
});
