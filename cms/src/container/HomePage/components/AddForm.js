import React, { useCallback, useMemo } from 'react';
import { Modal, Form, Button } from 'react-bootstrap';
import { Formik } from 'formik';
import { Input, Loading } from 'components';
import { initialValues, validationSchema } from './validate';
import { userActions, alertActions } from 'store/actions';
import { useDispatch } from 'react-redux';
import Styled from 'styled-components';

const StyledButton = Styled(Button)`
  font-size: 28px;
  line-height: 33px;
`;

const useConnect = () => {
  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onCreate: params =>
        dispatch({ type: userActions.CREATE_CENTER_REQUEST, params, meta: { thunk: true } }),
      success: alert => dispatch({ type: alertActions.OPEN_SUCCESS_ALERT, alert }),
      fail: alert => dispatch({ type: alertActions.OPEN_FAIL_ALERT, alert })
    }),
    [dispatch]
  );
  return {
    ...dispatchToProps
  };
};

export default ({ status, onClose }) => {
  const { onCreate, success, fail } = useConnect();

  const onSubmit = useCallback(async (values, actions) => {
    try {
      actions.setSubmitting(true);
      await onCreate(values);
      success('Success');
      onClose();
    } catch (e) {
      fail(e || e.message);
    } finally {
      actions.setSubmitting(false);
    }
  }, []);

  return (
    <Modal show={status} onHide={onClose}>
      <Modal.Body>
        <h3 className="text-center">Crear Centro</h3>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {props => (
            <Form onSubmit={props.handleSubmit}>
              <Form.Group>
                <Input
                  type="text"
                  placeholder="Nombre del centro"
                  name="profile.centerName"
                  error={props.errors.profile && props.errors.profile.centerName}
                  onChange={props.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Input
                  type="text"
                  placeholder="Nombre y apellidos de responsable"
                  name="profile.firstName"
                  error={props.errors.profile && props.errors.profile.firstName}
                  onChange={props.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Input
                  type="email"
                  placeholder="Mail"
                  name="email"
                  error={props.errors.email}
                  onChange={props.handleChange}
                />
              </Form.Group>
              <Form.Group>
                <Input
                  type="text"
                  placeholder="Teléfono"
                  name="profile.telephone"
                  error={props.errors.profile && props.errors.profile.telephone}
                  onChange={props.handleChange}
                />
              </Form.Group>
              <Form.Group className="text-center">
                {props.isSubmitting ? (
                  <Loading />
                ) : (
                    <StyledButton block type="submit" variant="primary" className="mt-5">
                      Crear ficha y enviar mail Bienvenida
                  </StyledButton>
                  )}
              </Form.Group>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
};
