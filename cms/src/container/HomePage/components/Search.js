import React from 'react';
import { Form, InputGroup, Image } from 'react-bootstrap';

export default () => (
  <Form.Group className="border w-100 m-0">
    <InputGroup>
      <InputGroup.Prepend>
        <InputGroup.Text className="bg-transparent border-0">
          <Image src="/images/icon-search.svg" />
        </InputGroup.Text>
      </InputGroup.Prepend>
      <Form.Control type="text" className="border-0" />
    </InputGroup>
  </Form.Group>
);
