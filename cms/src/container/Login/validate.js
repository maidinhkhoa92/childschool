import * as Yup from 'yup';

export const initialValues = {
  username: '',
  password: ''
};

export const validationSchema = Yup.object({
  username: Yup.string().required('Required'),
  password: Yup.string()
    .min(6, 'Password must be at least 6 chars long')
    .required('Required')
});
