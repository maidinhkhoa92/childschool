import React from 'react';
import { Form, Button, Col } from 'react-bootstrap';
import { Formik } from 'formik';
import Styled from 'styled-components';
import { COLORS } from 'constant';
import { Input, Loading } from 'components';
import { initialValues, validationSchema } from './validate';

const StyledForm = Styled(Form)`
  max-width: 526px;
  padding: 15px;
  .form-row {
    background: ${COLORS.red};
  }
`;

export default ({ onSubmit }) => (
  <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
    {props => (
      <StyledForm className="m-auto w-100" onSubmit={props.handleSubmit}>
        <h1 className="mb-5">Bienvenido a Ontabb</h1>
        <Form.Row className="justify-content-center pt-5">
          <Form.Group as={Col} md="8" xs="12">
            <Input
              type="text"
              placeholder="Introduce tu email"
              className="bg-secondary"
              name="username"
              error={props.errors.username}
              onChange={props.handleChange}
            />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-center">
          <Form.Group as={Col} md="8" xs="12">
            <Input
              type="password"
              placeholder="Contraseña"
              className="bg-secondary"
              name="password"
              error={props.errors.password}
              onChange={props.handleChange}
            />
          </Form.Group>
        </Form.Row>
        <Form.Row className="justify-content-center  pb-5">
          <Form.Group as={Col} md="8" xs="12" className="text-center">
            {props.isSubmitting ? (
              <Loading />
            ) : (
              <Button type="submit" block variant="secondary">
                Submit
              </Button>
            )}
          </Form.Group>
        </Form.Row>
      </StyledForm>
    )}
  </Formik>
);
