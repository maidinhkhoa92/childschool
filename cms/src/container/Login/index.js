import React, { useCallback, useMemo } from 'react';
import Template from './Template';
import { adminActions, alertActions } from 'store/actions';
import { useDispatch } from 'react-redux';

const useConnect = () => {
  const dispatch = useDispatch();
  const dispatchToProps = useMemo(
    () => ({
      onLogin: params =>
        dispatch({ type: adminActions.LOGIN_REQUEST, params, meta: { thunk: true } }),
      success: alert => dispatch({ type: alertActions.OPEN_SUCCESS_ALERT, alert }),
      fail: alert => dispatch({ type: alertActions.OPEN_FAIL_ALERT, alert })
    }),
    [dispatch]
  );
  return {
    ...dispatchToProps
  };
};

export default () => {
  const { onLogin, success, fail } = useConnect();
  const onSubmit = useCallback(
    async (values, actions) => {
      try {
        actions.setSubmitting(true);
        await onLogin(values);
        success('Success');
      } catch (e) {
        fail(e || e.message);
      } finally {
        actions.setSubmitting(false);
      }
    },
    [onLogin]
  );
  return <Template onSubmit={onSubmit} />;
};
