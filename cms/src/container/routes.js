import React from 'react';
import { Router, Switch, Redirect } from 'react-router-dom';
import { Public, Private } from './authRouter';
import { Toast, Alert } from 'react-bootstrap';
import Styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import actions from 'store/alert/actions';

// import component
import history from 'utils/history';
import RootContainer from './rootContainer';
import HomePage from './HomePage';
import Login from './Login';
import Detail from './Detail';
import History from './History';

// Connect to redux
const useConnect = () => {
  // Get state from reducer and map to props
  const stateToProps = {
    AlertState: useSelector(state => state.Alert)
  };
  // Get dispatch and map to props
  const dispatch = useDispatch();
  const dispatchToProps = {
    closeAlert: () => dispatch({ type: actions.CLOSE_ALERT })
  };
  return {
    ...stateToProps,
    ...dispatchToProps
  };
};
const StyledToast = Styled(Toast)`
  top: 0px;
  z-index: 4;
`;
const AppRouter = () => {
  const { AlertState, closeAlert } = useConnect();

  return (
    <Router history={history}>
      <RootContainer>
        <StyledToast
          show={AlertState.status}
          delay={3000}
          className="mw-100 m-0 border-0 position-fixed w-100"
          autohide
          onClose={closeAlert}
        >
          <Toast.Body className="w-100 p-0">
            <Alert variant={AlertState.variant} className="m-0" show={AlertState.status}>
              {AlertState.message}
            </Alert>
          </Toast.Body>
        </StyledToast>
        <Switch>
          <Private exact path="/" component={HomePage} />
          <Public path="/login" component={Login} />
          <Private path="/center/:id" component={Detail} />
          <Private path="/history" component={History} />
          <Redirect to="/" />
        </Switch>
      </RootContainer>
    </Router>
  );
};
export default AppRouter;
