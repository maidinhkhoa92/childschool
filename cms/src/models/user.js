export default {
  profile: {
    centerName: '',
    firstName: '',
    lastName: '',
    telephone: '',
    postcode: '',
    city: '',
    district: '',
    email: '',
    address: '',
    photo: '',
    hotline: '',
    addess: ''
  },
  settings: {
    current: 0,
    before: 0,
    paymentMethod: '',
    note: '',
    total: 0
  }
};
